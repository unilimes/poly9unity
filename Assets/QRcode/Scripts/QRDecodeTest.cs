using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TBEasyWebCam;
using System.Collections;

public class QRDecodeTest : MonoBehaviour
{
	public QRCodeDecodeController e_qrController;

	public Text UiText;

	//public GameObject resetBtn;

	//public GameObject scanLineObj;

	bool isTorchOn = false;

	public Sprite torchOnSprite;
	public Sprite torchOffSprite;
	//public Image torchImage;
	/// <summary>
	/// when you set the var is true,if the result of the decode is web url,it will open with browser.
	/// </summary>
	public bool isOpenBrowserIfUrl;
    public bool needCallMethod;

    private void Start()
	{
		if (this.e_qrController != null)
		{
			this.e_qrController.onQRScanFinished += new QRCodeDecodeController.QRScanFinished(this.qrScanFinished);
		}
	}

	

	private void qrScanFinished(string dataText)
	{
		if (isOpenBrowserIfUrl) {
			if (Utility.CheckIsUrlFormat(dataText))
			{
				if (!dataText.Contains("http://") && !dataText.Contains("https://"))
				{
					dataText = "http://" + dataText;
				}
				Application.OpenURL(dataText);
			}
		}

        HELPER.instance.qrResault = dataText;

        UiText.text = dataText;

        if (needCallMethod)
        {
            StartCoroutine(StartMethod());
        }


	}

	public void Reset()
	{
		if (this.e_qrController != null)
		{
			this.e_qrController.Reset();
		}
		if (this.UiText != null)
		{
			this.UiText.text = string.Empty;
		}

	}

    IEnumerator StartMethod()
    {
        needCallMethod = false;
        HELPER.instance.QRCam.SetActive(false);
        HELPER.instance.QRCanvas.SetActive(false);
        HELPER.instance.FindProductByQR();
        HELPER.instance.house.SetActive(true);
        yield return 0;
    }

    public void Play()
	{
		Reset ();
		if (this.e_qrController != null)
		{
			this.e_qrController.StartWork();
		}
	}

	public void Stop()
	{
		if (this.e_qrController != null)
		{
			this.e_qrController.StopWork();
		}


	}

	public void GotoNextScene(string scenename)
	{
		if (this.e_qrController != null)
		{
			this.e_qrController.StopWork();
		}
		//Application.LoadLevel(scenename);
		UnityEngine.SceneManagement.SceneManager.LoadScene(scenename);
	}

	/// <summary>
	/// Toggles the torch by click the ui button
	/// note: support the feature by using the EasyWebCam Component 
	/// </summary>
	public void toggleTorch()
	{
		#if UNITY_ANDROID && !UNITY_EDITOR
		if (EasyWebCam.isActive) {
			if (isTorchOn) {
				//torchImage.sprite = torchOffSprite;
				EasyWebCam.setTorchMode (TBEasyWebCam.Setting.TorchMode.Off);
			} else {
				//torchImage.sprite = torchOnSprite;
				EasyWebCam.setTorchMode (TBEasyWebCam.Setting.TorchMode.On);
			}
			isTorchOn = !isTorchOn;
		}
		#endif
	}

}

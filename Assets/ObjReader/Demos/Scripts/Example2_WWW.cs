// This example loads an OBJ file from the WWW, including the MTL file and any textures that might be reference

using UnityEngine;
using System.Collections;
using System.IO;

public class Example2_WWW : MonoBehaviour {

    public SceneManager sm;
	public string objFileName = "";
	//public Material standardMaterial;	// The shader for non-transparent objects is supplied by this material. Also used for objects that have no MTL file.
	ObjReader.ObjData objData;
	string loadingText = "";
	bool loading = false;
    public GameObject parentForOBJ;
    public PerCharacterKerning[] per;
    public Font customFont;
    public AddTextToTexture at;
	public FocusSquare focusSquare;
    //public Texture2D tex2dStandart;

    private void Start()
    {
        per = at.perCharacterKerning;
    }

    IEnumerator Load () {
		loading = true;
		if (objData != null && objData.gameObjects != null) {
			for (var i = 0; i < objData.gameObjects.Length; i++) {
				Destroy (objData.gameObjects[i]);
			}
		}

        Material mat1 = new Material(Shader.Find("Standard (Roughness setup)"));

       

        objData = ObjReader.use.ConvertFileAsync (objFileName, true, mat1);
		while (!objData.isDone) {
			loadingText = "Loading... " + (objData.progress*100).ToString("f0") + "%";
			if (Input.GetKeyDown (KeyCode.Escape)) {
				objData.Cancel();
				loadingText = "Cancelled download";
				loading = false;
				yield break;
			}
			yield return null;
		}
		loading = false;
		if (objData == null || objData.gameObjects == null) {
			loadingText = "Error loading file";
			yield return null;
			yield break;
		}

        var parentGo = Instantiate(parentForOBJ, transform.position, Quaternion.identity);
        parentGo.tag = "CurrentObjParent";
        parentGo.AddComponent<VoxelBusters.RuntimeSerialization.UIDSystem>();
        parentGo.GetComponent<VoxelBusters.RuntimeSerialization.UIDSystem>().ReassignUIDs(false);
		//parentGo.AddComponent<Lean.Touch.LeanRotate> ();
		parentGo.AddComponent<Lean.Touch.LeanRotate> ();
        parentGo.AddComponent<UnityEngine.XR.iOS.UnityARHitTestExample> ();
        parentGo.GetComponent<UnityEngine.XR.iOS.UnityARHitTestExample> ().m_HitTransform = HELPER.instance.arParent;
        parentGo.GetComponent<UnityEngine.XR.iOS.UnityARHitTestExample> ().maxRayDistance = 30;
        focusSquare.m_HitTransform = HELPER.instance.arParent;

        for (int i = 0; i < objData.gameObjects.Length; i++)
        {
            objData.gameObjects[i].transform.SetParent(parentGo.transform, true);
            objData.gameObjects[i].tag = "ChildObj1";
            objData.gameObjects[i].AddComponent<ModelCustomTag>();
            objData.gameObjects[i].AddComponent<ColorPickerTester>();
            objData.gameObjects[i].AddComponent<MeshCollider>();
            objData.gameObjects[i].AddComponent<VoxelBusters.RuntimeSerialization.UIDSystem>();
            objData.gameObjects[i].GetComponent<VoxelBusters.RuntimeSerialization.UIDSystem>().ReassignUIDs(false);
            // objData.gameObjects[i].AddComponent<AddTextToTexture>();
            //var addText = objData.gameObjects[i].GetComponent<AddTextToTexture>();
            //addText.fontCountX = 10;
            //addText.fontCountY = 10;
            //addText.textPlacementY = 462;
            //addText.perCharacterKerning = per;
            //addText.lineSpacing = 0.75f;
            //addText.customFont = customFont;
            //objData.gameObjects[i].AddComponent<cakeslice.Outline>();
        }
      
        var cam = GameObject.FindWithTag("MainCamera");
        cam.GetComponent<MainCameraController>().enabled = true;
        parentGo.AddComponent<TextBoundingBox>();
        
        sm.LoadTextures();
    }

    //void OnGUI () {
    //	GUILayout.BeginArea (new Rect(10, 10, 400, 400));
    //	objFileName = GUILayout.TextField (objFileName, GUILayout.Width(400));
    //	GUILayout.Label ("Also try pig.obj, car.obj, and cube.obj");
    //	if (GUILayout.Button ("Import") && !loading) {
    //		StartCoroutine (Load());
    //	}
    //	GUILayout.Label (loadingText);
    //	GUILayout.EndArea();
    //}

    public void LoadModel()
    {
        //string fileNameNoExt = Path.GetFileNameWithoutExtension(objFileName);
       // Debug.Log(fileNameNoExt);
        StartCoroutine(Load());
    }
	
}
﻿using System.Collections;
using UnityEngine;

public class CheckConnection : MonoBehaviour {

    
    float currCountdownValue;
    public GameObject alert;

    IEnumerator StartCountdown(float countdownValue = 20)
    {
        currCountdownValue = countdownValue;
        while (currCountdownValue > 0)
        {
            Debug.Log("Countdown: " + currCountdownValue);
            yield return new WaitForSeconds(1.0f);
            currCountdownValue--;
        }

        Debug.Log("End");
        alert.SetActive(true);
    }

    public void Meow()
    {
        StartCoroutine(StartCountdown());
    }

}

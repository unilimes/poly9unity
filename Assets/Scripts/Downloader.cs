﻿using System.Collections;
using UnityEngine;

public delegate void DownloadedObjCallback (string s,string a_url);
public delegate void DownloadedTextureCallback (Texture s,string a_url);
public class Downloader : MonoBehaviour
{ 
	static Downloader g_downloader;
	void Awake()
	{
		g_downloader = this;
	}



	public IEnumerator DownloadFile (string a_url, DownloadedObjCallback a_callback, DownloadedTextureCallback a_textureCallback)
	{
		if ((a_callback != null || a_textureCallback != null)) {
            //Texture2D tex;
            //tex = new Texture2D(1, 1, TextureFormat.DXT1, false);
            WWW www = new WWW (a_url);
            
            while (!www.isDone) {
               
				yield return www;
                //Debug.Log(www.isDone);

            }
           

            if (a_callback != null)
				a_callback (www.text,a_url);
			else if (a_textureCallback != null)

               // www.LoadImageIntoTexture(tex);
                a_textureCallback (www.texture, a_url);
			
		}
	}


	public static Downloader INSTANCE {
		get{
			return g_downloader;
		}
	}
}

﻿using UnityEngine;
using UnityEngine.UI;

public class PartsBtn : MonoBehaviour {

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(() => SwitchMaterial(transform));
    }

    public void SwitchMaterial(Transform tr)
    {
        HELPER.instance.ResetPartsBtn();
        transform.GetChild(1).gameObject.SetActive(true);
        Debug.Log(tr.GetSiblingIndex());
        HELPER.instance.currentMaterialBtn = HELPER.instance.regionNameHelper [tr.GetSiblingIndex()];
        HELPER.instance.ChangePartOnClick();
    }

}

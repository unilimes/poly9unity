﻿using UnityEngine;
using UnityEngine.UI;

public class VisibleTest : MonoBehaviour {
    [SerializeField]
    Image costomizeSlideBtn;
    [SerializeField]
    Sprite costomizeSlideUp;
    [SerializeField]
    Sprite costomizeSlideDown;
    [SerializeField]
    public void UpDownCostomizePanel(RectTransform panel)
    {
        if (panel.anchoredPosition.y == -56.5f)
        {
            panel.anchoredPosition = new Vector2(panel.anchoredPosition.x, -0.1f);
            costomizeSlideBtn.sprite = costomizeSlideDown;
        }
        else
        {
            panel.anchoredPosition = new Vector2(panel.anchoredPosition.x, -56.5f);
            costomizeSlideBtn.sprite = costomizeSlideUp;
        }
    }
}

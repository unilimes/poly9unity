﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class MainCameraController : MonoBehaviour
{

    [Header("TouchLimit")]
    public Transform target;
    public float xSpeed = 50;
    public float ySpeed = 50;
    public float yMinLimit = 5;
    public float yMaxLimit = 80;
    Touch touch;
    private float touchX, touchY;
    public Quaternion touchRot;
    public float constDistance;
    [SerializeField]
    Vector3 touchPos;

    public float perspectiveZoomSpeed = 0.5f;
    public float distance;
    public float zoomSmoothNumber;

    public float distanceZoomIn;
    public float distanceZoomOut;
    public bool needZoomIn;
    public float speed;

    public enum cameraSate { ZoomIn, ZoomOut, Zooming, };
    public cameraSate currentCamSate;

    public float TouchTime;
    public bool blockControl;
    public Material[] allMaterials;
    public List<Material> matWithAlpha;
    public int currentMatIndex;

    public string startName;
    [SerializeField]
    bool oneFinger;
    [SerializeField]
    bool blockRotateAndZoom;
    
    void Awake()
    {
        blockControl = false;
      
        currentCamSate = cameraSate.ZoomOut;
        distanceZoomOut = constDistance;
        touchY = 30;
        distance = constDistance;
    }

    
    void FixedUpdate()
    {
        HandleTouch();
    }

    void HandleTouch()
    {
        if (!blockControl)
        {
            foreach (Touch touch in Input.touches)
            {
                int id = touch.fingerId;

                if (EventSystem.current.IsPointerOverGameObject(id))
                {
                    return;  // UI
                }
            }

            if (HELPER.instance.currentOpenCunvas == HELPER.openCanvas.Customize || HELPER.instance.currentOpenCunvas == HELPER.openCanvas.View)
            {

               
                if (Input.touchCount == 1)
                {
                    Touch touchZero = Input.GetTouch(0);

                    if (touchZero.phase == TouchPhase.Began && !blockRotateAndZoom)
                    {
                        oneFinger = true;
                        Ray ray1 = Camera.main.ScreenPointToRay(touch.position);
                        RaycastHit hit1;

                        if (Physics.Raycast(ray1, out hit1))
                        {
                            Debug.Log("goname " + startName);
                            Debug.Log("hit " + hit1.transform.name);
                            startName = hit1.transform.name;
                        }

                    }
                    if (touchZero.phase == TouchPhase.Canceled || touchZero.phase == TouchPhase.Ended)
                    {
                        if (blockRotateAndZoom)
                        {
                            blockRotateAndZoom = false;
                        }
                        else
                        {
                            oneFinger = false;
                            if (HELPER.instance.currentOpenCunvas == HELPER.openCanvas.Customize)
                            {
                                Ray ray = Camera.main.ScreenPointToRay(touch.position);
                                RaycastHit hit;

                                if (Physics.Raycast(ray, out hit))
                                {
                                    Debug.Log("goname " + startName);
                                    Debug.Log("hit " + hit.transform.GetComponent<ModelCustomTag>().customTag);
                                    if (hit.transform.name == startName)
                                    {
                                        var uiDropdown = FindObjectOfType<UIDropdown>();
                                        uiDropdown.SelectOption(hit.transform.GetComponent<ModelCustomTag>().customTag);
                                        HELPER.instance.partName = hit.transform.GetComponent<ModelCustomTag>().customTag;
                                        HELPER.instance.currentRegionNameHelper = hit.transform.GetComponent<ModelCustomTag>().customTag;
                                        //  HELPER.instance.EnableOutline();
                                    }
                                }

                                //if (HELPER.instance.currentUserSate == HELPER.currentSate.Patterns)
                                //{
                                //    if (hit.transform.name == startName)
                                //    {
                                //       // ChangeAndPickMaterial();
                                //    }
                                //}
                                //if (HELPER.instance.currentUserSate == HELPER.currentSate.Finishes)
                                //{

                                //    //if (HELPER.instance.colorPicker.gameObject.activeSelf)
                                //    //{
                                //    //    HELPER.instance.DisableColorPicker();
                                //    //    HELPER.instance.DisableColorPickerScripts();
                                //    //}
                                //    //else
                                //    //{
                                //    //    HELPER.instance.EnableColorPickerScripts();
                                //    //}
                                //}
                            }
                        }
                    }
                   
                }
                if (Input.touchCount == 2)
                {
                    Touch touchZero = Input.GetTouch(0);
                    Touch touchOne = Input.GetTouch(1);

                    if (touchOne.phase == TouchPhase.Began)
                    {
                        oneFinger = false;
                    }

                    if (touchZero.phase == TouchPhase.Ended || touchZero.phase == TouchPhase.Canceled || touchOne.phase == TouchPhase.Ended || touchOne.phase == TouchPhase.Canceled)
                    {
                        blockRotateAndZoom = true;
                    }
                }


                if (oneFinger && !blockRotateAndZoom && Input.touchCount == 1)
                {
                    touch = Input.GetTouch(0);
                    touchX += touch.deltaPosition.x * xSpeed * 0.02f;
                    touchY -= touch.deltaPosition.y * ySpeed * 0.02f;

                }

                if (!oneFinger && !blockRotateAndZoom && Input.touchCount == 2)
                {
                    Touch touchZero = Input.GetTouch(0);
                    Touch touchOne = Input.GetTouch(1);
                    Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                    Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
                    float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                    float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
                    float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
                    distance += deltaMagnitudeDiff * perspectiveZoomSpeed;
                    distance = Mathf.Clamp(distance, 0.1f, 90f);
                }

            }
            //if (HELPER.instance.currentOpenCunvas == HELPER.openCanvas.View)
            //{
            //    for (var i = 0; i < Input.touchCount; ++i)
            //    {
            //        if (Input.GetTouch(i).phase == TouchPhase.Began)
            //        {
            //            if (Input.GetTouch(i).tapCount == 2)
            //            {
            //                if (currentCamSate == cameraSate.Zooming)
            //                    return;

            //                if (currentCamSate == cameraSate.ZoomOut)
            //                {
            //                    StartCoroutine(ZoomIn(distanceZoomOut, distanceZoomIn, 0.8f));
            //                }

            //                else
            //                {
            //                    StartCoroutine(ZoomOut(distanceZoomIn, distanceZoomOut, 0.8f));
            //                }

            //            }
            //        }
            //    }

            //    if (Input.touchCount == 1)
            //    {
            //        touch = Input.GetTouch(0);
            //        touchX += touch.deltaPosition.x * xSpeed * 0.02f;
            //        touchY -= touch.deltaPosition.y * ySpeed * 0.02f;
            //    }

            //}

            touchY = ClampTouchAngle(touchY, yMinLimit, yMaxLimit);
            touchRot = Quaternion.Euler(touchY, touchX, 0);
            var vTemp = new Vector3(0.0f, 0.0f, -distance * zoomSmoothNumber);
            touchPos = touchRot * vTemp + target.position;
            transform.position = touchPos;
            transform.rotation = touchRot;
        }
    }
    public void ChangeAndPickMaterial()
    {
        Ray ray = Camera.main.ScreenPointToRay(touch.position);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {

            var uiDropdown = FindObjectOfType<UIDropdown>();
           
            uiDropdown.ChangeItem(hit.transform.name);
            HELPER.instance.partName = hit.transform.name;

            Renderer rend = hit.transform.GetComponent<Renderer>();
            MeshCollider meshCollider = hit.collider as MeshCollider;

            if (rend == null || rend.sharedMaterial == null || rend.sharedMaterial.mainTexture == null || meshCollider == null)
                return;
            Vector2 pixelUV = hit.textureCoord;

            allMaterials = hit.transform.GetComponent<MeshRenderer>().materials;
           // HELPER.instance.EnableOutline();

            if (allMaterials.Length == 1)
            {
                Debug.Log("Only 1 Mat");
                currentMatIndex = 1;
                HELPER.instance.matIndex = currentMatIndex;
                HELPER.instance.DisableControlsForTextLogos();
                HELPER.instance.DisableTransparencyForMaterial();

            }
            if (allMaterials.Length > 1 && currentMatIndex < allMaterials.Length - 1)
            {

                currentMatIndex++;
                HELPER.instance.matIndex = currentMatIndex;
                HELPER.instance.EnableControlsForTextLogos();
                HELPER.instance.DisableTransparencyForMaterial();
                HELPER.instance.EnableTransparencyForMaterial();
            }
            else if (currentMatIndex == allMaterials.Length - 1)
            {
                currentMatIndex = 1;
                HELPER.instance.matIndex = currentMatIndex;
                HELPER.instance.EnableControlsForTextLogos();
                HELPER.instance.DisableTransparencyForMaterial();
                HELPER.instance.EnableTransparencyForMaterial();
            }

            else
            {
                HELPER.instance.DisableTransparencyForMaterial();
                HELPER.instance.DisableControlsForTextLogos();
                Debug.Log("6");
            }



        }

    }

    float ClampTouchAngle(float angle, float min, float max)
    {
        if (angle < -360)
        {
            angle += 360;
        }
        if (angle > 360)
        {
            angle -= 360;
        }
        return Mathf.Clamp(angle, min, max);
    }

    public static float ZoomLimit(float dist, float min, float max)

    {

        if (dist < min)

            dist = min;

        if (dist > max)

            dist = max;

        return dist;

    }

    IEnumerator ZoomIn(float v_start, float v_end, float duration)
    {
        currentCamSate = cameraSate.Zooming;
        float elapsed = 0.0f;
        while (elapsed < duration)
        {
            distance = Mathf.Lerp(v_start, v_end, elapsed / duration);
            elapsed += Time.deltaTime;
            yield return null;
        }
        distance = v_end;
        currentCamSate = cameraSate.ZoomIn;
    }

    IEnumerator ZoomOut(float v_start, float v_end, float duration)
    {
        currentCamSate = cameraSate.Zooming;
        float elapsed = 0.0f;
        while (elapsed < duration)
        {
            distance = Mathf.Lerp(v_start, v_end, elapsed / duration);
            elapsed += Time.deltaTime;
            yield return null;
        }
        distance = v_end;
        currentCamSate = cameraSate.ZoomOut;

    }

    public IEnumerator CustomZoomOut(float v_start, float v_end, float duration)
    {
        currentCamSate = cameraSate.Zooming;
        float elapsed = 0.0f;
        while (elapsed < duration)
        {
            distance = Mathf.Lerp(v_start, v_end, elapsed / duration);
            elapsed += Time.deltaTime;
            yield return null;
        }
        distance = v_end;
        currentCamSate = cameraSate.ZoomOut;

    }
    
}

﻿using UnityEngine;
using UnityEngine.UI;

public class FilterBtn : MonoBehaviour {

    Sprite originSp;
    public Sprite checkBoxSp;
    public Image btn;
    public bool isManuf;

    private void Start()
    {
        originSp = btn.sprite;
    }

    public void SwitchBtnSp()
    {
        if (btn.sprite == checkBoxSp)
        {
            btn.sprite = originSp;
            if (isManuf)
            {
                btn.transform.GetChild(0).GetComponent<Text>().text = "A";
            }
            
        }
        else
        {
            btn.sprite = checkBoxSp;
            if (isManuf)
            {
                btn.transform.GetChild(0).GetComponent<Text>().text = "";
            }
        }
    }
}

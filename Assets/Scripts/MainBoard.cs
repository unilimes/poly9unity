﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainBoard : MonoBehaviour {

   
    public List<Boards> boards;
    [SerializeField]
    bool firstCanvas;
    [SerializeField]
    bool secondCanvas;
    public bool boardWithHeader;

    public Sprite mainSp;
    public string mainTitle;
    public string mainDescription;


    

    public void OpenBoardDetailCanvas()
    {
        firstCanvas = false;
        secondCanvas = false;

        if (boardWithHeader)
        {
            HELPER.instance.detailBoard.GetChild(0).GetComponent<UIGradient>().enabled = false;
            HELPER.instance.detailBoard.GetChild(0).GetComponent<Image>().sprite = transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite;
            HELPER.instance.detailBoard.GetChild(0).GetChild(1).GetComponent<Text>().text = mainDescription;
            HELPER.instance.detailBoard.GetChild(0).GetChild(2).GetComponent<Text>().text = mainTitle;
        }
        else
        {
            HELPER.instance.detailBoard.GetChild(0).GetComponent<UIGradient>().enabled = true;
            HELPER.instance.detailBoard.GetChild(0).GetComponent<Image>().sprite = null;
            HELPER.instance.detailBoard.GetChild(0).GetChild(1).GetComponent<Text>().text = mainDescription;
            HELPER.instance.detailBoard.GetChild(0).GetChild(2).GetComponent<Text>().text = mainTitle;
        }

        for (int i = 0; i < boards.Count; i++)
        {
            if (boards[i].gridType == "3DProduct")
            {
                var btn = Instantiate(HELPER.instance.productBtn);
                if (!firstCanvas)
                {
                    btn.transform.SetParent(HELPER.instance.boardDetailsContent0, false);
                    btn.GetComponent<ProductButton>().productID = boards[i].id;
                    btn.GetComponent<ProductButton>().productType = "3DProduct";
                    firstCanvas = true;
                    secondCanvas = false;
                }
                else if (!secondCanvas)
                {
                    btn.transform.SetParent(HELPER.instance.boardDetailsContent1, false);
                    btn.GetComponent<ProductButton>().productID = boards[i].id;
                    btn.GetComponent<ProductButton>().productType = "3DProduct";
                    secondCanvas = true;
                    firstCanvas = false;
                }

            }
            else if (boards[i].gridType == "product" || boards[i].gridType == "2DProduct")
            {
                var btn = Instantiate(HELPER.instance.productBtn);
                if (!firstCanvas)
                {
                    btn.transform.SetParent(HELPER.instance.boardDetailsContent0, false);
                    btn.GetComponent<ProductButton>().productID = boards[i].id;
                    btn.GetComponent<ProductButton>().productType = "Product2D";
                    firstCanvas = true;
                    secondCanvas = false;
                }
                else if (!secondCanvas)
                {
                    btn.transform.SetParent(HELPER.instance.boardDetailsContent1, false);
                    btn.GetComponent<ProductButton>().productID = boards[i].id;
                    btn.GetComponent<ProductButton>().productType = "Product2D";
                    secondCanvas = true;
                    firstCanvas = false;
                }
            }

            else if (boards[i].gridType == "color")
            {
                var btn = Instantiate(HELPER.instance.conceptBtn);
                if (!firstCanvas)
                {
                    btn.transform.SetParent(HELPER.instance.boardDetailsContent0, false);
                    btn.GetComponent<ConceptButton>().hex = boards[i].colorCode;
                    btn.GetComponent<ConceptButton>().title = boards[i].title;
                    btn.GetComponent<ConceptButton>().productType = "Color";
                    btn.GetComponent<ConceptButton>().GetColor();
                    firstCanvas = true;
                    secondCanvas = false;
                }
                else if (!secondCanvas)
                {
                    btn.transform.SetParent(HELPER.instance.boardDetailsContent1, false);
                    btn.GetComponent<ConceptButton>().hex = boards[i].colorCode;
                    btn.GetComponent<ConceptButton>().title = boards[i].title;
                    btn.GetComponent<ConceptButton>().productType = "Color";
                    btn.GetComponent<ConceptButton>().GetColor();
                    secondCanvas = true;
                    firstCanvas = false;
                }
            }
            else if (boards[i].gridType == "concept")
            {
                var btn = Instantiate(HELPER.instance.conceptBtn);
                if (!firstCanvas)
                {
                    btn.transform.SetParent(HELPER.instance.boardDetailsContent0, false);
                    btn.GetComponent<ConceptButton>().title = boards[i].title;
                    btn.GetComponent<ConceptButton>().url = boards[i].url;
                    btn.GetComponent<ConceptButton>().hex = boards[i].fileType;
                    btn.GetComponent<ConceptButton>().description = boards[i].description;
                    btn.GetComponent<ConceptButton>().productType = "Concept";
                    
                    firstCanvas = true;
                    secondCanvas = false;
                }
                else if (!secondCanvas)
                {
                    btn.transform.SetParent(HELPER.instance.boardDetailsContent1, false);
                    btn.GetComponent<ConceptButton>().title = boards[i].title;
                    btn.GetComponent<ConceptButton>().url = boards[i].url;
                    btn.GetComponent<ConceptButton>().hex = boards[i].fileType;
                    btn.GetComponent<ConceptButton>().description = boards[i].description;
                    btn.GetComponent<ConceptButton>().productType = "Concept";
                   
                    secondCanvas = true;
                    firstCanvas = false;
                }
            }
        }

        HELPER.instance.detailBoard.gameObject.SetActive(true);

        HELPER.instance.detailBoard.GetComponent<SetHeight>().CulculateHeight();

    }
}

﻿using System.Collections.Generic;
using UnityEngine;

public class TextName : MonoBehaviour {

    public List<int> modelsCount;
    public List<string> modelName;
    public List<string> PartsName;
    public List<string> PartsMaterialsName;
    public List<Material> SavedMaterials;
    public List<string> PartNameWereCustomMaterialSave;

    public string path = "save.txt?tag=";
    public string loadModelName;
    public int loadIndex;
    public bool needDelete;

    void Start () {

        if (needDelete)
        {
            ES2.Delete("save.txt");
        }

        GameObject instance = Instantiate(Resources.Load("DefaultModelsPref/" + loadModelName, typeof(GameObject))) as GameObject;
    }
	
	
	void Update ()
    {

        if (Input.GetMouseButtonDown(0))
        {
            SaveModel();
        }
        if (Input.GetMouseButtonDown(1))
        {
            LoadModel(loadIndex);
        }

            
    }

    void SaveModel()
    {

        modelsCount.Clear();
        modelName.Clear();
        PartsName.Clear();
        PartsMaterialsName.Clear();
        SavedMaterials.Clear();
        PartNameWereCustomMaterialSave.Clear();

        if (ES2.Exists(path + "modelsCount"))
        {
            modelsCount = ES2.LoadList<int>(path + "modelsCount");
        }


        GameObject go = GameObject.FindWithTag("CurrentObjParent");
        Debug.Log("save " + path + "modelsCount");
        modelsCount.Add(modelsCount.Count);
        ES2.Save(modelsCount, path + "modelsCount");



        string goNameString = go.name;
        if (goNameString.Contains("("))
        {
            int dotIndex = goNameString.IndexOf("(");
            if (dotIndex >= 0)
            {
                goNameString = goNameString.Substring(0, dotIndex);
            }


            modelName.Add(goNameString);
        }
        else
        {
            modelName.Add(go.name);
        }


        
        ES2.Save(modelName, path + modelsCount.Count + "modelName");
        Debug.Log(modelsCount.Count);

        for (int i = 0; i < go.transform.childCount; i++)
        {
            PartsName.Add(go.transform.GetChild(i).name);
            ES2.Save(PartsName, path + modelsCount.Count + "modelPartsName");
           
            //если у материала есть символ ) или ( - удаляем все после пробла и сохр
            string testString = go.transform.GetChild(i).GetComponent<MeshRenderer>().materials[0].name;
            if (testString.Contains(" "))
            {
                int dotIndex = testString.IndexOf(" ");
                if (dotIndex >= 0)
                {
                    testString = testString.Substring(0, dotIndex + 1);
                }


                PartsMaterialsName.Add(testString);
            }
            else
            {
                PartsMaterialsName.Add(go.transform.GetChild(i).GetComponent<MeshRenderer>().materials[0].name);
            }

   // сохр        
            ES2.Save(PartsMaterialsName, path + modelsCount.Count + "modelPartsMaterialsName");
 //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            if (go.transform.GetChild(i).GetComponent<MeshRenderer>().materials.Length > 1)
            {

                PartNameWereCustomMaterialSave.Add(go.transform.GetChild(i).name);

                ES2.Save(PartNameWereCustomMaterialSave, path + modelsCount.Count + "PartNameWereCustomMaterialSave");

                for (int j = 1; j < go.transform.GetChild(i).GetComponent<MeshRenderer>().materials.Length; j++)
                {
                    SavedMaterials.Add(go.transform.GetChild(i).GetComponent<MeshRenderer>().materials[j]);

                    ES2.Save(SavedMaterials, path + modelsCount.Count + go.transform.GetChild(i).name + "modelSavedMaterials");

                }

               
            }

        }

        // instatiate (button (i))
      
    }

    void LoadModel(int index)
    {
        modelsCount.Clear();
        modelName.Clear();
        PartsName.Clear();
        PartsMaterialsName.Clear();
        SavedMaterials.Clear();
        PartNameWereCustomMaterialSave.Clear();

        Debug.Log("load " + path + "modelsCount");

        if (ES2.Exists(path + "modelsCount"))
        {
            modelsCount = ES2.LoadList<int>(path + "modelsCount");
           

        }

        if (ES2.Exists(path + index + "modelName"))
        {
            modelName = ES2.LoadList<string>(path + index + "modelName");


        }
        if (ES2.Exists(path + index + "modelPartsName"))
        {
            PartsName = ES2.LoadList<string>(path + index + "modelPartsName");


        }

        if (ES2.Exists(path + index + "modelPartsMaterialsName"))
        {
            PartsMaterialsName = ES2.LoadList<string>(path + index + "modelPartsMaterialsName");


        }



        if (ES2.Exists(path + index + "PartNameWereCustomMaterialSave"))
        {

            PartNameWereCustomMaterialSave = ES2.LoadList<string>(path + index + "PartNameWereCustomMaterialSave");



            for (int i = 0; i < PartNameWereCustomMaterialSave.Count; i++)
            {
                //Debug.Log(PartNameWereCustomMaterialSave[i]);
                if (ES2.Exists(path + index + PartNameWereCustomMaterialSave[i] + "modelSavedMaterials"))
                    {
                            SavedMaterials = ES2.LoadList<Material>(path + index + PartNameWereCustomMaterialSave[i] + "modelSavedMaterials");
                    }

            }

         }

        

    }


}

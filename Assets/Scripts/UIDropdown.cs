﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIDropdown : MonoBehaviour {
    
    public Dropdown m_dropdown;
    public List<string> regionName;

    public List<string> matID;

    public CheckItemsCount cik;
    public CheckItemsCount cik2;

    public IEnumerator FillUiDropdown()
    {
        m_dropdown = gameObject.GetComponent<Dropdown>();
        HELPER.instance.currentRegionNameHelper = m_dropdown.options[m_dropdown.value].text;
        HELPER.instance.currentDropdownIndex = m_dropdown.value;
        
        matID.Clear();

        if (ES2.Exists(HELPER.instance.currentProductID + m_dropdown.options[m_dropdown.value].text))
        {
            matID = ES2.LoadList<string>(HELPER.instance.currentProductID + m_dropdown.options[m_dropdown.value].text);
            var md = FindObjectOfType<MaterialDropdown>();
            md.matID.Clear();
            
            for (int i = 0; i < matID.Count; i++)
            {
                md.matID.Add(matID[i]);
            }

            md.AddLoadName();
        }

        cik.CheckItems();
        cik2.CheckItems();
        m_dropdown.onValueChanged.AddListener(delegate {

            HELPER.instance.currentRegionNameHelper = m_dropdown.options[m_dropdown.value].text;
            HELPER.instance.currentDropdownIndex = m_dropdown.value;
            matID.Clear();
           
            if (ES2.Exists(HELPER.instance.currentProductID + m_dropdown.options[m_dropdown.value].text))
            {
                matID = ES2.LoadList<string>(HELPER.instance.currentProductID + m_dropdown.options[m_dropdown.value].text);

                var md = FindObjectOfType<MaterialDropdown>();
                md.matID.Clear();
                for (int i = 0; i < matID.Count; i++)
                {
                    md.matID.Add(matID[i]);
                }

                md.AddLoadName();
                cik2.CheckItems();
                HELPER.instance.Changer();
                HELPER.instance.ChangeColorPickerRegion();
            }

        });
       
        yield return 0;
    }

    public void AddLoadName()
    {
        m_dropdown = gameObject.GetComponent<Dropdown>();
        m_dropdown.ClearOptions();

        
        for (int i = 0; i < regionName.Count; i++)
        {
            if (i == 0)
            {
                AddItemToDropdown(regionName[i],true);
            }
            else
            {
                AddItemToDropdown(regionName[i]);
            }
        }

        m_dropdown.RefreshShownValue();
        StartCoroutine(FillUiDropdown());
    }
    public void SelectOption(string text) {

        int option_id = 0;
        foreach (Dropdown.OptionData data in m_dropdown.options) {
            if (data.text == text) {
                m_dropdown.value = option_id;
                m_dropdown.RefreshShownValue();
            }
            option_id++;
        }
    }

    public void AddItemToDropdown(string item, bool select_on_add=false) {
        m_dropdown.options.Add(new Dropdown.OptionData(item));

        if (select_on_add) {
            SelectOption(item);
        }
    }

    public void AddItemToDropdownAndSelect(string item) {
        AddItemToDropdown(item, true);
    }

    public void ChangeItem(string value)
    {
        m_dropdown.options[m_dropdown.value].text = value;
        m_dropdown.RefreshShownValue();
    }
}

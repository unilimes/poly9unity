﻿using System.Collections.Generic;
using UnityEngine;
using VoxelBusters.RuntimeSerialization;

public class SaveTestController : MonoBehaviour {
    private const string kSerializationIDForPrefs = "primitive-generator-prefs";
    public List <GameObject> go;
   


    void Update()
    {

        if (Input.GetKeyDown(KeyCode.I))
        {

            RSManager.Serialize(go, kSerializationIDForPrefs, eSaveTarget.FILE_SYSTEM);
           
        }

        if (Input.GetKeyDown(KeyCode.Y))
        {
            RSManager.Deserialize<List<GameObject>>(kSerializationIDForPrefs);
          
        }

    }
}

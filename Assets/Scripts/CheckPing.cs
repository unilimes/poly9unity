﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class CheckPing : MonoBehaviour {

    public string url = "http://clgupta.polynine.co";

    public Text text;

    private void Start()
    {
        CheckPingStatus(url);
    }

    public void CheckPingStatus(string url)
    {
        StartCoroutine(StartPing(url));
    }

    IEnumerator StartPing(string ip)
    {
        WaitForSeconds f = new WaitForSeconds(1f);
        Ping p = new Ping(ip);
        while (p.isDone == false)
        {
            yield return f;
        }
        PingFinished(p);
    }


    public void PingFinished(Ping p)
    {
        text.text = p.time.ToString();
        CheckPingStatus(url);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class testSerialization : MonoBehaviour {

    private const string kSerializationIDForFiles = "primitive-generator-files";



    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.S))
        {
            Debug.Log("save");
            Serialize();
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            Debug.Log("load");
            Deserialize();
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            Debug.Log("delete");
           // RSManager.RemoveAll();


        }


    }


    public void Serialize()
    {
        // For WebPlayer, we are saving data to player prefs. 
        // Note: We are using unique identifiers for player prefs and file based saving.
#if (UNITY_WEBPLAYER || UNITY_WEBGL)
			RSManager.Serialize<PrimitiveGenerator>(this, kSerializationIDForPrefs, eSaveTarget.PLAYER_PREFS);
#else
       // savedModelsList = new List<GameObject>(GameObject.FindGameObjectsWithTag("Player"));

        //for (int i = 0; i < savedModelsList.Count; i++)
        //{
            
        //    Destroy(savedModelsList[i].GetComponent<AddTextToTexture>());
        //    Destroy(savedModelsList[i].GetComponent<ColorPickerTester>());

        //}

        //RSManager.Serialize(savedModelsList, kSerializationIDForFiles, eSaveTarget.FILE_SYSTEM);
#endif
    }

    public void Deserialize()
    {
       // Clear();

        // For WebPlayer, we are reading data from player prefs. 
        // Note: We are using unique identifiers for player prefs and file based saving.
#if (UNITY_WEBPLAYER || UNITY_WEBGL)
			RSManager.Deserialize<PrimitiveGenerator>(kSerializationIDForPrefs);
#else
       // var lol = RSManager.Deserialize<List<GameObject>>(kSerializationIDForFiles);
       // Instantiate(lol[0]);
        //Debug.Log(lol.Count);
#endif
    }

    public void Clear()
    {
        //if (savedModelsList == null)
        //    return;

        //foreach (GameObject _spawnedGO in savedModelsList)
        //    DestroyImmediate(_spawnedGO);

        //savedModelsList.Clear();
    }

}

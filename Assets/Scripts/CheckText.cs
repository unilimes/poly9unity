﻿using UnityEngine;
using UnityEngine.UI;


public class CheckText : MonoBehaviour {

    public InputField inputF;
    public Image pHolder;

    [SerializeField]
    bool isForgotPassScreen;

	public void ChechIfText () {

        if (inputF.text.Length > 1)
        {
            
            pHolder.color = Color.black;
        }
        else if (inputF.text.Length == 0) 
        {
            pHolder.color = Color.white;
            
        }

        if (isForgotPassScreen)
        {
            HELPER.instance.ResetForgotEmailIncorrect();
        }
        else
        {
            HELPER.instance.ResetIncorrect();
        }
      

    }

   
}

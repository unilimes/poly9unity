﻿using UnityEngine;
using UnityEngine.UI;

public class MaterialBtn : MonoBehaviour {

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(() => SwitchMaterial(transform));
    }

    public void SwitchMaterial(Transform tr)
    {
        HELPER.instance.ResetMaterialsBtn();
        transform.GetChild(1).gameObject.SetActive(true);
        Debug.Log(tr.GetSiblingIndex());
        HELPER.instance.currentPartBtn = transform.GetChild(2).GetComponent<Text>().text;
        HELPER.instance.ChangeMaterialOnClick();
    }

}

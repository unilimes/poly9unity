﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SynhBtn : MonoBehaviour {

    public RectTransform btn;

    public void SwitchSyncBtn()
    {
        if (btn.anchoredPosition.x == 9f)
        {
            btn.anchoredPosition = new Vector2(-10f, btn.anchoredPosition.y);
        }
        else
        {
            btn.anchoredPosition = new Vector2(9f, btn.anchoredPosition.y);
        }
    }
}

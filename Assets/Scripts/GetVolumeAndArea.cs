﻿using UnityEngine;
using UnityEngine.UI;



public class GetVolumeAndArea : MonoBehaviour {

    Mesh mesh;
   
    Collider m_Collider;
    Vector3 m_Size;
    public Text resText;
    public Transform resPanel;

    private void OnEnable()
    {
        //mesh = GetComponent<MeshFilter>().sharedMesh;
        //float volume = VolumeOfMesh(mesh);
        //float area = AreaOfMesh(mesh);
        //resPanel.gameObject.SetActive(true);
        //resText.text = string.Empty;
        //resText.text = "Volume = " + volume + " " + "Area " + area;
    }

    void OnDisable()
    {
        //resPanel.gameObject.SetActive(false);
        
    }

    //void Start ()
    //{
       
       
    //   // string msg = "ПЛОЩАДЬ МЕША " + volume + " М В КВАДРАТЕ";
    //   // Debug.Log(msg);

        
    //   // float area = AreaOfMesh(mesh);
    //   // Debug.Log("The area of the mesh is " + area + " square units.");
    //}

    public float AreaOfTriangle(Vector3 p1, Vector3 p2, Vector3 p3)
    {
        float a = Vector3.Distance(p1, p2);
        float b = Vector3.Distance(p2, p3);
        float c = Vector3.Distance(p3, p1);
        float p = 0.5f * (a + b + c);
        float s = Mathf.Sqrt(p * (p - a) * (p - b) * (p - c));
        return s;
    }

    public float AreaOfMesh(Mesh mesh)
    {
        float area = 0f;

        Vector3[] vertices = mesh.vertices;
        int[] triangles = mesh.triangles;

        for (int i = 0; i < mesh.triangles.Length; i += 3)
        {
            Vector3 p1 = vertices[triangles[i + 0]];
            Vector3 p2 = vertices[triangles[i + 1]];
            Vector3 p3 = vertices[triangles[i + 2]];
            area += AreaOfTriangle(p1, p2, p3);
        }

        return area;
    }
   

    public float SignedVolumeOfTriangle(Vector3 p1, Vector3 p2, Vector3 p3)
    {
        float v321 = p3.x * p2.y * p1.z;
        float v231 = p2.x * p3.y * p1.z;
        float v312 = p3.x * p1.y * p2.z;
        float v132 = p1.x * p3.y * p2.z;
        float v213 = p2.x * p1.y * p3.z;
        float v123 = p1.x * p2.y * p3.z;
        return (1.0f / 6.0f) * (-v321 + v231 + v312 - v132 - v213 + v123);
    }
    public float VolumeOfMesh(Mesh mesh)
    {
        float volume = 0;
        Vector3[] vertices = mesh.vertices;
        int[] triangles = mesh.triangles;
        for (int j = 0; j < mesh.triangles.Length; j += 3)
     {
            Vector3 p1 = vertices[triangles[j + 0]];
            Vector3 p2 = vertices[triangles[j + 1]];
            Vector3 p3 = vertices[triangles[j + 2]];
            volume += SignedVolumeOfTriangle(p1, p2, p3);
        }

        volume *= GetComponent<MeshFilter>().gameObject.transform.localScale.x * GetComponent<MeshFilter>().gameObject.transform.localScale.y * GetComponent<MeshFilter>().gameObject.transform.localScale.z;
        return volume;
    }
}

﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Board : MonoBehaviour {

    public string headerURL;
    public string hex;
    public string id;
    Color newCol;

    void Start()
    {

        if (headerURL != "")
        {
           var text1 = headerURL;
           int dotIndex = text1.LastIndexOf('.');
           if (dotIndex >= 0)
           {
              text1 = text1.Substring(dotIndex + 1);
           }

            if (text1 == "png" || text1 == "jpeg" || text1 == "jpg")
            {
                transform.GetComponent<Image>().color = Color.white;
                StartCoroutine(LoadHeader());
                transform.GetChild(0).gameObject.SetActive(false);
            }
            else
            {
                transform.GetComponent<Image>().color = Color.white;
            }

			
        }

        else if (PlayerPrefs.HasKey(id + "style_Thumbnail_Img"))
        {
			transform.GetComponent<Image> ().color = Color.white;
            headerURL = PlayerPrefs.GetString(id + "style_Thumbnail_Img");
            StartCoroutine(LoadHeader());
            transform.GetChild(0).gameObject.SetActive(false);
        }
        else
        {
            transform.GetComponent<Image>().sprite = null;
            GetColor();
            transform.GetChild(0).gameObject.SetActive(false);
        }
    }

    IEnumerator LoadHeader()
    {
        yield return new WaitForSeconds(2f);
        Texture2D tex;
        tex = new Texture2D(1, 1, TextureFormat.DXT1, false);
        using (WWW www = new WWW(headerURL))
        {
            yield return www;
            www.LoadImageIntoTexture(tex);

            var mySprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);

            transform.GetComponent<Image>().sprite = mySprite;
         
        }
    }

    public void GetColor()
    {
        hex = "#" + hex;
        if (ColorUtility.TryParseHtmlString(hex, out newCol))
        {
            GetComponent<Image>().color = newCol;
        }
    }
}

﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class FinishButton : MonoBehaviour {

   
    public List<string> fBaseMatID;
    public Transform icon;
    public string finishID;
    public bool notChangeIcon;
    [SerializeField]
    int siblingIndex;
    [SerializeField]
    int fBaseMatIDCount;
    [SerializeField]
    string finishName;
    [SerializeField]
    Sprite logoImage;
    public bool isColor;

    private void Start()
    {
        icon = transform.GetChild(1).GetComponent<Transform>();
    }

    public void Click()
	{
        var sm = FindObjectOfType<SceneManager>();
        for (int i = 0; i < sm.m_finishUIParent.childCount; i++)
        {
            for (int j = 0; j < HELPER.instance.currentFinishesID.Count; j++)
            {
                HELPER.instance.currentFinishesID[HELPER.instance.currentDropdownIndex] = finishID;
            }
        }

        for (int i = 0; i < sm.m_finishUIParent.childCount; i++)
        {
            if (sm.m_finishUIParent.GetChild(i).GetChild(0).GetComponent<FinishButton>().finishID != finishID)
            {
                sm.m_finishUIParent.GetChild(i).GetChild(0).GetChild(1).gameObject.SetActive(false);
            }
        }

        var parent = transform.parent;
        sm.LoadFinish(parent.GetChild(1).GetComponent<Text>().text);
        icon.gameObject.SetActive(true);
       
        
	}

    public void SaveData()
    {
        siblingIndex = transform.parent.GetSiblingIndex();
        finishName = transform.parent.name;
        PlayerPrefs.SetString("FinishBtn" + siblingIndex, finishID);
        PlayerPrefs.SetString("FinishName" + siblingIndex, finishName);
        PlayerPrefs.SetInt("fBaseMatIDCount" + siblingIndex, fBaseMatID.Count);
        ES2.Save(transform.GetChild(0).GetComponent<Image>().sprite, siblingIndex + "logoImage");
        for (int i = 0; i < fBaseMatID.Count; i++)
        {
            PlayerPrefs.SetString("fBaseMatID" + i + siblingIndex, fBaseMatID[i]);
        }
    }

    public void LoadData(int index)
    {
        fBaseMatID.Clear();

        Debug.Log("Load " + index );
        finishID = PlayerPrefs.GetString("FinishBtn" + index);
        fBaseMatIDCount = PlayerPrefs.GetInt("fBaseMatIDCount" + index);

        for (int i = 0; i < fBaseMatIDCount; i++)
        {
            var id = PlayerPrefs.GetString("fBaseMatID" + i + index);
            fBaseMatID.Add(id);
        }
        finishName = PlayerPrefs.GetString("FinishName" + index);
        transform.parent.name = finishName;
        var parent = transform.parent;
        parent.GetChild(1).GetComponent<Text>().text = finishName;
        logoImage = ES2.Load<Sprite>(index + "logoImage");
        transform.GetChild(0).GetComponent<Image>().sprite = logoImage;
    }



}

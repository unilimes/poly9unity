﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModelButton : MonoBehaviour {
   
    //public List<int> polygon_GroupIDCount;
    public string product_Model_Source_File;
    public long date;
    public string product_id;
    public string product_StyleID;
  //  public string startFinishID;
    public string style_Thumbnail_Img;
    public string style_price;
    public string stylesCount;
    public string styleDescription;
    public string prName;
    public List<string> regionBaseMaterialID;

    public List<string> matIDInThisModel;
    [SerializeField]
    List<string> tempMatList;
   
    public List<string> regionNameModelBtn;

    public List<string> finishesId;

    [Header("Styles")]

    public List<string> style_Thumbnail_ImgList;
    public List<string> style_priceList;
    public List<string> styleDescriptionList;
    public List<string> styleNameList;
    [SerializeField]
    int counter;

    public bool modelIsSave;

    private void Start()
    {
        if (HELPER.instance.isOfflineMode)
        {
            var siblingIndex = transform.parent.GetSiblingIndex();
            product_id = PlayerPrefs.GetString(siblingIndex + "product_id");

            regionNameModelBtn = ES2.LoadList<string>(siblingIndex + "regionNameModelBtn");
            transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = ES2.Load<Sprite>(siblingIndex + "image");
        }


       
        if (PlayerPrefs.HasKey(product_id + "style_Thumbnail_Img"))
        {
            var str = PlayerPrefs.GetString(product_id + "style_Thumbnail_Img");
            style_Thumbnail_Img = str;
        }

        if (PlayerPrefs.HasKey(product_id + "style_Description"))
        {
            styleDescription = PlayerPrefs.GetString(product_id + "style_Description");

        }

        if (PlayerPrefs.HasKey(product_id + "style_ID"))
        {
            product_StyleID = PlayerPrefs.GetString(product_id + "style_ID");
        }

        if (PlayerPrefs.HasKey(product_id + "style_price"))
        {
            var str = PlayerPrefs.GetString(product_id + "style_price");
            style_price = str;

            if (int.Parse(style_price) <= 0 || style_price == " ")
            {
                transform.GetChild(6).gameObject.SetActive(false);
                transform.GetChild(2).GetComponent<Text>().text = "";
            }
            else
            {
                transform.GetChild(2).GetComponent<Text>().text = style_price;
            }

        }

        else
        {
            transform.GetChild(6).gameObject.SetActive(false);
            transform.GetChild(2).GetComponent<Text>().text = "";
        }

        if (PlayerPrefs.HasKey(product_id + "styles"))
        {
            var str = PlayerPrefs.GetString(product_id + "styles");
            stylesCount = str;

            var getInt = int.Parse(stylesCount);

            getInt--;
            if (getInt > 1)
            {
                transform.GetChild(4).GetComponent<Text>().text = getInt.ToString();
                transform.GetChild(3).GetComponent<Text>().text = "Styles";
            }
            else if (getInt == 1)
            {
                transform.GetChild(4).GetComponent<Text>().text = getInt.ToString();
                transform.GetChild(3).GetComponent<Text>().text = "Style";
            }
            else if (getInt == 0)
            {
                transform.GetChild(4).GetComponent<Text>().text = "";
                transform.GetChild(5).gameObject.SetActive(false);
                transform.GetChild(3).GetComponent<Text>().text = "";
            }
         }

        if (PlayerPrefs.HasKey(product_id + "style_Name"))
        {
            var str = PlayerPrefs.GetString(product_id + "style_Name");
            prName = str;
            transform.GetChild(1).GetComponent<Text>().text = prName;
        }


        for (int i = 0; i < regionNameModelBtn.Count; i++)
        {
            if (ES2.Exists(product_id + regionNameModelBtn[i]))
            {
                tempMatList = ES2.LoadList<string>(product_id + regionNameModelBtn[i]);
            }

            for (int j = 0; j < tempMatList.Count; j++)
            {
                matIDInThisModel.Add(tempMatList[j]);
            }

            tempMatList.Clear();
        }

        for (int i = 0; i < regionNameModelBtn.Count; i++)
        {
            if (PlayerPrefs.HasKey(product_id + "finishes_ID" + i))
            {
                var finishID = PlayerPrefs.GetString(product_id + "finishes_ID" + i);
                finishesId.Add(finishID);
            }
           
        }

        if (ES2.Exists(product_id + "time"))
        {
            date = ES2.Load<long>(product_id + "time");
        }
       

        // Styles
        if (ES2.Exists(product_id + "style_Thumbnail_ImgList"))
        {
            style_Thumbnail_ImgList = ES2.LoadList<string>(product_id + "style_Thumbnail_ImgList");
        }

        if (ES2.Exists(product_id + "style_NameList"))
        {
            styleNameList = ES2.LoadList<string>(product_id + "style_NameList");
        }

        if (ES2.Exists(product_id + "style_DescriptionList"))
        {
            styleDescriptionList = ES2.LoadList<string>(product_id + "style_DescriptionList");
        }

        if (ES2.Exists(product_id + "style_PriceList"))
        {
            style_priceList = ES2.LoadList<string>(product_id + "style_PriceList");
        }
    }

   void DeleteStylesBtn()
    {
        for (int i = 0; i < HELPER.instance.ParentForStylesBtn.childCount; i++)
        {
            Destroy(HELPER.instance.ParentForStylesBtn.GetChild(i).gameObject);
        }
    }

    public void InstatiateStylesBtn()
    {
        var sm = FindObjectOfType<SceneManager>();
        for (int i = 0; i < styleNameList.Count; i++)
        {
            var btn = Instantiate(HELPER.instance.styleBtn);
            btn.transform.SetParent(HELPER.instance.ParentForStylesBtn, false);
            btn.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(() => sm.OpenStyle(btn.transform));
        }

        counter = 0;

		if (!HELPER.instance.isOfflineMode) 
		{
			StartCoroutine(StartLoad());
		}
       
        
    }



    IEnumerator StartLoad()
    {
       
        Texture2D tex;
        tex = new Texture2D(164, 164, TextureFormat.DXT1, false);
        Debug.Log("counter " + counter);

        if (counter < HELPER.instance.ParentForStylesBtn.childCount)
        {
            var btn = HELPER.instance.ParentForStylesBtn.GetChild(counter);

            using (WWW www = new WWW(style_Thumbnail_ImgList[counter]))
            {
                yield return www;
                www.LoadImageIntoTexture(tex);
                Sprite sprite = Sprite.Create((Texture2D)tex, new Rect(0, 0, tex.width, tex.height), new Vector2(1.0f, 1.0f));
                btn.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Image>().sprite = sprite;

                var coff = (float)tex.width / (float)tex.height;


                if (style_priceList.Count != 0)
                {
                    btn.transform.GetChild(0).GetChild(2).GetComponent<Text>().text = style_priceList[counter];
                }
                else
                {
                    btn.transform.GetChild(0).GetChild(3).gameObject.SetActive(false);
                    btn.transform.GetChild(0).GetChild(2).gameObject.SetActive(false);

                }
                
                
                btn.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = styleNameList[counter];
                btn.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(btn.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<RectTransform>().sizeDelta.x * coff, btn.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<RectTransform>().sizeDelta.y);
                
                if (www.isDone)
                {

                    counter++;
                    StartCoroutine(StartLoad());

                }
            }

        }
    }

    public void SaveData()
    {
        var siblingIndex = transform.parent.GetSiblingIndex();
        PlayerPrefs.SetString(siblingIndex + "product_id", product_id);

        ES2.Save(regionNameModelBtn, siblingIndex + "regionNameModelBtn");

        ES2.Save(transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite, siblingIndex + "image");
    }

    public void Click()
	{
        HELPER.instance.savedModelName = prName;
        var loader = FindObjectOfType<Example2_WWW>();
        var sm = FindObjectOfType<SceneManager>();

        HELPER.instance.needClearList = true;
        HELPER.instance.loadingModelCounter++;
        loader.objFileName = product_Model_Source_File;
        HELPER.instance.ClearRegionsList();

        for (int i = 0; i < regionNameModelBtn.Count; i++)
        {
            HELPER.instance.regionNameHelper.Add(regionNameModelBtn[i]);
        }

        //for (int i = 0; i < polygon_GroupIDCount.Count; i++)
        //{
        //    HELPER.instance.polygon_GroupIDCountHelper.Add(polygon_GroupIDCount[i]);
        //}

        HELPER.instance.currentRegionName.Clear();
        HELPER.instance.currentRegionName.Clear();

        for (int i = 0; i < regionNameModelBtn.Count; i++)
        {
            HELPER.instance.currentRegionName.Add(regionNameModelBtn[i]);
        }

        HELPER.instance.currentStyleDescription = styleDescription;
        HELPER.instance.currentProductID = product_id;
        HELPER.instance.currentProductNameHelper = transform.GetChild(1).GetComponent<Text>().text;
        HELPER.instance.currentProductPriceHelper = transform.GetChild(2).GetComponent<Text>().text;
        HELPER.instance.currentStyleHelper = transform.GetChild(4).GetComponent<Text>().text;
        HELPER.instance.currentFinishesID.Clear();

        for (int i = 0; i < finishesId.Count; i++)
        {
            HELPER.instance.currentFinishesID.Add(finishesId[i]);

        }

        if (styleNameList.Count > 0)
        {
            InstatiateStylesBtn();
        }

        if (product_Model_Source_File != null && product_Model_Source_File != "")
        {
            if (HELPER.instance.isOfflineMode)
            {
                HELPER.instance.LoadModel();
            }
            else
            {
                sm.LoadModel();
            }
        }
        else
        {
            HELPER.instance.Product2DCanvas.transform.GetChild(3).GetComponent<Image>().sprite = transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite;
            HELPER.instance.Product2DCanvas.transform.GetChild(5).GetChild(0).GetChild(5).GetComponent<Text>().text = PlayerPrefs.GetString(product_id + "style_Name");

            if (HELPER.instance.isPriceShow)
            {
                HELPER.instance.Product2DCanvas.transform.GetChild(5).GetChild(0).GetChild(3).gameObject.SetActive(true);
                HELPER.instance.Product2DCanvas.transform.GetChild(5).GetChild(0).GetChild(4).GetComponent<Text>().text = PlayerPrefs.GetString(product_id + "style_price");

                if (PlayerPrefs.HasKey(product_id + "styles"))
                {
                    HELPER.instance.Product2DCanvas.transform.GetChild(5).GetChild(0).GetChild(6).gameObject.SetActive(true);
                    HELPER.instance.Product2DCanvas.transform.GetChild(5).GetChild(0).GetChild(6).GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, 48.13335f);
                    var str = PlayerPrefs.GetString(product_id + "styles");
                    HELPER.instance.Product2DCanvas.transform.GetChild(5).GetChild(0).GetChild(6).GetChild(1).GetComponent<Text>().text = str;

                }

                else
                {
                    HELPER.instance.Product2DCanvas.transform.GetChild(5).GetChild(0).GetChild(6).GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, 48.13335f);
                    HELPER.instance.Product2DCanvas.transform.GetChild(5).GetChild(0).GetChild(6).gameObject.SetActive(false);
                }



            }
            else
            {
                HELPER.instance.Product2DCanvas.transform.GetChild(5).GetChild(0).GetChild(3).gameObject.SetActive(false);

                if (PlayerPrefs.HasKey(product_id + "styles"))
                {
                    HELPER.instance.Product2DCanvas.transform.GetChild(5).GetChild(0).GetChild(6).gameObject.SetActive(true);
                    var str = PlayerPrefs.GetString(product_id + "styles");
                    HELPER.instance.Product2DCanvas.transform.GetChild(5).GetChild(0).GetChild(6).GetChild(1).GetComponent<Text>().text = str;
                    HELPER.instance.Product2DCanvas.transform.GetChild(5).GetChild(0).GetChild(6).GetComponent<RectTransform>().anchoredPosition = new Vector2(-53f, 48.13335f);
                }

                else
                {
                    HELPER.instance.Product2DCanvas.transform.GetChild(5).GetChild(0).GetChild(6).gameObject.SetActive(false);
                    HELPER.instance.Product2DCanvas.transform.GetChild(5).GetChild(0).GetChild(6).GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, 48.13335f);
                }

            }


            HELPER.instance.Product2DCanvas.SetActive(true);

        }


       



        for (int i = 0; i < HELPER.instance.partsCanvasContent.childCount; i++)
        {
            Destroy(HELPER.instance.partsCanvasContent.GetChild(i).gameObject);
        } 




        for (int i = 0; i < regionNameModelBtn.Count; i++)
        {
            var btn = Instantiate(HELPER.instance.partsCanvasBtn);
            btn.transform.SetParent(HELPER.instance.partsCanvasContent, false);
            btn.transform.GetChild(2).GetComponent<Text>().text = regionNameModelBtn[i];
            var tempText = regionNameModelBtn[i];
            tempText = tempText.Substring(0, 2);
            btn.transform.GetChild(0).GetComponent<Text>().text = tempText.ToUpper();

            if (i == 0)
            {
                btn.transform.GetChild(1).gameObject.SetActive(true);
            }
        }                                               
    }
}

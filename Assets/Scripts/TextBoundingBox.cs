﻿using UnityEngine;



public class TextBoundingBox : MonoBehaviour {

    [SerializeField] 
    float height = 3f;
    
    float newHeight;

    void Start ()
    {
        CalculateLocalBounds();
    }

    private void CalculateLocalBounds()
    {
        Quaternion currentRotation = transform.rotation;
        transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        Bounds bounds = new Bounds(transform.position, Vector3.zero);
        foreach (Renderer renderer in GetComponentsInChildren<Renderer>())
        {
            bounds.Encapsulate(renderer.bounds);
        }
        Vector3 localCenter = bounds.center - transform.position;
        bounds.center = localCenter;
        Debug.Log("The local bounds of this model is " + bounds.size.y + "" + bounds.size.x + "" + bounds.size.z);
        transform.rotation = currentRotation;

        if (bounds.size.y > bounds.size.x)
        {
            var scaleM = height / bounds.size.y;
            transform.localScale = new Vector3(scaleM, scaleM, scaleM);
        }

        else
        {
            var scaleM = height / bounds.size.x;
            //Debug.Log("scaleeeeee " +scaleM);
            transform.localScale = new Vector3(scaleM, scaleM, scaleM);
        }


    }


}

﻿using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;

public class RotationSliderZ : MonoBehaviour,  IDragHandler, IInitializePotentialDragHandler {

    HELPER helper;
    public float multiplier;
    public Transform currentOBJ;
    Slider slider;
    float currentRotX;
    float currentRotY;
    float currentRotZ;
    MainCameraController MCC;

    void Start ()
    {
        MCC = FindObjectOfType<MainCameraController>();
        helper = FindObjectOfType<HELPER>();
        slider = GetComponent<Slider>();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        eventData.selectedObject = gameObject;
    }

    public void OnInitializePotentialDrag(PointerEventData eventData)
    {
        
        currentOBJ = GameObject.Find(helper.partName).transform;
        MCC.blockControl = true;
        currentRotX = currentOBJ.localEulerAngles.x;
        currentRotY = currentOBJ.localEulerAngles.y;
        currentRotZ = currentOBJ.localEulerAngles.z;
    }

    public void OnDrag(PointerEventData eventData)
    {
        //y pos
        if (currentRotX != 0 || currentRotY != 0 || currentRotZ != 0)
        {
            currentOBJ.localRotation = Quaternion.Euler(currentRotX, currentRotY, slider.value * multiplier + currentRotZ);
        }
        else
        {
             currentOBJ.localRotation = Quaternion.Euler(0, 0, slider.value * multiplier);
        }
       
        //Debug.Log(currentOBJ.localRotation.y);
    }

    public void End()
    {
        MCC.blockControl = false;
        slider.value = 0;
    }
}

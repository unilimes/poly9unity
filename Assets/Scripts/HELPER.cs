﻿using DigitalRuby.Threading;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class HELPER : MonoBehaviour
{
    [Header("Enums Materials")]
    [Space(10)]
    public static HELPER instance = null;
    MainCameraController camController;

    public enum currentSate { Shapes, Finishes, Patterns, };
    public currentSate currentUserSate;

    public enum currentSwitchState { Transform, Rotate, };
    public currentSwitchState currentMySwitchState;

    public enum openCanvas { MainMenu, Collection, CategoryInCollection, Orders, View, Customize, Setting, };
    public openCanvas currentOpenCunvas;

    [Header("Transforms")]
    [Space(10)]
    public Transform controlsForTextLogos;
    public Transform transfomContaner;
    public Transform scaleContaner;
    public Transform colorPicker;
    public Transform partsCanvasContent;

    public GameObject materialCanvas;
    public Transform materialParent;

    public GameObject partsCanvasBtn;
    public GameObject materialsCanvasBtn;
    public GameObject viewPopupLoading;
    public HexColorField hexColorField;
    public GameObject offColorPicker;
    public GameObject onColorPicker;
    public GameObject partsPopupCanvas;
    public Text currentPartText;
    public Text currentMaterialText;
    public ColorImage colorImage;
    public Transform changeColorBtn;
    public Transform calculateAreaSurfaceBTN;
    public Transform loginCanvas;
    public Transform loadingCanvas;
    public Transform mainCanvas;
    public Transform mainBoardCanvas;
    public Transform categoryInCollectionCanvas;
    public Transform viewCanvas;
    public Transform customizeCanvas;
    public Transform shapesScrollView;
    public Transform finishesScrollView;
    public Transform patternsScrollView;
    public Transform addTextPanel;
    public Transform btnSwitch;
    public Transform collectionViewCanvas;
    public Transform settingCanvas;
    public Transform categoryCanvas;
    public Transform manufacturerCanvas;
    public Transform filterCanvas;
    public Transform ordersDetailsCanvas;
    public Transform ordersCanvas;
    //  public Transform saveCanvas;
    public Transform shapesContent;
    public Transform finishesContent;
    public Transform savedContent;
    public Transform shapesButtons;
    public Transform rotButtons;
    public Transform transformButtons;
    public Transform parentForCollectionBtn;
    public Transform textRotButton;
    public Transform searchCanvas;
    public Transform parentForCategoryInCollButtons;
    public RectTransform castomizePanel;
    public GameObject savedBtnPref;
    public RectTransform parentForMainScroll;
    public RectTransform parentScroll;
    public Transform profileCanvas;
    public Transform ShapesScrollViewContent;
    public bool getError;

    [Header("UI")]
    [Space(10)]
    public InputField mainInputField;
    public Text textRotTr;

    public RectTransform loginInputField;
    //335 wrong login
    //313.5 good login

    public RectTransform passInputField;
    //270.5 wrong pass for pass
    //249.5 goodpass
    //335 wor login input
    public Image loginPlaceHolder;
    public Image dropDownImage;
    public Text loginText1;


    public Image passPlaceHolder;
    public Image passdropDownImage;
    public Text passText1;
    public RectTransform forgotEmailInputField;

    public Image forgotPassPlaceHolder;
    public Image forgotPassdropDownImage;
    public Text forgotPassText1;


    public Sprite wrongLogin;
    public Sprite wrongPass;
    public Sprite goodLogin;
    public Sprite goodPass;
    public Sprite wrongInputField;
    public Sprite goodInputField;
    public Transform wrongPassText;
    public RectTransform wrongLoginText;
    public Transform wrongForgotEmailText;
    public RectTransform mainCanvasForAnim;
    public Image bg;

    [SerializeField]
    Sprite materialChooseSprite;
    [SerializeField]
    Sprite materialNotChooseSprite;

    [Header("List")]
    [Space(10)]
    [SerializeField]
    List<GameObject> allCanvases;

    public List<GameObject> btnForCloseViewCanvas;
    public GameObject openDiscriptionBtn;
    public GameObject closeDiscriptionBtn;
    [SerializeField]
    List<Transform> underLines;

    //[Header("Save")]
    //[Space(10)]
    //[SerializeField]
    //bool DeleteAllSaves;
    //Material[] arrayMaterials;
    //Material[] tempMaterials;
    //[SerializeField]
    //List<int> modelsCount;
    //[SerializeField]
    //List<string> modelName;
    //[SerializeField]
    //List<string> partsName;
    //[SerializeField]
    //List<string> partsMaterialsName;
    //[SerializeField]
    //List<Material> SavedMaterials;
    //[SerializeField]
    //List<string> partNameWereCustomMaterialSave;
    //[SerializeField]
    //List<string> customModelsName;
    //[SerializeField]
    //List<Vector3> positionList;
    //[SerializeField]
    //List<Vector3> rotationList;
    //[SerializeField]
    //List<Sprite> savedScreenshots;
    //[SerializeField]
    //List<string> partsWithChangedColor;
    //[SerializeField]
    //List<Color> colorsList;
    //[SerializeField]
    //List<string> addedPartMaterial;
    //public List<Sprite> spritesForTextLogoBtn;
    //List<GameObject> partsModelList;
    //public List<string> customTextList;
    //public List<string> customMaterialPartName;
    //public List<int> indexMatList;
    [SerializeField]
    RectTransform styleParent;
    [SerializeField]
    RectTransform styleParentViewPopup;

    [SerializeField]
    RectTransform styleParentCustomizeCanvas;

    [SerializeField]
    RectTransform arRectParent;
    [Header("Other Fields")]
    [Space(10)]
    [SerializeField]
    Sprite costomizeSlideUp;
    [SerializeField]
    Sprite costomizeSlideDown;
    [SerializeField]
    Image costomizeSlideBtn;

    public Sprite defSprite;
    public string hackText;
    public string path = "save.txt?tag=";
    public bool needDelete;
    [SerializeField]
    bool castomizePanelUp;
    [SerializeField]
    public int matIndex;
    public string partName;
    public string currentPartType;
    GameObject ButtonForDelete;
    [SerializeField]
    int btnIndex;
    [SerializeField]
    Sprite parentForCollectionBtnSprite;
    //public bool isSave;
    public bool isLoadiiiing;
    public Material matForCopy;
    [SerializeField]
    SceneManager sceneManager;
    

    [Header("DDP")]
    [Space(10)]

    public GameObject materialBtn;
    public Transform parentForMaterialBtns;
    public bool isPriceShow;
    public Text firstCharacterCompanyName;
    public bool isLogged;
    public List<Text> CompanyNameList;
    public Text modelsCountText;
    public Text filterModelsCountText;

    public List<string> currentRegionName;
    public List<bool> needDisplaySelectedIcon;

    public Transform parentForFinishBtn;
    public int currentDropdownIndex;
    [Space(10)]

    public string absoletePath = "http://d2ww3metzfrng3.cloudfront.net";
    public string userID;

    public Text userName;
    public Image userLogo;

    public Image userLogoProfile;
    public Text userNameProfile;


    public Text emailID;
    public Text phoneProfile;
    public Text companyNameProfile;
    public Text departamentNameProfile;
    public Text adressProfile;
    public List<string> styleName;
    public List<string> styleThumbnailImg;
    public List<string> stylePrice;
    public List<string> customizationRegionID;

    public List<string> regionName;
    public List<string> region_Base_MaterialID;
    public List<string> region_finishes_techniques;
    public List<string> region_ID;

    public List<string> finishesNames;

    public List<string> finishesPhotoURL;
    public List<string> finishesDiffuseURL;
    public List<string> finishesRoughnessURL;
    public List<string> finishesMetallnesURL;
    public List<string> finishesNormalMapURL;
    public List<string> materialName;
    public List<string> materialID;

    public string currentMaterialBtn;
    public string currentPartBtn;
    public List<string> matID;

    public List<string> regionNameHelper;
    public List<int> polygon_GroupIDCountHelper;
    public string currentProductID;
    public string currentRegionNameHelper;
    public string currentMaterialNameHelper;
    public string currentProductNameHelper;
    public string currentProductPriceHelper;
    public string currentStyleHelper;
    public string currentStyleDescription;
    public List<string> currentFinishesID;
    public GameObject parentObj;
    public int counter;
    public int counterSum;
    public Transform modelsUIParent;
    public Transform finishesUIParent;

    public Transform viewPriceIcon;
    public Transform viewStyleIcon;
    public Text viewModelName;
    public Text viewPrice;
    public Text viewStyleCount;

    public Transform viewPopupPriceIcon;
    public Transform viewPopupStyleIcon;
    public Text viewPopupModelName;
    public Text viewPopupPrice;
    public Text viewPopupStyleCount;
    public Text viewPopupDescription;

    public Transform customizeStyleIcon;
    public Transform customizePriceIcon;
    public Text customizeModelName;
    public Text customizePrice;
    public Text customizeStyleCount;


    public Transform arStyleIcon;
    public Transform arPriceIcon;
    public Text arModelName;
    public Text arPrice;
    public Text arStyleCount;

    public AnimImage ai;
    public Transform blackTopMainCanvas;
    public Image topBgImage;
    public Text topText; // forColor
    public Image topFilterBtnImage;
    public Image topSearchBtnImage;
    public Image topCategoryBtnImage;
    public Image toplogoImage;
    public Image topCamImage;

    public bool needClearList;
    public int loadingModelCounter = 0;


    [Header("Filter")]
    public Text minPrice;
    public Text maxPrice;
    [SerializeField]
    InputField filterByName;
    public List<string> filtredMaterial;
    public float waitTime = 5f;
    float timer;
    public GameObject styleBtn;
    public Transform ParentForStylesBtn;
    public GameObject PriceTr1;
    public GameObject PriceTr2;


    [Header("Internet Connection")]
    public List<GameObject> InternetConnectionLostWarning;
    public List<GameObject> InternetConnectionFindWarning;
    public bool connectionLost;
    public bool connectionLostIsShow;
    public bool connectionFind;
    public bool connectionFindIsShow;

    [Header("QRCode")]
    [Space(10)]
    public string qrResault;
    public GameObject QRCanvas;
    public GameObject QRCam;
    public GameObject house;
    public GameObject qrLoadingCanvas;
    public GameObject badRequstPanel;

    [SerializeField]
    GameObject ARCanvas;
    public List<GameObject> arGO;
    public Transform arParent;
    public Transform hitPlayer;
	public UnityARCameraManager arCamManager;
    public Camera arCam;
    public Transform mainCam;
    public Vector3 startObjectPos;
    public bool isARMode;
	public GameObject arRotationBtn;
	public bool rotationInAR;

    [Header("CategoryCanvas")]
    [Space(10)]
    [SerializeField]
    GameObject secondPanel;
    [SerializeField]
    GameObject thirdPanel;
    [SerializeField]
    GameObject parentForFirstPanel;
    [SerializeField]
    GameObject parentForSecondPanel;
    [SerializeField]
    GameObject parentForThirdPanel;

    // [Header("SavedContent")]
    // [Space(20)]

    public int finishesCounter;
    public bool isOfflineMode;

    [Header("Save")]
    [Space(10)]
    public GameObject savedModel;
    public string savedModelName;
    [SerializeField]
    GameObject savedGameObject;

    public List<bool> img;

	public bool needDisplaySave;
	public string saveTextForGUI;

    [Header("Board")]
    [Space(10)]
    public Transform mainBoardContent;
    public GameObject boardWithHeader;
    public GameObject boardWithoutHeader;
    public Transform detailBoard;
    public GameObject productBtn;
    public GameObject conceptBtn;
    public Transform boardDetailsContent0;
    public Transform boardDetailsContent1;
    public GameObject conceptCanvas;
    public GameObject colorCanvas;
    public GameObject Product2DCanvas;
    [Header("ColorPicker")]
    [Space(10)]
    public bool needActiveColorPicker;

    public GameObject loadPanel;

    public string newpath;
    public List<long> dateLongList;
    public bool needDisableLoadingCanvas;
    void Awake()
    {

        if (instance == null)
        {
            instance = this; 
        }

        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        loadingModelCounter = 0;
		Application.targetFrameRate = 60;

    }

    void Start()
    {
        Screen.fullScreen = false;
        StartCoroutine(BuildScene());
    }

    public void SortMainBtns()
    {
        for (int i = 0; i < parentForMainScroll.childCount; i++)
        {
            dateLongList.Add(parentForMainScroll.GetChild(i).GetChild(0).GetComponent<ModelButton>().date);
        }

        dateLongList.Sort();
        dateLongList.Reverse();
        for (int i = 0; i < dateLongList.Count; i++)
        {

            for (int j = 0; j < parentForMainScroll.childCount; j++)
            {
                if(parentForMainScroll.GetChild(j).GetChild(0).GetComponent<ModelButton>().date == dateLongList[i])
                {
                    parentForMainScroll.GetChild(j).SetSiblingIndex(i);
                }
            }
        }

        if (ai._animImage.fillAmount > 0.9f)
        {
            loadingCanvas.gameObject.SetActive(false);
            ai.SetFillAmount();
        }
        else
        {
            needDisableLoadingCanvas = true;
        }
       
    }


    private void Update()
    {
        CheckConnection();
		if (Input.GetKeyDown(KeyCode.W))
        {
            SaveData();
        }
        if (Input.GetKeyDown(KeyCode.F2))
        {
            LoadData();
        }

        if (Input.GetKeyDown(KeyCode.F3))
        {
            StartCoroutine(LoadAllSaveTextures());
        }
        //if (Input.GetKeyDown(KeyCode.M))
        //{
        //    LoadModel();
        //}
		if (Input.GetKeyDown(KeyCode.Q))
        {
            DeleteAllSa();
        }
    }

    public void DeleteAllSa()
    {
        Debug.Log("DeleteAll");
        ES2.DeleteDefaultFolder();
        RSManager.RemoveAll();
        PlayerPrefs.DeleteAll();
    }
    public void JustExit()
    {
        StartCoroutine(ExitFromLoading());
    }

    IEnumerator ExitFromLoading()
    {
        var currentGO = GameObject.FindWithTag("CurrentObjParent").gameObject;
        Destroy(currentGO.gameObject);
        loadPanel.transform.GetChild(0).GetComponent<Text>().text = "Texture Error";
        yield return new WaitForSeconds(1.5f);
        loadPanel.SetActive(false);
        loadPanel.transform.GetChild(0).GetComponent<Text>().text = "Loading...";
        
    }

    public void SaveModel()
    {
        if (!RSManager.ContainsKey("Model" + savedModelName))
        {
            Debug.Log("save model");
            RSManager.Serialize(savedModel, "Model" + savedModelName, VoxelBusters.RuntimeSerialization.eSaveTarget.FILE_SYSTEM);
        }
        else
        {
            Debug.Log("model saved");
        }
       
    }

    public void LoadModel()
    {
        StartCoroutine(ActivateLoadingScreen());
       
    }

    public IEnumerator ActivateLoadingScreen()
    {
        if (RSManager.ContainsKey("Model" + savedModelName))
        {
            sceneManager.mainScenePanel.gameObject.SetActive(true);
            yield return new WaitForSeconds(0.3f);
            RSManager.Deserialize<GameObject>("Model" + savedModelName);
        }
    }



    IEnumerator LoadAllSaveTextures()
    {

        var firstTexture = false;
        var secondTexture = false;
        var thirdTexture = false;
        var fourthTexture = false;
        yield return new WaitForEndOfFrame();

        if (finishesCounter < sceneManager.m_finishes.Count)
        {
            Debug.Log(finishesCounter);
			if (!ES2.Exists(finishesCounter + "diffuseTexture") && sceneManager.m_finishes[finishesCounter].m_diffuseUrl != null)
            {
                using (WWW www = new WWW(sceneManager.m_finishes[finishesCounter].m_diffuseUrl))
                {
                    yield return www;
                    ES2.Save(www.texture, finishesCounter + "diffuseTexture");
                    firstTexture = true;
                }
            }
            else
            {
               // Debug.Log("finishExist " + finishesCounter);
                firstTexture = true;
            }
			if (!ES2.Exists(finishesCounter + "normalMap")&& sceneManager.m_finishes[finishesCounter].m_normalMapUrl != null)
            {
                using (WWW www = new WWW(sceneManager.m_finishes[finishesCounter].m_normalMapUrl))
                {
                    yield return www;
                    ES2.Save(www.texture, finishesCounter + "normalMap");
                    secondTexture = true;
                }
            }
            else
            {
                secondTexture = true;
            }

			if (!ES2.Exists(finishesCounter + "metalnessTexture") && sceneManager.m_finishes[finishesCounter].m_metalnessMapUrl != null)
            {
                using (WWW www = new WWW(sceneManager.m_finishes[finishesCounter].m_metalnessMapUrl))
                {
                    yield return www;
                    ES2.Save(www.texture, finishesCounter + "metalnessTexture");
                    thirdTexture = true;
                }
            }
            else
            {
                thirdTexture = true;
            }

			if (!ES2.Exists(finishesCounter + "roughnessTexture") && sceneManager.m_finishes[finishesCounter].m_roughnessMapUrl != null)
            {
                using (WWW www = new WWW(sceneManager.m_finishes[finishesCounter].m_roughnessMapUrl))
                {

                    yield return www;
                    ES2.Save(www.texture, finishesCounter + "roughnessTexture");
                    fourthTexture = true;
                }
            }
            else
            {
                fourthTexture = true;
            }

            yield return new WaitUntil(() => firstTexture && secondTexture && thirdTexture && fourthTexture);
            finishesCounter++;
            Resources.UnloadUnusedAssets();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            StartCoroutine(LoadAllSaveTextures());
        }
        else
       {
            SaveData();

        }
    }

    public void justLoad()
    {
        newpath = Application.persistentDataPath;
        EZThread.ExecuteInBackground(LoadAllTexturesOnDisk);
    }

    void LoadAllTexturesOnDisk()
    {

        
        for (int i = 0; i < sceneManager.m_finishes.Count; i++)
        {
            if (sceneManager.m_finishes[i].m_diffuseUrl != null)
            {
                WebClient wb = new WebClient();

                wb.DownloadFile(sceneManager.m_finishes[i].m_diffuseUrl, Path.Combine(newpath, i + "diffuse.jpg"));
            }

            if (sceneManager.m_finishes[i].m_metalnessMapUrl != null)
            {
                WebClient wb = new WebClient();
                
                wb.DownloadFile(sceneManager.m_finishes[i].m_metalnessMapUrl, Path.Combine(newpath, i +"metal.jpg"));
            }

            if (sceneManager.m_finishes[i].m_normalMapUrl != null)
            {
                WebClient wb = new WebClient();

                wb.DownloadFile(sceneManager.m_finishes[i].m_normalMapUrl, Path.Combine(newpath, i + "normal.jpg"));
            }

            if (sceneManager.m_finishes[i].m_roughnessMapUrl != null)
            {
                WebClient wb = new WebClient();

                wb.DownloadFile(sceneManager.m_finishes[i].m_roughnessMapUrl, Path.Combine(newpath, i + "roughness.jpg"));
            }

            if (i == sceneManager.m_finishes.Count-1)
            {
               EZThread.ExecuteOnMainThread(SaveData);
            }
        }
        
            
    }

    public void OpenSecondPanelInCategoryCanvas(Transform btnSiblingIndex)
    {
        if (!secondPanel.activeSelf)
        {
            for (int i = 0; i < parentForFirstPanel.transform.childCount; i++)
            {
                parentForFirstPanel.transform.GetChild(i).GetChild(3).gameObject.SetActive(false);
            }

            parentForFirstPanel.transform.GetChild(btnSiblingIndex.GetSiblingIndex()).transform.GetChild(3).gameObject.SetActive(true);
            secondPanel.SetActive(true);
        }
        else
        {
            if (thirdPanel.activeSelf)
            {
                for (int i = 0; i < parentForSecondPanel.transform.childCount; i++)
                {
                    parentForSecondPanel.transform.GetChild(i).GetChild(2).gameObject.SetActive(false);
                }
                thirdPanel.SetActive(false);
            }
            if (secondPanel.activeSelf)
            {
                for (int i = 0; i < parentForFirstPanel.transform.childCount; i++)
                {
                    parentForFirstPanel.transform.GetChild(i).GetChild(3).gameObject.SetActive(false);
                }
                secondPanel.SetActive(false);
            }
        }


    }


    public void OpenThirdPanelInCategoryCanvas(Transform btnSiblingIndex)
    {
        if (!thirdPanel.activeSelf)
        {
            for (int i = 0; i < parentForSecondPanel.transform.childCount; i++)
            {
                parentForSecondPanel.transform.GetChild(i).GetChild(2).gameObject.SetActive(false);
            }

            parentForSecondPanel.transform.GetChild(btnSiblingIndex.GetSiblingIndex()).transform.GetChild(2).gameObject.SetActive(true);
            thirdPanel.SetActive(true);
        }
        else
        {
            for (int i = 0; i < parentForSecondPanel.transform.childCount; i++)
            {
                parentForSecondPanel.transform.GetChild(i).GetChild(2).gameObject.SetActive(false);
            }
            thirdPanel.SetActive(false);
        }
    }

    public void ResetPartsBtn()
    {
        for (int i = 0; i < partsCanvasContent.childCount; i++)
        {
            partsCanvasContent.GetChild(i).GetChild(1).gameObject.SetActive(false);
        }
    }

    public void OpenAR() 
    { 
		isARMode = true;

        CloseAllCanvases();
        house.SetActive(false);
        var cam = GameObject.FindWithTag("MainCamera");
        cam.GetComponent<Camera>().enabled = false;
        cam.GetComponent<MainCameraController>().enabled = false;

        var go = GameObject.FindWithTag("CurrentObjParent");

        for (int i = 0; i < arGO.Count; i++) {
            arGO[i].SetActive(true);
        }
		arCamManager.CustomStart ();

        go.transform.position = hitPlayer.position;
        go.transform.localScale = new Vector3(go.transform.localScale.x / 10, go.transform.localScale.y / 10, go.transform.localScale.z / 10);
        go.transform.SetParent(arParent);


		var gom = GameObject.Find ("FocusSquare").GetComponent<FocusSquare> ();

		gom.enabled = true; 
		gom.findingSquare.SetActive (true);

		//gom.findingSquare.SetActive (true);
		//gom.findingSquare.GetComponent<Animator>().enabled = true;
		gom.hitExample = go.GetComponent<UnityEngine.XR.iOS.UnityARHitTestExample> ();
		go.GetComponent<Lean.Touch.LeanRotate> ().Camera = arCam;
		go.GetComponent<UnityEngine.XR.iOS.UnityARHitTestExample> ().enabled = false;
        EnableUnderline();
        counter = 0;
        counterSum = 0;
        SetupModelTag();


        PartStart();

        needDisplaySelectedIcon = new List<bool>(regionNameHelper.Count);

        for (int i = 0; i < regionNameHelper.Count; i++)
        {
            needDisplaySelectedIcon.Add(true);
        }



        for (int i = 0; i < parentForFinishBtn.childCount; i++)
        {
            parentForFinishBtn.GetChild(i).GetChild(0).GetChild(1).gameObject.SetActive(false);
            parentForFinishBtn.GetChild(i).GetChild(0).GetComponent<FinishButton>().notChangeIcon = false;
        }

        for (int i = 0; i < parentForFinishBtn.childCount; i++)
        {
            for (int j = 0; j < currentFinishesID.Count; j++)
            {
                if (currentFinishesID[j] == parentForFinishBtn.GetChild(i).GetChild(0).GetComponent<FinishButton>().finishID)
                {
                    parentForFinishBtn.GetChild(i).GetChild(0).GetChild(1).gameObject.SetActive(true);
                    parentForFinishBtn.GetChild(i).GetChild(0).GetComponent<FinishButton>().notChangeIcon = true;
                }
            }
        }
		//arRotationBtn.SetActive (true);
        OpenCustomizeCanvas();
    }

	public void RotINAR()
	{
		rotationInAR = !rotationInAR;

		if (!rotationInAR) {
			var go = GameObject.FindWithTag ("CurrentObjParent");
			go.GetComponent<Lean.Touch.LeanRotate> ().enabled = false;
		} else 
		{
			var go = GameObject.FindWithTag ("CurrentObjParent");
			go.GetComponent<Lean.Touch.LeanRotate> ().enabled = true;
		}

	}

    public void CloseAR()
    {
        var go = GameObject.FindWithTag("CurrentObjParent");
        go.transform.parent = null;
		#if UNITY_IOS
		//var go1 = FindObjectOfType<UnityEngine.XR.iOS.ARKitRemoteConnection> ();
		//Destroy (go1.gameObject);
		#endif
        go.transform.position = startObjectPos;
        go.transform.localScale = new Vector3(go.transform.localScale.x * 10, go.transform.localScale.y * 10, go.transform.localScale.z * 10);
        for (int i = 0; i < arGO.Count; i++) {
            arGO[i].SetActive(false);
        }
        CloseAllCanvases();
        //ARCanvas.SetActive (false);
        mainCam.GetComponent<Camera>().enabled = true;
        mainCam.GetComponent<MainCameraController>().enabled = true;

        house.SetActive(true);
        viewCanvas.gameObject.SetActive(true);
		//arRotationBtn.SetActive (false);
		isARMode = false;

    }

    public void ResetMaterialsBtn()
    {
        for (int i = 0; i < materialParent.childCount; i++)
        {
            materialParent.GetChild(i).GetChild(1).gameObject.SetActive(false);
        }
    }

    public void Changer()
    {
        for (int i = 0; i < parentForFinishBtn.childCount; i++)
        {
            for (int j = 0; j < currentFinishesID.Count; j++)
            {
                if (currentFinishesID[currentDropdownIndex] == parentForFinishBtn.GetChild(i).GetChild(0).GetComponent<FinishButton>().finishID)
                {
                    parentForFinishBtn.GetChild(i).GetChild(0).GetChild(1).gameObject.SetActive(true);
                    parentForFinishBtn.GetChild(i).GetChild(0).GetComponent<FinishButton>().notChangeIcon = true;
                }

                else
                {
                    parentForFinishBtn.GetChild(i).GetChild(0).GetChild(1).gameObject.SetActive(false);
                    parentForFinishBtn.GetChild(i).GetChild(0).GetComponent<FinishButton>().notChangeIcon = false;
                }
            }
        }
    }

    public void BackFromQRCanvas()
    {
        var qr = FindObjectOfType<QRDecodeTest>();
        qr.Reset();
        CloseAllCanvases();
        house.SetActive(true);
        QRCam.SetActive(false);
        var mainCam = GameObject.FindWithTag("MainCamera");
        mainCam.GetComponent<Camera>().enabled = true;
        qr.needCallMethod = true;
        qrLoadingCanvas.SetActive(false);
        QRCanvas.SetActive(false);
        OpenMainMenuCanvas();
        badRequstPanel.SetActive(false);

    }

    public void OpenQRScan()
    {
        var mainCam = GameObject.FindWithTag("MainCamera");
        mainCam.GetComponent<Camera>().enabled = false;
        QRCam.SetActive(true);
        var qr = FindObjectOfType<QRDecodeTest>();
        qr.needCallMethod = true;
        CloseAllCanvases();
        QRCanvas.SetActive(true);
        house.SetActive(false);
        qr.Reset();
    }

    public void FindProductByQR()
    {

        int dotIndex = qrResault.LastIndexOf('/');
        if (dotIndex >= 0)
        {
            qrResault = qrResault.Substring(dotIndex + 1);
        }

        qrLoadingCanvas.SetActive(true);
        var trigger = false;
        for (int i = 0; i < parentForMainScroll.childCount; i++)
        {
            if (parentForMainScroll.GetChild(i).GetChild(0).GetComponent<ModelButton>().product_StyleID == qrResault)
            {
                trigger = true;
                CloseAllCanvases();
                parentForMainScroll.GetChild(i).GetChild(0).GetComponent<ModelButton>().Click();
                var mainCam = GameObject.FindWithTag("MainCamera");
                mainCam.GetComponent<Camera>().enabled = true;
                badRequstPanel.SetActive(false);
            }
        }
        qrResault = string.Empty;

        if (!trigger)
        {
            //var mainCam = GameObject.FindWithTag("MainCamera");
            //mainCam.GetComponent<Camera>().enabled = true;
            //CloseAllCanvases();
            //qrLoadingCanvas.SetActive(false);
            //OpenMainMenuCanvas();
            badRequstPanel.SetActive(true);

        }

    }

    public void DeleteStylesBtn()
    {
        for (int i = 0; i < ParentForStylesBtn.childCount; i++)
        {
            Destroy(ParentForStylesBtn.GetChild(i).gameObject);
        }
    }


    public void Discription(RectTransform decription)
    {
        if (decription.sizeDelta.y == 28.6f)
        {
            decription.sizeDelta = new Vector2(decription.sizeDelta.x, 98.293f);
            decription.anchoredPosition = new Vector2(decription.anchoredPosition.x, -163.33f);
            openDiscriptionBtn.SetActive(false);
            closeDiscriptionBtn.SetActive(true);

            for (int i = 0; i < btnForCloseViewCanvas.Count; i++)
            {
                btnForCloseViewCanvas[i].SetActive(false);
            }
        }
        else
        {
            decription.sizeDelta = new Vector2(decription.sizeDelta.x, 28.6f);
            decription.anchoredPosition = new Vector2(decription.anchoredPosition.x, -128.48f);
            closeDiscriptionBtn.SetActive(false);
            openDiscriptionBtn.SetActive(true);

            for (int i = 0; i < btnForCloseViewCanvas.Count; i++)
            {
                btnForCloseViewCanvas[i].SetActive(true);
            }
        }
    }

    public void CloseLoadingScreen(Transform screen)
    {
		
        screen.gameObject.SetActive(false);
        OpenMainMenuCanvas();
    }

	public void CloseLoadingCanvasInMainMenu(Transform canvas)
	{
		
		canvas.gameObject.SetActive(false);
        var currentGO = GameObject.FindWithTag("CurrentObjParent").gameObject;
        Destroy(currentGO.gameObject);
    }

    public void FilterByName()
    {

        if (filterByName.text.Length != 1)
        {

            for (int i = 0; i < modelsUIParent.childCount; i++)
            {
                modelsUIParent.GetChild(i).gameObject.SetActive(true);
            }


            for (int i = 0; i < modelsUIParent.childCount; i++)
            {
                var name = modelsUIParent.GetChild(i).GetChild(0).GetComponent<ModelButton>().prName.ToLower();
                
                var filter = filterByName.text.ToLower();
                if (name.Contains(filter))
                {
                    Debug.Log("yes " + i);
                }
                else
                {
                    modelsUIParent.GetChild(i).gameObject.SetActive(false);
                }
            }

            parentForMainScroll.GetComponent<SetupHeightOnScroll>().Setup();
        }
    }

    void CheckConnection()
    {
        timer += Time.deltaTime;

        if (timer > waitTime)
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                EnableLostConnectionWarning();
            }
            else
            {
                // Debug.Log("find");
                StartCoroutine(EnableFindConnection());
            }
            timer = 0f;
        }
    }

    void EnableLostConnectionWarning()
    {
        connectionFindIsShow = false;
        if (!connectionLostIsShow)
        {
            connectionLostIsShow = true;
            for (int i = 0; i < InternetConnectionLostWarning.Count; i++)
            {
                InternetConnectionLostWarning[i].SetActive(true);
            }
        }
    }

    IEnumerator EnableFindConnection()
    {
        if (!connectionFindIsShow)
        {

            connectionLostIsShow = false;
            connectionFindIsShow = true;
            for (int i = 0; i < InternetConnectionLostWarning.Count; i++)
            {
                InternetConnectionLostWarning[i].SetActive(false);
            }

            for (int i = 0; i < InternetConnectionFindWarning.Count; i++)
            {
                InternetConnectionFindWarning[i].SetActive(true);
            }


            yield return new WaitForSeconds(2f);

            for (int i = 0; i < InternetConnectionFindWarning.Count; i++)
            {
                InternetConnectionFindWarning[i].SetActive(false);
            }
        }
    }


    public void ChooseMaterialInFilter(Image image)
    {
        if (image.sprite.name == "WhiteFilter")
        {
            image.sprite = materialChooseSprite;
            image.transform.GetChild(1).gameObject.SetActive(false);
            filtredMaterial.Add(materialID[image.transform.GetSiblingIndex()]);
        }
        else
        {
            image.sprite = materialNotChooseSprite;
            image.transform.GetChild(1).gameObject.SetActive(true);
            filtredMaterial.Remove(materialID[image.transform.GetSiblingIndex()]);
        }
    }

    public void ApplyFilter()
    {

        for (int i = 0; i < parentForMainScroll.childCount; i++)
        {
            parentForMainScroll.GetChild(i).gameObject.SetActive(true);
        }

        for (int i = 0; i < parentForMainScroll.childCount; i++)
        {
            var currentPrice = parentForMainScroll.GetChild(i).GetChild(0).GetComponent<ModelButton>().style_price;

            if (currentPrice != "")
            {

                var tempMin = minPrice.text;
                tempMin = tempMin.Replace(",", "");

                var tempMax = maxPrice.text;
                tempMax = tempMax.Replace(",", "");



                if (int.Parse(currentPrice) > int.Parse(tempMin) && int.Parse(currentPrice) < int.Parse(tempMax))
                {
                    Debug.Log("goodPrice");
                }
                else
                {
                    parentForMainScroll.GetChild(i).gameObject.SetActive(false);
                }
            }
        }

        if (filtredMaterial.Count != 0)
        {
            for (int i = 0; i < parentForMainScroll.childCount; i++)
            {

                var trigger = false;
                for (int j = 0; j < parentForMainScroll.GetChild(i).GetChild(0).GetComponent<ModelButton>().matIDInThisModel.Count; j++)
                {
                    for (int k = 0; k < filtredMaterial.Count; k++)
                    {
                        if (filtredMaterial[k] == parentForMainScroll.GetChild(i).GetChild(0).GetComponent<ModelButton>().matIDInThisModel[j])
                        {
                            trigger = true;
                        }
                    }
                }

                if (trigger)
                {
                    Debug.Log("good filter " + parentForMainScroll.GetChild(i));
                }
                else
                {
                    parentForMainScroll.GetChild(i).gameObject.SetActive(false);
                }
            }
        }

    }

    public void ScrollMainCanvas()
    {

        var curPos = parentScroll.anchoredPosition.y;

        if (curPos > -1000000 && curPos < 2)
        {
            // top 142.01 ---- normal
            // 64 ------- upper
            bg.color = new Color(bg.color.r, bg.color.g, bg.color.b, 1);
            blackTopMainCanvas.gameObject.SetActive(false);
        }
        else
        {
            if (!blackTopMainCanvas.gameObject.activeSelf)
            {
                blackTopMainCanvas.gameObject.SetActive(true);
            }
            bg.color = new Color(bg.color.r, bg.color.g, bg.color.b, curPos/142);
            topBgImage.color = new Color(topBgImage.color.r, topBgImage.color.g, topBgImage.color.b, (142-curPos) / 142);
            topText.color = new Color(topText.color.r, topText.color.g, topText.color.b, (142 - curPos) / 142);
            topFilterBtnImage.color = new Color(topFilterBtnImage.color.r, topFilterBtnImage.color.g, topFilterBtnImage.color.b, (142 - curPos) / 142);
            topSearchBtnImage.color = new Color(topSearchBtnImage.color.r, topSearchBtnImage.color.g, topSearchBtnImage.color.b, (142 - curPos) / 142);
            topCategoryBtnImage.color = new Color(topCategoryBtnImage.color.r, topCategoryBtnImage.color.g, topCategoryBtnImage.color.b, (142 - curPos) / 142);
            toplogoImage.color = new Color(toplogoImage.color.r, toplogoImage.color.g, toplogoImage.color.b, (142 - curPos) / 142);
            topCamImage.color = new Color(topCamImage.color.r, topCamImage.color.g, topCamImage.color.b, (142 - curPos) / 142);

        }

       
    }

	public void CheckScroolPos()
	{
		var curPos = parentScroll.anchoredPosition.y;
		Debug.Log ("endDrag " + curPos);
		if (curPos > -1000000 && curPos < 2)
		{
			Debug.Log ("yeeees go drag ");
			bg.color = new Color(bg.color.r, bg.color.g, bg.color.b, 1f);
			topBgImage.color = new Color(topBgImage.color.r, topBgImage.color.g, topBgImage.color.b, 1f);
			topText.color = new Color(topText.color.r, topText.color.g, topText.color.b, 1f);
			topFilterBtnImage.color = new Color(topFilterBtnImage.color.r, topFilterBtnImage.color.g, topFilterBtnImage.color.b, 1f);
			topSearchBtnImage.color = new Color(topSearchBtnImage.color.r, topSearchBtnImage.color.g, topSearchBtnImage.color.b, 1f);
			topCategoryBtnImage.color = new Color(topCategoryBtnImage.color.r, topCategoryBtnImage.color.g, topCategoryBtnImage.color.b, 1f);
			toplogoImage.color = new Color(toplogoImage.color.r, toplogoImage.color.g, toplogoImage.color.b, 1f);
			topCamImage.color = new Color(topCamImage.color.r, topCamImage.color.g, topCamImage.color.b, 1f);
			blackTopMainCanvas.gameObject.SetActive(false);
		}
		
	}


    public void LoadLogo(string url)
    {
        StartCoroutine(StartLoadLogoImage(url));
    }

    IEnumerator StartLoadLogoImage(string url)
    {

        Texture2D tex;
        tex = new Texture2D(1, 1, TextureFormat.DXT1, false);
        using (WWW www = new WWW(url))
        {
            yield return www;
            www.LoadImageIntoTexture(tex);

            var mySprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);

            userLogo.sprite = mySprite;
            userLogoProfile.sprite = mySprite;
        }
    }

    public void IncorrectLogin()
    {
        loginInputField.anchoredPosition = new Vector2(loginInputField.anchoredPosition.x, 335f);
        loginPlaceHolder.color = Color.white;
        loginPlaceHolder.sprite = wrongLogin;
        dropDownImage.sprite = wrongInputField;
        loginText1.color = new Color32(249, 61, 102, 225);
        wrongPassText.gameObject.SetActive(false);
        getError = true;
    }

    public void IncorrectPass()
    {
        passInputField.anchoredPosition = new Vector2(passInputField.anchoredPosition.x, 270.5f);
        loginInputField.anchoredPosition = new Vector2(loginInputField.anchoredPosition.x, 335f);
        passPlaceHolder.color = Color.white;
        passPlaceHolder.sprite = wrongPass;
        passText1.color = new Color32(249, 61, 102, 225);
        passdropDownImage.sprite = wrongInputField;
        wrongPassText.gameObject.SetActive(true);



        getError = true;
    }

    public void IncorrectLoginAndPass()
    {

        passInputField.anchoredPosition = new Vector2(passInputField.anchoredPosition.x, 270.5f);
        loginInputField.anchoredPosition = new Vector2(loginInputField.anchoredPosition.x, 365f);
        wrongLoginText.anchoredPosition = new Vector2(-56.585f, 318f);
        passPlaceHolder.color = Color.white;
        passPlaceHolder.sprite = wrongPass;
        passText1.color = new Color32(249, 61, 102, 225);
        passdropDownImage.sprite = wrongInputField;
        wrongPassText.gameObject.SetActive(true);

        loginPlaceHolder.color = Color.white;
        loginPlaceHolder.sprite = wrongLogin;
        dropDownImage.sprite = wrongInputField;
        loginText1.color = new Color32(249, 61, 102, 225);

        getError = true;
    }


    public void ResetIncorrect()
    {

        if (getError)
        {

            loginInputField.anchoredPosition = new Vector2(loginInputField.anchoredPosition.x, 313.5f);
            passInputField.anchoredPosition = new Vector2(passInputField.anchoredPosition.x, 249.5f);
            wrongLoginText.anchoredPosition = new Vector2(-56.585f, 292.82f);
            passPlaceHolder.sprite = goodPass;
            loginPlaceHolder.sprite = goodLogin;
            dropDownImage.sprite = goodInputField;
            loginText1.color = new Color32(50, 50, 50, 225);
            passdropDownImage.sprite = goodInputField;
            passText1.color = new Color32(50, 50, 50, 225);
            wrongPassText.gameObject.SetActive(false);
            getError = false;
        }
    }


    public void IncorrectForgotEmail()
    {

        forgotEmailInputField.anchoredPosition = new Vector2(passInputField.anchoredPosition.x, 270.5f);
        forgotPassPlaceHolder.color = Color.white;
        forgotPassPlaceHolder.sprite = wrongLogin;
        forgotPassText1.color = new Color32(249, 61, 102, 225);
        forgotPassdropDownImage.sprite = wrongInputField;
        wrongForgotEmailText.gameObject.SetActive(true);
        getError = true;
    }

    public void ResetForgotEmailIncorrect()
    {

        if (getError)
        {
            forgotEmailInputField.anchoredPosition = new Vector2(passInputField.anchoredPosition.x, 249.5f);
            forgotPassPlaceHolder.sprite = goodLogin;
            forgotPassdropDownImage.sprite = goodInputField;
            forgotPassText1.color = new Color32(50, 50, 50, 225);
            wrongForgotEmailText.gameObject.SetActive(false);
            getError = false;
        }
    }

    public void ForgotPass()
    {
        if (forgotEmailInputField.GetComponent<InputField>().text.Contains("@") && forgotEmailInputField.GetComponent<InputField>().text.Length > 5)
        {
            Debug.Log("Good Email");
        }
        else
        {
            IncorrectForgotEmail();
        }
    }

    public void DeleteAll()
    {
        ES2.Delete("savedMaterials.txt");
        Debug.Log("delete");
        ES2.DeleteDefaultFolder();
        ES2.Delete("savedSprite.txt");
        ES2.Delete("save.txt");
    }

    void CheckIfUserIsLogged()
    {
        if (PlayerPrefs.HasKey("userID"))
        {
            userID = PlayerPrefs.GetString("userID");
        }
    }

    public void DeleteSaves()
    {
        RSManager.RemoveAll();
    }

    public void SaveData()
    {
        Debug.Log("LastSave");
        PlayerPrefs.SetInt("FinishCount", finishesContent.childCount);
        PlayerPrefs.SetInt("MainBtnsCount", parentForMainScroll.childCount);

        for (int i = 0; i < finishesContent.childCount; i++)
        {
            finishesContent.GetChild(i).GetChild(0).GetComponent<FinishButton>().SaveData();
        }

        for (int i = 0; i < parentForMainScroll.childCount; i++)
        {
            parentForMainScroll.GetChild(i).GetChild(0).GetComponent<ModelButton>().SaveData();
        }
        sceneManager.SaveFinishes();
        ES2.Save(materialID, "NewMaterialID");
        ES2.Save(materialName, "NewMaterialName");
        Debug.Log("Save");

        sceneManager.mainScenePanel.gameObject.SetActive(false);
        sceneManager.mainScenePanel.GetChild(0).GetComponent<Text>().text = "Loading...";
        // StopAllCoroutines();
        Resources.UnloadUnusedAssets();
        GC.Collect();
		saveTextForGUI = "saved";
		needDisplaySave = false;

    }

    public void LoadData()
    {
        Debug.Log("Load");
        var finishCount = PlayerPrefs.GetInt("FinishCount");
        var MainBtnsCount = PlayerPrefs.GetInt("MainBtnsCount");

        materialID = ES2.LoadList<string>("NewMaterialID");
        materialName = ES2.LoadList<string>("NewMaterialName");

        for (int i = 0; i < finishCount; i++)
        {
            var fButton = Instantiate(GetComponent<SceneManager>().m_finishUI);
            fButton.transform.SetParent(GetComponent<SceneManager>().m_finishUIParent, false);
        }

        for (int i = 0; i < MainBtnsCount; i++)
        {
            var mButton = Instantiate(GetComponent<SceneManager>().m_modelsUI);
            mButton.transform.SetParent(GetComponent<SceneManager>().m_modelsUIParent, false);
        }


        for (int i = 0; i < finishesContent.childCount; i++)
        {
            Debug.Log(i);
            finishesContent.GetChild(i).GetChild(0).GetComponent<FinishButton>().LoadData(i);
        }

		parentForMainScroll.GetComponent<SetupHeightOnScroll> ().Setup ();


        sceneManager.LoadFinishes();
		modelsCountText.text = parentForMainScroll.childCount.ToString();
		filterModelsCountText.text = parentForMainScroll.childCount.ToString() + " Models";
    }

   

    IEnumerator BuildScene()
    {

        if (PlayerPrefs.HasKey("userID"))
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                isOfflineMode = true;
                firstCharacterCompanyName.text = PlayerPrefs.GetString("FirstCharacter");

                var cn = PlayerPrefs.GetString("CompanyName");
                for (int i = 0; i < CompanyNameList.Count; i++)
                {
                    CompanyNameList[i].text = cn;
                }

                Debug.Log("OfflineMode");
                
                loadingCanvas.gameObject.SetActive(true);
                camController = FindObjectOfType<MainCameraController>();
                currentUserSate = currentSate.Finishes;
                currentMySwitchState = currentSwitchState.Rotate;
                ai.StartAnim();
                LoadData();
            }
            else
            {
                isOfflineMode = false;
                ai.needWait = true;
                Debug.Log("Get Saves");
                Debug.Log("OnlineMode");
                userID = PlayerPrefs.GetString("userID");
                var testDDPAccount = FindObjectOfType<TestDdpAccount>();
                loadingCanvas.gameObject.SetActive(true);
                yield return new WaitForSeconds(3.5f);
                testDDPAccount.GetUserDetails();
                camController = FindObjectOfType<MainCameraController>();
                //CloseAllCanvases();
                loadingCanvas.gameObject.SetActive(true);
                currentUserSate = currentSate.Finishes;
                currentMySwitchState = currentSwitchState.Rotate;
            }
            

            //if (needDelete)
            //{
            //    ES2.Delete("save.txt");
            //}
            //else
            //{
            //    CreateBtnInCollectionViaLoading();
            //}

        }

        else
        {
            Debug.Log("noSaves");
            camController = FindObjectOfType<MainCameraController>();
            CloseAllCanvases();
            loginCanvas.gameObject.SetActive(true);
            currentUserSate = currentSate.Finishes;
            currentMySwitchState = currentSwitchState.Rotate;
            // DeleteButtonsInSavedContent();

            //if (needDelete)
            //{
            //    ES2.Delete("save.txt");
            //}
            //else
            //{
            //    CreateBtnInCollectionViaLoading();
            //}
        }
       
    }


    IEnumerator MyltipleLoading()
    {

        img = new List<bool> { false, false, false, false, false, false, false, false, false, false, false, false };
        var f = false;
        var s = false;
        var t = false;
        var th = false;
        var fi = false;
        var fth = false;
        var six = false;
        var sev = false;
        var eight = false;
        var nine = false;
        var ten = false;
        var eleven = false;

        if (counter < parentForMainScroll.childCount)
        {
            StartCoroutine(StartLoad(counter, 0));
            counter++;
        }
        else
        {
            f = true;
        }

        if (counter < parentForMainScroll.childCount)
        {
            StartCoroutine(StartLoad(counter, 1));
            counter++;
        }
        else
        {
            s = true;
        }
        if (counter < parentForMainScroll.childCount)
        {
            StartCoroutine(StartLoad(counter, 2));
            counter++;
        }
        else
        {
            t = true;
        }

        if (counter < parentForMainScroll.childCount)
        {
            StartCoroutine(StartLoad(counter, 3));
            counter++;
        }
        else
        {
            th = true;
        }
        if (counter < parentForMainScroll.childCount)
        {
            StartCoroutine(StartLoad(counter, 4));
            counter++;
        }
        else
        {
            fi = true;
        }
        if (counter < parentForMainScroll.childCount)
        {
            StartCoroutine(StartLoad(counter, 5));
            counter++;
        }
        else
        {
            fth = true;
        }

        if (counter < parentForMainScroll.childCount)
        {
            StartCoroutine(StartLoad(counter, 6));
            counter++;
        }
        else
        {
            six = true;
        }

        if (counter < parentForMainScroll.childCount)
        {
            StartCoroutine(StartLoad(counter, 7));
            counter++;
        }
        else
        {
            sev = true;
        }

        if (counter < parentForMainScroll.childCount)
        {
            StartCoroutine(StartLoad(counter, 8));
            counter++;
        }
        else
        {
            eight = true;
        }

        if (counter < parentForMainScroll.childCount)
        {
            StartCoroutine(StartLoad(counter, 9));
            counter++;
        }
        else
        {
            nine = true;
        }

        if (counter < parentForMainScroll.childCount)
        {
            StartCoroutine(StartLoad(counter, 10));
            counter++;
        }
        else
        {
            ten = true;
        }

        if (counter < parentForMainScroll.childCount)
        {
            StartCoroutine(StartLoad(counter, 11));
            counter++;
        }
        else
        {
            eleven = true;
        }





        if (t || s || f || th || fi || fth || six || sev || eight || nine || ten || eleven)
        {
            //StopAllCoroutines();
            modelsCountText.text = string.Empty;
            modelsCountText.text = parentForMainScroll.childCount.ToString();
            filterModelsCountText.text = parentForMainScroll.childCount.ToString() + " Models";
            Debug.Log("finishLoadingThymbail");
            SortMainBtns();
            //offline Support
            //StartSave();
            //SaveData();

        }


        yield return new WaitUntil(() => img[0] && img[1] && img[2] && img[3] && img[4] && img[5] && img[6] && img[7] && img[8] && img[9] && img[10] && img[11]);
        StartCoroutine(MyltipleLoading());
    }

    //IEnumerator MyltipleLoading()
    //{
    //    for (int i = 0; i < parentForMainScroll.childCount; i++)
    //    {
    //        Texture2D tex;
    //        tex = new Texture2D(1, 1, TextureFormat.ARGB32, false);
    //        var btn = modelsUIParent.GetChild(i);
    //        var stImg = modelsUIParent.GetChild(i).GetChild(0).GetComponent<ModelButton>().style_Thumbnail_Img;
    //        using (WWW www = new WWW(stImg))
    //        {
    //            yield return www;
    //            www.LoadImageIntoTexture(tex);
    //            Sprite sprite = Sprite.Create((Texture2D)tex, new Rect(0, 0, tex.width, tex.height), new Vector2(1.0f, 1.0f));
    //            btn.GetChild(0).GetChild(0).GetChild(0).GetComponent<Image>().sprite = sprite;

    //            var coff = (float)tex.width / (float)tex.height;

    //            btn.GetChild(0).GetChild(0).GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(btn.GetChild(0).GetChild(0).GetChild(0).GetComponent<RectTransform>().sizeDelta.x * coff, btn.GetChild(0).GetChild(0).GetChild(0).GetComponent<RectTransform>().sizeDelta.y);


    //        }
    //        if (i == parentForMainScroll.childCount - 1)
    //        {
    //            modelsCountText.text = parentForMainScroll.childCount.ToString();
    //            filterModelsCountText.text = parentForMainScroll.childCount.ToString() + " Models";
    //            Debug.Log("finishLoadingThymbail");
    //            SortMainBtns();
    //        }


    //    }
    //    yield return 0;


    //}

    IEnumerator StartLoad(int index, int boolIndex)
    {

        Debug.Log(index + " " + boolIndex);
        Texture2D tex;
        tex = new Texture2D(1, 1, TextureFormat.ARGB32, false);
        var btn = modelsUIParent.GetChild(index);
        var stImg = modelsUIParent.GetChild(index).GetChild(0).GetComponent<ModelButton>().style_Thumbnail_Img;
        var price = modelsUIParent.GetChild(index).GetChild(0).GetComponent<ModelButton>().style_price;
        using (WWW www = new WWW(stImg))
        {
            yield return www;
            www.LoadImageIntoTexture(tex);
            Sprite sprite = Sprite.Create((Texture2D)tex, new Rect(0, 0, tex.width, tex.height), new Vector2(1.0f, 1.0f));
            btn.GetChild(0).GetChild(0).GetChild(0).GetComponent<Image>().sprite = sprite;

            var coff = (float)tex.width / (float)tex.height;
            btn.GetChild(0).GetChild(2).GetComponent<Text>().text = price;
            btn.GetChild(0).GetChild(0).GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(btn.GetChild(0).GetChild(0).GetChild(0).GetComponent<RectTransform>().sizeDelta.x * coff, btn.GetChild(0).GetChild(0).GetChild(0).GetComponent<RectTransform>().sizeDelta.y);

            if (www.isDone)
            {
                img[boolIndex] = true;
            }
        }
        

       
    }



    //IEnumerator StartLoad()
    //{
    //    Texture2D tex;
    //    tex = new Texture2D(1, 1, TextureFormat.ARGB32, false);

    //    if (counter < parentForMainScroll.childCount)
    //    {
    //        var btn = modelsUIParent.GetChild(counter);
    //        var stImg = modelsUIParent.GetChild(counter).GetChild(0).GetComponent<ModelButton>().style_Thumbnail_Img;
    //        var price = modelsUIParent.GetChild(counter).GetChild(0).GetComponent<ModelButton>().style_price;

    //        using (WWW www = new WWW(stImg))
    //        {
    //            yield return www;
    //            www.LoadImageIntoTexture(tex);
    //            Sprite sprite = Sprite.Create((Texture2D)tex, new Rect(0, 0, tex.width, tex.height), new Vector2(1.0f, 1.0f));
    //            btn.GetChild(0).GetChild(0).GetChild(0).GetComponent<Image>().sprite = sprite;

    //            var coff = (float)tex.width / (float)tex.height;
    //            btn.GetChild(0).GetChild(2).GetComponent<Text>().text = price;
    //            btn.GetChild(0).GetChild(0).GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(btn.GetChild(0).GetChild(0).GetChild(0).GetComponent<RectTransform>().sizeDelta.x * coff, btn.GetChild(0).GetChild(0).GetChild(0).GetComponent<RectTransform>().sizeDelta.y);

    //            if (www.isDone)
    //            {
    //                counter++;
    //                StartCoroutine(StartLoad());
    //            }
    //        }
    //    }

    //    else
    //    {
    //        modelsCountText.text = string.Empty;
    //        modelsCountText.text = parentForMainScroll.childCount.ToString();
    //        filterModelsCountText.text = parentForMainScroll.childCount.ToString() + " Models";
    //        Debug.Log("finishLoadingThymbail");
    //        StartSave();
    //    }
    //}

    public void StartSave()
    {
		needDisplaySave = true;
        Debug.Log("start Save");
        //StartCoroutine(LoadAllSaveTextures()); 
        // StartCoroutine(LoadAllTexturesOnDisk());
        justLoad();
    }

	void OnGUI()
	{

		if (needDisplaySave) 
		{
			int w = Screen.width, h = Screen.height;

			GUIStyle style = new GUIStyle();

			Rect rect = new Rect(0, 0, w, h * 2 / 100);
			style.alignment = TextAnchor.UpperLeft;
			style.fontSize = h * 2 / 100;
			style.normal.textColor = Color.white;

			string text = "saving...";
			GUI.Label(rect, text, style);
		}





	}

    public void InstatiateFinishBTN()
    {
        
        sceneManager.InstatiateFinishBtn();
    }


    IEnumerator WaitForSort()
    {

        yield return new WaitForSeconds(1f);
        SortMainBtns();
        yield return new WaitForSeconds(1f);
        loadingCanvas.gameObject.SetActive(false);
    }
    
    public void OpenMainMenuCanvas()
    {
        if (isLoadiiiing)
        {
			if (!isOfflineMode) 
			{
				StartCoroutine(StartLoadThymbailImage());
			}
            
            currentOpenCunvas = openCanvas.MainMenu;
            CloseAllCanvases();
            //loadingCanvas.gameObject.SetActive(false);
            //mainCanvas.gameObject.SetActive(true);
           // StartCoroutine(WaitForSort());
            counter = 0;
           
            isLoadiiiing = false;
            DeleteStylesBtn();
           
            
        }

        if (currentOpenCunvas == openCanvas.View)
        {
            
                camController.enabled = false;
                var currentGO = GameObject.FindWithTag("CurrentObjParent").gameObject;
                Destroy(currentGO.gameObject);
                //isSave = false;
                currentOpenCunvas = openCanvas.MainMenu;
                CloseAllCanvases();
                mainCanvas.gameObject.SetActive(true);
               // customMaterialPartName.Clear();
            DeleteStylesBtn();
            // CreateBtnInCollectionRuntime();
            //}
        }

        else
        {
            //isSave = false;
            currentOpenCunvas = openCanvas.MainMenu;
            CloseAllCanvases();
            mainCanvas.gameObject.SetActive(true);
           // customMaterialPartName.Clear();
            DeleteStylesBtn();
            //CreateBtnInCollectionRuntime();
        }


    }

    public IEnumerator StartLoadThymbailImage()
    {
        yield return new WaitForSeconds(1f);

        StartCoroutine(MyltipleLoading());
    }

   

    public void BoardDetailsCanvas()
    {
       
        if (!detailBoard.gameObject.activeSelf)
        {
            detailBoard.gameObject.SetActive(true);
        }
        else
        {
            for (int i = 0; i < boardDetailsContent0.childCount; i++)
            {
                Destroy(boardDetailsContent0.GetChild(i).gameObject);
            }
            for (int i = 0; i < boardDetailsContent1.childCount; i++)
            {
                Destroy(boardDetailsContent1.GetChild(i).gameObject);
            }
            detailBoard.gameObject.SetActive(false);
        }

    }



    public void OpenMainBoardCanvas()
    { 
		if (isARMode)
        {
            CloseAR();
        }
        if (colorPicker.gameObject.activeSelf)
        {
            colorPicker.gameObject.SetActive(false);
        }

        if (currentOpenCunvas == openCanvas.Customize)
        {

            camController.enabled = false;
            var currentGO = GameObject.FindWithTag("CurrentObjParent").gameObject;
            Destroy(currentGO.gameObject);
            //isSave = false;
            currentOpenCunvas = openCanvas.Collection;
            CloseAllCanvases();
            mainBoardCanvas.gameObject.SetActive(true);
           // customMaterialPartName.Clear();
          //  CreateBtnInCollectionRuntime();
        //    }
        }
        else
        {
                //isSave = false;
                currentOpenCunvas = openCanvas.Collection;
                CloseAllCanvases();
                mainBoardCanvas.gameObject.SetActive(true);
                camController.enabled = false;
                var currentGO = GameObject.FindWithTag("CurrentObjParent").gameObject;
                Destroy(currentGO.gameObject);
               // customMaterialPartName.Clear();
              //  CreateBtnInCollectionRuntime();
         }
    }

    // CanvasCategoryInCollection
    public void OpenCategoryInCollection()
    {
        currentOpenCunvas = openCanvas.CategoryInCollection;
        CloseAllCanvases();
        categoryInCollectionCanvas.gameObject.SetActive(true);
        CreateBtnInCollectionRuntime();
    }

    // open view Canvas
    public void OpenViewCanvas()
    {

		if (isARMode) 
		{
			CloseAR ();	

			currentOpenCunvas = openCanvas.View;
		} 

		else 
		
		{
			if (qrLoadingCanvas.activeSelf)
			{
				qrLoadingCanvas.SetActive(false);
			}

			if (colorPicker.gameObject.activeSelf)
			{
				colorPicker.gameObject.SetActive(false);
			}

			currentOpenCunvas = openCanvas.View;
			CloseAllCanvases();
			viewCanvas.gameObject.SetActive(true);

			viewStyleCount.text = string.Empty;
			viewModelName.text = string.Empty;
			viewPrice.text = string.Empty;
			viewModelName.text = currentProductNameHelper;

			if (currentProductPriceHelper == "")
			{
				viewPriceIcon.gameObject.SetActive(false);
				styleParent.anchoredPosition = new Vector2(-53.4f, 48.13335f);
			}
			else
			{
				viewPriceIcon.gameObject.SetActive(true);
				viewPrice.text = currentProductPriceHelper;
				styleParent.anchoredPosition = new Vector2(0, 48.13335f);

			}

			if (currentStyleHelper == "")
			{
				viewStyleIcon.gameObject.SetActive(false);
			}
			else
			{
				if (int.Parse(currentStyleHelper) > 1)
				{
					viewStyleCount.text = currentStyleHelper + " Styles";
					viewStyleIcon.gameObject.SetActive(true);
				}
				if (int.Parse(currentStyleHelper) == 1)
				{
					viewStyleCount.text = currentStyleHelper + " Style";
					viewStyleIcon.gameObject.SetActive(true);
				}
			}
			camController.StartCoroutine(camController.CustomZoomOut(camController.distance, camController.distanceZoomOut, 0.8f));
			camController.enabled = true;
		}


        
       

    }

    public void OpenCustomizeCanvas()
    {
        parentObj = GameObject.FindWithTag("CurrentObjParent");
        currentOpenCunvas = openCanvas.Customize;
        CloseAllCanvases();
        customizeCanvas.gameObject.SetActive(true);
        EnableUnderline();

        customizeModelName.text = string.Empty;
        customizePrice.text = string.Empty;

        customizeModelName.text = currentProductNameHelper;
       
        customizeStyleCount.text = string.Empty;

        if (currentStyleHelper == "")
        {
            customizeStyleIcon.gameObject.SetActive(false);
        }
        else
        {
            if (int.Parse(currentStyleHelper) > 1)
            {
                customizeStyleCount.text = currentStyleHelper + " Styles";
                customizeStyleIcon.gameObject.SetActive(true);
            }
            else
            {
                customizeStyleCount.text = currentStyleHelper + " Style";
                customizeStyleIcon.gameObject.SetActive(true);
            }
        }

        if (currentProductPriceHelper == "")
        {
            customizePriceIcon.gameObject.SetActive(false);

            styleParentCustomizeCanvas.anchoredPosition = new Vector2(-120f, -70.75f);

        }
        else
        {
            customizePriceIcon.gameObject.SetActive(true);
            styleParentCustomizeCanvas.anchoredPosition = new Vector2(-63.99999f, -70.75f);
            customizePrice.text = currentProductPriceHelper;
        }

        

       // var uiDropdown = FindObjectOfType<UIDropdown>();

       // uiDropdown.regionName.Clear();

       // for (int i = 0; i < regionNameHelper.Count; i++)
       // {
       //     uiDropdown.regionName.Add(regionNameHelper[i]);
       // }
        counter = 0;
        counterSum = 0;
        SetupModelTag();

        //uiDropdown.AddLoadName();
        PartStart();

        needDisplaySelectedIcon = new List<bool>(regionNameHelper.Count);

        for (int i = 0; i < regionNameHelper.Count; i++)
        {
            needDisplaySelectedIcon.Add(true);
        }



        for (int i = 0; i < parentForFinishBtn.childCount; i++)
        {
            parentForFinishBtn.GetChild(i).GetChild(0).GetChild(1).gameObject.SetActive(false);
            parentForFinishBtn.GetChild(i).GetChild(0).GetComponent<FinishButton>().notChangeIcon = false;
        }

        for (int i = 0; i < parentForFinishBtn.childCount; i++)
        {
            for (int j = 0; j < currentFinishesID.Count; j++)
            {
                if (currentFinishesID[j] == parentForFinishBtn.GetChild(i).GetChild(0).GetComponent<FinishButton>().finishID)
                {
                    parentForFinishBtn.GetChild(i).GetChild(0).GetChild(1).gameObject.SetActive(true);
                    parentForFinishBtn.GetChild(i).GetChild(0).GetComponent<FinishButton>().notChangeIcon = true;
                }
            }
        }

        if (needActiveColorPicker)
        {
            EnablePicker();
        }
        else
        {
            DisableColorPicker();
        }


        //для трансформа и ратэйт
        // ShapesButtonsSwitchState();
        //~~~~~~~~~~~~~~~~~~~~~~~
        //EnableOutline();

    }

    public void ClearRegionsList()
    {
        if (needClearList && loadingModelCounter > 1)
        {
            regionName.Clear();
            regionNameHelper.Clear();
            polygon_GroupIDCountHelper.Clear();
            needClearList = false;
        }
    }

    public void SetupModelTag()
    {
        var parent = GameObject.FindWithTag("CurrentObjParent").gameObject;

        for (int i = 0; i < parent.transform.childCount; i++)
        {
            for (int j = 0; j < currentRegionName.Count; j++)
            {
           
                if (PlayerPrefs.HasKey(currentProductID + currentRegionName[j] + parent.transform.GetChild(i).name))
                {
                    parent.transform.GetChild(i).GetComponent<ModelCustomTag>().customTag = currentRegionName[j];
                }
            }
        }
        currentRegionName.Clear();
 }

    public void OpenSettingCanvas()
    {
		if (isARMode)
        {
            CloseAR();
        }

        if (colorPicker.gameObject.activeSelf)
        {
            colorPicker.gameObject.SetActive(false);
        }
        if (currentOpenCunvas == openCanvas.Customize)
        {
            camController.enabled = false;
            var currentGO = GameObject.FindWithTag("CurrentObjParent").gameObject;
            Destroy(currentGO.gameObject);
            // isSave = false;
            currentOpenCunvas = openCanvas.Setting;
            CloseAllCanvases();
            settingCanvas.gameObject.SetActive(true);
           // customMaterialPartName.Clear();

        }
        else
        {
            currentOpenCunvas = openCanvas.Setting;
            CloseAllCanvases();
            settingCanvas.gameObject.SetActive(true);
           // isSave = false;
            camController.enabled = false;
            var currentGO = GameObject.FindWithTag("CurrentObjParent").gameObject;
            Destroy(currentGO.gameObject);
           // customMaterialPartName.Clear();
        }
    }

   

    public void OpenManufacturerCanvas()
    {
        manufacturerCanvas.gameObject.SetActive(true);
    }

    public void CloseManufacturerCanvas()
    {
        manufacturerCanvas.gameObject.SetActive(false);
    }

    public void OpenPartsPopup()
    {
        partsPopupCanvas.SetActive(true);
    }

    public void OpenFilterCanvas()
    {
        filterCanvas.gameObject.SetActive(true);

        if (!isPriceShow)
        {
            PriceTr1.SetActive(false);
            PriceTr2.SetActive(false);
        }
    }


    public void CloseFilterCanvas()
    {
        filterCanvas.gameObject.SetActive(false);
    }

    public void OpenCategoryCanvas()
    {
        categoryCanvas.gameObject.SetActive(true);
    }

    public void CloseCategoryCanvas()
    {
        categoryCanvas.gameObject.SetActive(false);
    }

    public void OpenOrdersDetailsCanvas()
    {
        if (colorPicker.gameObject.activeSelf)
        {
            colorPicker.gameObject.SetActive(false);
        }
        ordersDetailsCanvas.gameObject.SetActive(true);
    }

    public void CloseOrdersDetailsCanvas()
    {
        ordersDetailsCanvas.gameObject.SetActive(false);
    }

    public void OpenSearchCanvas()
    {
        searchCanvas.gameObject.SetActive(true);
    }

    public void OpenOrdersCanvas()
    {

		if (isARMode)
        {
            CloseAR();
        }

        if (currentOpenCunvas == openCanvas.Customize)
        {


            camController.enabled = false;
            var currentGO = GameObject.FindWithTag("CurrentObjParent").gameObject;
            Destroy(currentGO.gameObject);
           // isSave = false;
            currentOpenCunvas = openCanvas.Orders;
            CloseAllCanvases();
            ordersCanvas.gameObject.SetActive(true);
           // customMaterialPartName.Clear();
        }
    
        else
        {

            currentOpenCunvas = openCanvas.Orders;
            CloseAllCanvases();
            ordersCanvas.gameObject.SetActive(true);
            //isSave = false;
            camController.enabled = false;
            var currentGO = GameObject.FindWithTag("CurrentObjParent").gameObject;
            Destroy(currentGO.gameObject);
           // customMaterialPartName.Clear();
        }
    }


    void ShapesButtonsSwitchState()
    {

        if (currentUserSate == currentSate.Shapes)
        {

            if (castomizePanelUp)
            {
                if (currentMySwitchState == currentSwitchState.Rotate)
                {
                    textRotButton.gameObject.SetActive(true);
                    shapesButtons.gameObject.SetActive(true);
                    transformButtons.gameObject.SetActive(false);
                    rotButtons.gameObject.SetActive(true);


                }
                if (currentMySwitchState == currentSwitchState.Transform)
                {
                    textRotButton.gameObject.SetActive(true);
                    shapesButtons.gameObject.SetActive(true);
                    transformButtons.gameObject.SetActive(true);
                    rotButtons.gameObject.SetActive(false);
                }
            }

            else
            {
                shapesButtons.gameObject.SetActive(false);
                textRotButton.gameObject.SetActive(false);
            }
        }
        else
        {
            shapesButtons.gameObject.SetActive(false);
            textRotButton.gameObject.SetActive(false);
        }
    }




    // method in MAIN_SCROLL which load models. 
    public void Loading3DMODEL(string modelName)
    {

        OpenViewCanvas();
        var loadingObj = Resources.Load("DefaultModelsPref/" + modelName) as GameObject;
        var GO = GameObject.Instantiate(loadingObj, transform.position, transform.rotation);

        if (GameObject.FindWithTag("CurrentObjParent").transform != null)
        {
            camController.target = GameObject.FindWithTag("CurrentObjParent").transform;
        }

    }


    public void CloseAllCanvases()
    {
        for (int i = 0; i < allCanvases.Count; i++)
        {
            allCanvases[i].SetActive(false);
        }
    }

    public void Culculate()
    {
        GameObject.Find(partName).GetComponent<GetVolumeAndArea>().enabled = true;
    }

    public void CloseCalculatePopup()
    {
        GameObject.Find(partName).GetComponent<GetVolumeAndArea>().enabled = false;
    }

    public void SwitchRotationOrTransform()
    {
        if (rotButtons.gameObject.activeSelf)
        {

            rotButtons.gameObject.SetActive(false);
            transformButtons.gameObject.SetActive(true);
            textRotTr.text = "rotate";

        }
        else
        {
            transformButtons.gameObject.SetActive(false);
            rotButtons.gameObject.SetActive(true);
            textRotTr.text = "transform";

        }
    }


    //public void EnableOutline()
    //{
    //    GameObject.FindWithTag("MainCamera").GetComponent<cakeslice.OutlineEffect>().enabled = true;
    //    List<GameObject> listCurrentGO = new List<GameObject>(GameObject.FindGameObjectsWithTag("ChildObj1"));

    //    for (int i = 0; i < listCurrentGO.Count; i++)
    //    {
    //        if (partName != null)
    //        {
    //            if (listCurrentGO[i].name == partName)
    //            {
    //                listCurrentGO[i].GetComponent<cakeslice.Outline>().enabled = true;
    //            }
    //            else
    //            {
    //                listCurrentGO[i].GetComponent<cakeslice.Outline>().enabled = false;
    //            }
    //        }
    //    }
    //    listCurrentGO.Clear();

    //}

    //public void DisableOutline()
    //{
    //    List<GameObject> listCurrentGO = new List<GameObject>(GameObject.FindGameObjectsWithTag("ChildObj1"));

    //    for (int i = 0; i < listCurrentGO.Count; i++)
    //    {
    //        listCurrentGO[i].GetComponent<cakeslice.Outline>().enabled = false;
    //    }
    //    GameObject.FindWithTag("MainCamera").GetComponent<cakeslice.OutlineEffect>().enabled = false;
    //    listCurrentGO.Clear();
    //}
    public void DisableAllSelectIcon()
    {
        for (int i = 0; i < ShapesScrollViewContent.childCount; i++)
        {
            ShapesScrollViewContent.GetChild(i).GetChild(0).GetChild(1).gameObject.SetActive(false);
        }
    }


    public void CloseCavas(Transform canvas)
    {
        canvas.gameObject.SetActive(false);
    }

    public void OpenForgotPassportCavas(Transform canvas)
    {
        canvas.gameObject.SetActive(true);
    }

    void EnableUnderline()
    {
        for (int i = 0; i < underLines.Count; i++)
        {
            underLines[i].transform.GetChild(1).gameObject.SetActive(false);
            underLines[i].transform.GetChild(0).GetComponent<Text>().color = new Color32(155,155,155,255);
        }

        switch (currentUserSate)
        {
            case currentSate.Shapes:
                underLines[1].transform.GetChild(1).gameObject.SetActive(true);
                underLines[1].transform.GetChild(0).GetComponent<Text>().color = Color.black;
                break;
            case currentSate.Finishes:
                underLines[0].transform.GetChild(1).gameObject.SetActive(true);
                underLines[0].transform.GetChild(0).GetComponent<Text>().color = Color.black;
                break;
            case currentSate.Patterns:
                underLines[2].transform.GetChild(1).gameObject.SetActive(true);
                underLines[2].transform.GetChild(0).GetComponent<Text>().color = Color.black;
                break;
            default:
                break;
        }
    }

    void DisableHighlightFinishBtn()
    {
        for (int i = 0; i < finishesContent.childCount; i++)
        {
            finishesContent.GetChild(i).GetChild(0).gameObject.SetActive(false);
        }
    }

    public void EnableShapesScroll()
    {
        currentUserSate = currentSate.Shapes;
        finishesScrollView.gameObject.SetActive(false);
        patternsScrollView.gameObject.SetActive(false);
        shapesScrollView.gameObject.SetActive(true);
        controlsForTextLogos.gameObject.SetActive(false);
        //ShapesButtonsSwitchState();
        EnableUnderline();
        //EnableOutline();
        controlsForTextLogos.gameObject.SetActive(false);
       // CheckColorPickerBtnState();
    }

    public void EnableFinishesScroll()
    {
        currentUserSate = currentSate.Finishes;
        shapesScrollView.gameObject.SetActive(false);
        patternsScrollView.gameObject.SetActive(false);
        controlsForTextLogos.gameObject.SetActive(false);
        finishesScrollView.gameObject.SetActive(true);
        EnableUnderline();
       // ShapesButtonsSwitchState();
       // CheckColorPickerBtnState();
    }

    public void EnablePatternsScroll()
    {
        currentUserSate = currentSate.Patterns;
        shapesScrollView.gameObject.SetActive(false);
        finishesScrollView.gameObject.SetActive(false);
        controlsForTextLogos.gameObject.SetActive(false);
        patternsScrollView.gameObject.SetActive(true);
        EnableUnderline();
       // ShapesButtonsSwitchState();
       // CheckColorPickerBtnState();
    }

    public void OpenViewPopupCanvas()
    {
        CloseAllCanvases();
        allCanvases[17].gameObject.SetActive(true);
        viewPopupDescription.text = currentStyleDescription;
        viewPopupModelName.text = string.Empty;
        viewPopupPrice.text = string.Empty;
        viewPopupModelName.text = currentProductNameHelper;
        var cd = FindObjectOfType<CheckText1>();
        cd.Check();
        viewPopupStyleCount.text = string.Empty;

        if (currentStyleHelper == "")
        {
            viewPopupStyleIcon.gameObject.SetActive(false);
        }
        else
        {

            if (int.Parse(currentStyleHelper) > 1)
            {
                viewPopupStyleCount.text = currentStyleHelper + " Styles";
                viewPopupStyleIcon.gameObject.SetActive(true);
            }
            else
            {
                viewPopupStyleCount.text = currentStyleHelper + " Style";
                viewPopupStyleIcon.gameObject.SetActive(true);
            }
        }

        if (currentProductPriceHelper == "")
        {
            viewPopupPriceIcon.gameObject.SetActive(false);
            styleParentViewPopup.anchoredPosition = new Vector2(-55.5f, -333f);
        }
        else
        {
            styleParentViewPopup.anchoredPosition = new Vector2(0f, -333f);
            viewPopupPrice.text = currentProductPriceHelper;
            viewPopupPriceIcon.gameObject.SetActive(true);
        }
    }

    public void ShowMoreViewPopup()
    {

    }

    public void EnableColorPickerScripts()
    {
        GameObject[] allGO = GameObject.FindGameObjectsWithTag("ChildObj1");
        for (int i = 0; i < allGO.Length; i++)
        {
            allGO[i].GetComponent<ColorPickerTester>().enabled = false;
        }
        GameObject.Find(partName).GetComponent<ColorPickerTester>().enabled = true;
    }

    public void DisableColorPickerScripts()
    {
        GameObject[] allGO = GameObject.FindGameObjectsWithTag("ChildObj1");
        for (int i = 0; i < allGO.Length; i++)
        {
            allGO[i].GetComponent<ColorPickerTester>().enabled = false;
        }
       
    }

    //public void SwitchColorPicker()
    //{
    //    if (currentUserSate == currentSate.Finishes)
    //    {
    //        if (colorPicker.gameObject.activeSelf)
    //        {
    //            colorPicker.gameObject.SetActive(false);
    //            DisableColorPickerScripts();
    //        }
    //        else
    //        {
    //            colorPicker.gameObject.SetActive(true);
    //            EnableColorPickerScripts();
    //        }
    //    }
    //}

    public void DisableColorPicker()
    {
        colorPicker.gameObject.SetActive(false);
        offColorPicker.SetActive(false);
        onColorPicker.SetActive(false);
    }

    // отк вкл колор пикер
    public void EnablePicker()
    {
       

            colorPicker.gameObject.SetActive(true);
            offColorPicker.SetActive(true);
            onColorPicker.SetActive(true);
        colorPicker.GetComponent<ColorPicker>().SendIvent();
            var parent = GameObject.FindWithTag("CurrentObjParent");

            for (int i = 0; i < parent.transform.childCount; i++)
            {
                parent.transform.GetChild(i).GetComponent<ColorPickerTester>().DisableColorPicker();
            }

            for (int i = 0; i < parent.transform.childCount; i++)
            {

                if (parent.transform.GetChild(i).GetComponent<ModelCustomTag>().customTag == currentRegionNameHelper)
                {
                    Debug.Log("current tag = " + currentRegionNameHelper);
                    parent.transform.GetChild(i).GetComponent<ColorPickerTester>().EnableColorPicker();

                }

            }

            hexColorField.CustomAddLisener();
            colorImage.CuasomAddLisener();
    }

    public void ChangeColorPickerRegion()
    {
        var parent = GameObject.FindWithTag("CurrentObjParent");

        for (int i = 0; i < parent.transform.childCount; i++)
        {
            parent.transform.GetChild(i).GetComponent<ColorPickerTester>().DisableColorPicker();
        }

        hexColorField.CustomAddLisener();
        colorImage.CuasomAddLisener();
        for (int i = 0; i < parent.transform.childCount; i++)
        {
            if (parent.transform.GetChild(i).GetComponent<ModelCustomTag>().customTag == currentRegionNameHelper)
            {
                parent.transform.GetChild(i).GetComponent<ColorPickerTester>().EnableColorPicker();
            }
        }

    }

    public void OpenAddTextPanel()
    {
        addTextPanel.gameObject.SetActive(true);
        mainInputField.text = string.Empty;
    }

    public void OpenProfileCanvas()
    {
        profileCanvas.gameObject.SetActive(true);
    }

    public void CloseProfileCanvas()
    {
        profileCanvas.gameObject.SetActive(false);
    }

    public void AddTextAndClose()
    {
        if (mainInputField.text != null)
        {
            var currentGO = GameObject.Find(partName);
            currentGO.GetComponent<AddTextToTexture>().text = mainInputField.text;
           // customTextList.Add(mainInputField.text);
            currentGO.GetComponent<AddTextToTexture>().CreateAndAddTextToTexture();
        }
        addTextPanel.gameObject.SetActive(false);
        mainInputField.text = string.Empty;

    }

    public void ChangeMaterial(Material mat)
    {
        DisableHighlightFinishBtn();
        var go = GameObject.Find(partName);
        go.GetComponent<MeshRenderer>().material = mat;
    }

    
    public void InstatieteShapes(GameObject GO)
    {
        ClearAllTicks();

        var parent = GameObject.FindWithTag("CurrentObjParent");
        for (int i = 2; i < parent.transform.childCount; i++)
        {
            Destroy(parent.transform.GetChild(i).gameObject);
        }

        GameObject tempGO = Instantiate(GO, transform.position, transform.rotation);
       
        tempGO.transform.parent = parent.transform;
        tempGO.GetComponent<MeshRenderer>().materials[0] = Resources.Load("Materials/DefaultMaterial") as Material;

        var ui = FindObjectOfType<UIDropdown>();
        ui.AddItemToDropdown(tempGO.name, false);

        StartCoroutine(WaitForUpdateParts());
    }

    IEnumerator WaitForUpdateParts()
    {
        var ui = FindObjectOfType<UIDropdown>();
        yield return new WaitForSeconds(0.3f);
        ui.m_dropdown.onValueChanged.RemoveAllListeners();
        //StartCoroutine(ui.FillUiDropdown());
        PartStart();
    }

    void PartStart()
    {
        currentRegionNameHelper = regionNameHelper[0];
        currentDropdownIndex = 0;
        currentPartText.text = regionNameHelper[0];
        currentMaterialBtn = regionNameHelper[0];

        for (int i = 0; i < materialParent.childCount; i++)
        {
            Destroy(materialParent.GetChild(i).gameObject);
        }


        if (ES2.Exists(currentProductID + regionNameHelper[0]))
        {
            matID = ES2.LoadList<string>(currentProductID + regionNameHelper[0]);
            for (int i = 0; i < matID.Count; i++)
            {
                var btn = Instantiate(materialsCanvasBtn);
                btn.transform.SetParent(materialParent, false);

                if (i == 0)
                {
                    btn.transform.GetChild(1).gameObject.SetActive(true);
                }

                for (int j = 0; j < materialID.Count; j++)
                {
                    if (matID[i] == materialID[j])
                    {

                        currentMaterialNameHelper = materialName[j];
                        currentMaterialText.text = materialName[j];

                        btn.transform.GetChild(2).GetComponent<Text>().text = materialName[j];
                    }
                }

            }
            FilterMaterial();
        }
    }

    public void OpenMaterialsPopup()
    {
        materialCanvas.SetActive(true);
    }


    void FilterMaterial()
    {
        Debug.Log("filter");
        for (int i = 0; i < sceneManager.m_finishUIParent.childCount; i++)
        {
            sceneManager.m_finishUIParent.GetChild(i).gameObject.SetActive(true);
        }



        for (int i = 0; i < sceneManager.m_finishUIParent.childCount; i++)
        {
            var trigger = false;
            for (int j = 0; j < sceneManager.m_finishUIParent.GetChild(i).GetChild(0).GetComponent<FinishButton>().fBaseMatID.Count; j++)
            {
                for (int k = 0; k < matID.Count; k++)
                {
                    if (matID[k] == sceneManager.m_finishUIParent.GetChild(i).GetChild(0).GetComponent<FinishButton>().fBaseMatID[j])
                    {
                        trigger = true;

                    }
                }
            }

            if (trigger)
            {

            }
            else
            {
                sceneManager.m_finishUIParent.GetChild(i).gameObject.SetActive(false);
            }
        }
    }

    public void ChangeMaterialOnClick()
    {
        currentMaterialNameHelper = currentPartBtn;
        FilterMaterial();
        materialCanvas.SetActive(false);
    }

    public void ChangePartOnClick()
    {
        currentRegionNameHelper = currentMaterialBtn;

        for (int i = 0; i < regionNameHelper.Count; i++)
        {
            if (regionNameHelper[i] == currentMaterialBtn)
            {
                currentDropdownIndex = i;

            }
        }
        currentPartText.text = currentMaterialBtn;
        matID.Clear();

        if (ES2.Exists(currentProductID + currentMaterialBtn))
        {
            matID = ES2.LoadList<string>(currentProductID + currentMaterialBtn);

            //var md = FindObjectOfType<MaterialDropdown>();
            //md.matID.Clear();

            for (int i = 0; i < materialParent.childCount; i++)
            {
                Destroy(materialParent.GetChild(i).gameObject);
            }

            for (int i = 0; i < matID.Count; i++)
            {
                var btn = Instantiate(materialsCanvasBtn);
                btn.transform.SetParent(materialParent, false);

                if (i == 0)
                {
                    btn.transform.GetChild(1).gameObject.SetActive(true);
                }

                for (int j = 0; j < materialID.Count; j++)
                {
                    if (matID[i] == materialID[j])
                    {

                        currentMaterialNameHelper = materialName[j];
                        currentMaterialText.text = materialName[j];

                        btn.transform.GetChild(2).GetComponent<Text>().text = materialName[j];
                    }
                }

            }
            FilterMaterial();
            Changer();
            ChangeColorPickerRegion();
        }

        partsPopupCanvas.SetActive(false);
    }

    void ClearAllTicks()
    {
        for (int i = 0; i < shapesContent.childCount; i++)
        {
            shapesContent.GetChild(i).GetChild(1).gameObject.SetActive(false);
        }
    }

    public void SwitchCastomizePanel()
    {
        // - 340
        // - 186
        if (castomizePanel.anchoredPosition.y == -340)
        {
            castomizePanelUp = true;
            castomizePanel.anchoredPosition = new Vector2(castomizePanel.anchoredPosition.x, -186f);
           // ShapesButtonsSwitchState();
        }
        else
        {
            castomizePanelUp = false;
            castomizePanel.anchoredPosition = new Vector2(castomizePanel.anchoredPosition.x, -340f);
        }
    }

    public void EnableControlsForTextLogos()
    {
        controlsForTextLogos.gameObject.SetActive(true);
    }

    public void DisableControlsForTextLogos()
    {
        controlsForTextLogos.gameObject.SetActive(false);
    }

    public void ScaleTex(int value)
    {
        if (value == 1)
        {

            var changeMat = GameObject.Find(partName).GetComponent<MeshRenderer>().materials[matIndex];
            if (changeMat != null)
            {
                Vector2 scale = changeMat.GetTextureScale("_MainTex");
                Vector2 offset = changeMat.GetTextureOffset("_MainTex");
                changeMat.SetTextureScale("_MainTex", new Vector2(scale.x += 0.5f, scale.y += 0.5f));
                changeMat.SetTextureOffset("_MainTex", new Vector2(offset.x -= 0.25f, offset.y -= 0.25f));
            }
        }

        if (value == 2)
        {
            var changeMat = GameObject.Find(partName).GetComponent<MeshRenderer>().materials[matIndex];
            if (changeMat != null)
            {
                Vector2 scale = changeMat.GetTextureScale("_MainTex");
                Vector2 offset = changeMat.GetTextureOffset("_MainTex");
                changeMat.SetTextureScale("_MainTex", new Vector2(scale.x -= 0.5f, scale.y -= 0.5f));
                changeMat.SetTextureOffset("_MainTex", new Vector2(offset.x += 0.25f, offset.y += 0.25f));
            }
        }
    }

    public void RightLeftUpDownTex(int value)
    {
        if (value == 1)
        {
            var changeMat = GameObject.Find(partName).GetComponent<MeshRenderer>().materials[matIndex];
            if (changeMat != null)
            {
                Vector2 scale = changeMat.GetTextureOffset("_MainTex");
                changeMat.SetTextureOffset("_MainTex", new Vector2(scale.x += 0.1f, scale.y));
            }
        }

        if (value == 2)
        {
            var changeMat = GameObject.Find(partName).GetComponent<MeshRenderer>().materials[matIndex];
            if (changeMat != null)
            {
                Vector2 scale = changeMat.GetTextureOffset("_MainTex");
                changeMat.SetTextureOffset("_MainTex", new Vector2(scale.x -= 0.1f, scale.y));
            }
        }

        if (value == 3)
        {
            var changeMat = GameObject.Find(partName).GetComponent<MeshRenderer>().materials[matIndex];
            if (changeMat != null)
            {
                Vector2 scale = changeMat.GetTextureOffset("_MainTex");
                changeMat.SetTextureOffset("_MainTex", new Vector2(scale.x, scale.y -= 0.1f));
            }
        }

        if (value == 4)
        {
            var changeMat = GameObject.Find(partName).GetComponent<MeshRenderer>().materials[matIndex];
            if (changeMat != null)
            {
                Vector2 scale = changeMat.GetTextureOffset("_MainTex");
                changeMat.SetTextureOffset("_MainTex", new Vector2(scale.x, scale.y += 0.1f));
            }
        }
    }

    void CreateBtnInCollectionRuntime()
    {

       // savedScreenshots.Clear();

        //if (ES2.Exists(path + modelsCount.Count + "SavedScreenshot"))
        //{
        //    savedScreenshots = ES2.LoadList<Sprite>(path + modelsCount.Count + "SavedScreenshot");

        //    for (int i = 0; i < parentForCategoryInCollButtons.childCount; i++)
        //    {
        //        Destroy(parentForCategoryInCollButtons.GetChild(i).gameObject);
        //        Debug.Log("Destroy " + i);
        //    }

        //    for (int i = 0; i < parentForMainScroll.childCount-2; i++)
        //    {
        //        Destroy(parentForMainScroll.GetChild(i).gameObject);
        //        Debug.Log("Destroy " + i);
        //    }

        //    for (int i = 0; i < parentForCollectionBtn.childCount; i++)
        //    {
        //        parentForCollectionBtn.GetChild(i).GetComponent<Image>().sprite = parentForCollectionBtnSprite;
                
        //    }


        //    for (int i = 0; i < modelsCount.Count; i++)
        //    {
        //        int index = i;
        //        savedScreenshots.Clear();
        //        savedScreenshots = ES2.LoadList<Sprite>(path + (i + 1) + "SavedScreenshot");
        //        var collectionBtn = Instantiate(Resources.Load("Prefabs/CategInCollectionBtn") as GameObject);
        //        collectionBtn.transform.SetParent(parentForCategoryInCollButtons, false);
        //        var tempCounter = index + 1;
        //        collectionBtn.GetComponent<Button>().onClick.AddListener(() => LoadingModel(tempCounter));
        //        collectionBtn.transform.GetChild(0).GetComponent<Image>().sprite = savedScreenshots[0];

                
        //        var mainCollectionScroll = Instantiate(Resources.Load("Prefabs/New_Body") as GameObject);
        //        mainCollectionScroll.transform.SetParent(parentForMainScroll, false);
        //        mainCollectionScroll.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(() => LoadingModel(tempCounter));
        //        mainCollectionScroll.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = savedScreenshots[0];
        //        mainCollectionScroll.transform.SetSiblingIndex(0);

        //        parentForCollectionBtn.GetChild(i).GetComponent<Image>().sprite = savedScreenshots[0];

        //    }
        //}


    }

    void CreateBtnInCollectionViaLoading()
    {
        //if (ES2.Exists(path + "modelsCount"))
        //{
        //    modelsCount = ES2.LoadList<int>(path + "modelsCount");

        //    for (int i = 0; i < modelsCount.Count; i++)
        //    {
        //        int tempInt = i;
        //        savedScreenshots.Clear();

        //        savedScreenshots = ES2.LoadList<Sprite>(path + (i + 1) + "SavedScreenshot");

        //        var collectionBtn = Instantiate(Resources.Load("Prefabs/CategInCollectionBtn") as GameObject);
        //        collectionBtn.transform.SetParent(parentForCategoryInCollButtons, false);
        //        collectionBtn.GetComponent<Button>().onClick.AddListener(() => LoadingModel(tempInt));
        //        collectionBtn.transform.GetChild(0).GetComponent<Image>().sprite = savedScreenshots[0];

        //        var mainCollectionScroll = Instantiate(Resources.Load("Prefabs/New_Body") as GameObject);
        //        mainCollectionScroll.transform.SetParent(parentForMainScroll, false);
        //        mainCollectionScroll.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(() => LoadingModel(tempInt));
        //        mainCollectionScroll.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = savedScreenshots[0];
        //        mainCollectionScroll.transform.SetSiblingIndex(0);

        //        parentForCollectionBtn.GetChild(i).GetComponent<Image>().sprite = savedScreenshots[0];

        //    }
        //}
    }


    //IEnumerator TakeScreenshot()
    //{
    //   // DisableOutline();

    //    yield return new WaitForEndOfFrame();

    //    Debug.Log("Sheigh " + Screen.height);
    //    Debug.Log("Swith " + Screen.width);

    //    var width = Screen.width / 1.5f;
    //    var height = Screen.width / 1.5f;

    //    var startX = height / 3f;
    //    var startY = height * .8f;
    //    var tex2D = new Texture2D((int)width, (int)height, TextureFormat.RGB24, false);
    //    Rect rex = new Rect(startX, startY, width, height);
    //    tex2D.ReadPixels(rex, 0, 0);
    //    tex2D.Apply();
    //    var tempSP = Sprite.Create(tex2D, new Rect(0.0f, 0.0f, tex2D.width, tex2D.height), new Vector2(0.5f, 0.5f), 100.0f);
    //    savedScreenshots.Add(tempSP);
    //    ES2.Save(savedScreenshots, path + modelsCount.Count + "SavedScreenshot");

    //    //EnableOutline();
    //}


    #region Save Model Logic

    //public void SaveModels()
    //{
    //    modelsCount.Clear();
    //    modelName.Clear();
    //    partsName.Clear();
    //    partsMaterialsName.Clear();
    //    SavedMaterials.Clear();
    //    partNameWereCustomMaterialSave.Clear();
    //    customModelsName.Clear();
    //    positionList.Clear();
    //    rotationList.Clear();
    //    savedScreenshots.Clear();
    //    colorsList.Clear();
    //    partsWithChangedColor.Clear();


    //    if (ES2.Exists(path + "modelsCount"))
    //    {
    //        modelsCount = ES2.LoadList<int>(path + "modelsCount");
    //    }

    //    GameObject go = GameObject.FindWithTag("CurrentObjParent");
    //    modelsCount.Add(modelsCount.Count);
    //    ES2.Save(modelsCount, path + "modelsCount");
    //    string goNameString = go.name;

    //    if (goNameString.Contains("("))
    //    {
    //        int dotIndex = goNameString.IndexOf("(");

    //        if (dotIndex >= 0)
    //        {
    //            goNameString = goNameString.Substring(0, dotIndex);
    //        }

    //        modelName.Add(goNameString);
    //    }
    //    else
    //    {
    //        modelName.Add(go.name);
    //    }

    //    ES2.Save(modelName, path + modelsCount.Count + "modelName");

    //    ES2.Save(customMaterialPartName, path + modelsCount.Count + "customMaterialPartName");

    //    ES2.Save(indexMatList, path + modelsCount.Count + "indexMatList");

    //    ES2.Save(spritesForTextLogoBtn, path + modelsCount.Count + "spritesForTextLogoBtn");

    //    //Проверка на цвет (колор пикер)
    //    for (int i = 0; i < go.transform.childCount; i++)
    //    {
            
    //            if (go.transform.GetChild(i).GetComponent<MeshRenderer>().material.color != Color.white)
    //            {
    //                partsWithChangedColor.Add(go.transform.GetChild(i).name);
    //                colorsList.Add(go.transform.GetChild(i).GetComponent<MeshRenderer>().material.color);

    //            }
            
          

    //    }
    //    ES2.Save(partsWithChangedColor, path + modelsCount.Count + "partsWithChangedColor");
    //    ES2.Save(colorsList, path + modelsCount.Count + "colors");
    //    // ПРОВЕРКА НА новые детали
    //    var model = (Resources.Load("DefaultModelsPref/" + modelName[0]) as GameObject);
    //    if (model.transform.childCount != go.transform.childCount)
    //    {

    //        for (int i = 0; i < go.transform.childCount; i++)
    //        {
    //            if (go.transform.GetChild(i).GetComponent<MyType>().parenting == MyType.Parenting.Relative)
    //            {
    //                partsName.Add(go.transform.GetChild(i).name);
    //                ES2.Save(partsName, path + modelsCount.Count + "modelPartsName");
    //            }
    //            else if (go.transform.GetChild(i).GetComponent<MyType>().parenting == MyType.Parenting.Added)
    //            {
    //                string addedGoName = go.transform.GetChild(i).name;

    //                if (addedGoName.Contains("("))
    //                {
    //                    int dotIndex = addedGoName.IndexOf("(");

    //                    if (dotIndex >= 0)
    //                    {
    //                        addedGoName = addedGoName.Substring(0, dotIndex);

    //                    }
    //                }

    //                customModelsName.Add(addedGoName);
    //                ES2.Save(customModelsName, path + modelsCount.Count + "customModelsName");

    //                var pos = go.transform.GetChild(i).transform.position;
    //                positionList.Add(pos);
    //                ES2.Save(positionList, path + modelsCount.Count + "customModelsPosition");

    //                var rot = go.transform.GetChild(i).transform.eulerAngles;
    //                rotationList.Add(rot);
    //                ES2.Save(rotationList, path + modelsCount.Count + "customModelsRotation");
    //            }
    //        }



    //        for (int i = 0; i < go.transform.childCount; i++)
    //        {
    //            if (go.transform.GetChild(i).GetComponent<MyType>().parenting == MyType.Parenting.Relative)
    //            {
    //                string testString = go.transform.GetChild(i).GetComponent<MeshRenderer>().materials[0].name;

    //                if (testString.Contains(" "))
    //                {
    //                    int dotIndex = testString.IndexOf(" ");
    //                    if (dotIndex >= 0)
    //                    {
    //                        testString = testString.Substring(0, dotIndex);
    //                    }


    //                    partsMaterialsName.Add(testString);
    //                }
    //                else
    //                {
    //                    partsMaterialsName.Add(go.transform.GetChild(i).GetComponent<MeshRenderer>().materials[0].name);
    //                }


    //                ES2.Save(partsMaterialsName, path + modelsCount.Count + "modelPartsMaterialsName");
    //                ES2.Save(SavedMaterials, path + modelsCount.Count + "modelSavedMaterials");

    //                if (go.transform.GetChild(i).GetComponent<MeshRenderer>().materials.Length > 1)
    //                {

    //                    if (go.transform.childCount >= i)
    //                    {
    //                        partNameWereCustomMaterialSave.Add(go.transform.GetChild(i).name);

    //                        ES2.Save(partNameWereCustomMaterialSave, path + modelsCount.Count + "PartNameWereCustomMaterialSave");
    //                        SavedMaterials.Clear();
    //                        for (int j = 1; j < go.transform.GetChild(i).GetComponent<MeshRenderer>().materials.Length; j++)
    //                        {
    //                            var tempInt = j - 1;
    //                            SavedMaterials.Add(go.transform.GetChild(i).GetComponent<MeshRenderer>().materials[j]);
    //                            ES2.Save(SavedMaterials, path + modelsCount.Count + go.transform.GetChild(i).name + "modelSavedMaterials");
    //                        }
    //                    }
    //                }
    //            }

    //            else if (go.transform.GetChild(i).GetComponent<MyType>().parenting == MyType.Parenting.Added)
    //            {
    //                string testString = go.transform.GetChild(i).GetComponent<MeshRenderer>().materials[0].name;

    //                if (testString.Contains(" "))
    //                {
    //                    int dotIndex = testString.IndexOf(" ");
    //                    if (dotIndex >= 0)
    //                    {
    //                        testString = testString.Substring(0, dotIndex);
    //                    }


    //                    addedPartMaterial.Add(testString);
    //                }
    //                //else
    //                //{
    //                //    addedPartMaterial.Add(go.transform.GetChild(i).GetComponent<MeshRenderer>().materials[0].name);
    //                //}

    //                ES2.Save(addedPartMaterial, path + modelsCount.Count + "addedPartMaterial");
    //                ES2.Save(SavedMaterials, path + modelsCount.Count + "modelSavedMaterials");

    //                if (go.transform.GetChild(i).GetComponent<MeshRenderer>().materials.Length > 1)
    //                {

    //                    if (go.transform.childCount >= i)
    //                    {
    //                        partNameWereCustomMaterialSave.Add(go.transform.GetChild(i).name);

    //                        ES2.Save(partNameWereCustomMaterialSave, path + modelsCount.Count + "PartNameWereCustomMaterialSave");
    //                        SavedMaterials.Clear();
    //                        for (int j = 1; j < go.transform.GetChild(i).GetComponent<MeshRenderer>().materials.Length; j++)
    //                        {
    //                            var tempInt = j - 1;
    //                            SavedMaterials.Add(go.transform.GetChild(i).GetComponent<MeshRenderer>().materials[j]);
    //                            ES2.Save(SavedMaterials, path + modelsCount.Count + go.transform.GetChild(i).name + "modelSavedMaterials");
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //    }

    //    else
    //    {
    //        Debug.Log("new details -----------");
    //        for (int i = 0; i < go.transform.childCount; i++)
    //        {

    //            // проверить соответвует ли части модели загр, если да - все ок, если нет, на сколько больше прогоняем по циклу и сохраняем трансформ и ротэйшн

    //            partsName.Add(go.transform.GetChild(i).name);
    //            ES2.Save(partsName, path + modelsCount.Count + "modelPartsName");
    //            //если у материала есть символ ) или ( - удаляем все после пробла и сохр

    //            string testString = go.transform.GetChild(i).GetComponent<MeshRenderer>().materials[0].name;

    //            if (testString.Contains(" "))
    //            {
    //                int dotIndex = testString.IndexOf(" ");
    //                if (dotIndex >= 0)
    //                {
    //                    testString = testString.Substring(0, dotIndex);
    //                }


    //                partsMaterialsName.Add(testString);
    //            }
    //            else
    //            {
    //                partsMaterialsName.Add(go.transform.GetChild(i).GetComponent<MeshRenderer>().materials[0].name);
    //            }

    //            // сохр        
    //            ES2.Save(partsMaterialsName, path + modelsCount.Count + "modelPartsMaterialsName");
    //            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    //            if (go.transform.GetChild(i).GetComponent<MeshRenderer>().materials.Length > 1)
    //            {

    //                partNameWereCustomMaterialSave.Add(go.transform.GetChild(i).name);

    //                ES2.Save(partNameWereCustomMaterialSave, path + modelsCount.Count + "PartNameWereCustomMaterialSave");
    //                SavedMaterials.Clear();
    //                for (int j = 1; j < go.transform.GetChild(i).GetComponent<MeshRenderer>().materials.Length; j++)
    //                {
    //                    SavedMaterials.Add(go.transform.GetChild(i).GetComponent<MeshRenderer>().materials[j]);
    //                    ES2.Save(SavedMaterials, path + modelsCount.Count + go.transform.GetChild(i).name + "modelSavedMaterials");
    //                }


    //            }

    //        }
    //    }

    //    StartCoroutine(TakeScreenshot());

    //}
    #endregion

    //#region Load Model

    //void LoadingModel(int index)
    //{
    //    CloseAllCanvases();
    //    OpenViewCanvas();
    //  //  DeleteButtonsInSavedContent();
    //    addedPartMaterial.Clear();
    //    modelsCount.Clear();
    //    modelName.Clear();
    //    partsName.Clear();
    //    partsMaterialsName.Clear();
    //    SavedMaterials.Clear();
    //    partNameWereCustomMaterialSave.Clear();
    //    customModelsName.Clear();
    //    positionList.Clear();
    //    rotationList.Clear();
    //    savedScreenshots.Clear();
    //    customMaterialPartName.Clear();
    //    colorsList.Clear();
    //    partsWithChangedColor.Clear();
    //    indexMatList.Clear();
    //    spritesForTextLogoBtn.Clear();

    //    if (ES2.Exists(path + "modelsCount"))
    //    {
    //        modelsCount = ES2.LoadList<int>(path + "modelsCount");
    //    }

    //    if (ES2.Exists(path + index + "modelName"))
    //    {
    //        modelName = ES2.LoadList<string>(path + index + "modelName");
    //    }

    //    if (ES2.Exists(path + index + "modelPartsName"))
    //    {
    //        partsName = ES2.LoadList<string>(path + index + "modelPartsName");
    //    }

    //    if (ES2.Exists(path + index + "customMaterialPartName"))
    //    {
    //        customMaterialPartName = ES2.LoadList<string>(path + index + "customMaterialPartName");
    //    }

    //    if (ES2.Exists(path + index + "indexMatList"))
    //    {
    //        indexMatList = ES2.LoadList<int>(path + index + "indexMatList");
    //    }
    //    // ES2.Save(addedPartMaterial, path + modelsCount.Count + "addedPartMaterial");
    //    if (ES2.Exists(path + index + "addedPartMaterial"))
    //    {
    //        addedPartMaterial = ES2.LoadList<string>(path + index + "addedPartMaterial");
    //    }

    //    if (ES2.Exists(path + index + "modelPartsMaterialsName"))
    //    {
    //        partsMaterialsName = ES2.LoadList<string>(path + index + "modelPartsMaterialsName");
    //    }

    //    GameObject tempGo = Instantiate(Resources.Load("DefaultModelsPref/" + modelName[0]) as GameObject);

    //    if (tempGo.transform.childCount == partsName.Count)
    //    {
    //        Debug.Log("No new Details");
    //    }
    //    else
    //    {
    //        Debug.Log("added new Details");
    //        for (int i = 0; i < tempGo.transform.childCount; i++)
    //        {
    //            bool YesPart = false;

    //            for (int j = 0; j < partsName.Count; j++)
    //            {

    //                if (tempGo.transform.GetChild(i).name == partsName[j])
    //                {
    //                    Debug.Log("Yes " + tempGo.transform.GetChild(i).name);
    //                    YesPart = true;
    //                    j++;
    //                }

    //                else
    //                {
    //                    Debug.Log("NO " + tempGo.transform.GetChild(i).name);
    //                    YesPart = false;

    //                }

    //                if (j == partsName.Count - 1)
    //                {
    //                    if (!YesPart)
    //                    {
    //                        Destroy(tempGo.transform.GetChild(i).gameObject);
    //                    }
    //                }

    //            }
    //        }
    //    }



    //    //


    //    if (ES2.Exists(path + index + "customModelsName"))
    //    {
    //        customModelsName = ES2.LoadList<string>(path + index + "customModelsName");
    //        positionList = ES2.LoadList<Vector3>(path + index + "customModelsPosition");
    //        rotationList = ES2.LoadList<Vector3>(path + index + "customModelsRotation");
    //        var GOparent = GameObject.FindWithTag("CurrentObjParent");

    //        for (int i = 0; i < customModelsName.Count; i++)
    //        {
    //            var customGO = Instantiate(Resources.Load("ModelParts/" + customModelsName[i]) as GameObject);
    //            customGO.transform.position = positionList[i];
    //            customGO.transform.eulerAngles = rotationList[i];
    //            customGO.transform.SetParent(GOparent.transform, false);

    //            if (addedPartMaterial[i] != "DefaultMaterial")
    //            {
    //                Debug.Log("notdef");
    //                customGO.GetComponent<MeshRenderer>().material = Resources.Load("LoadMaterials/" + addedPartMaterial[i]) as Material;
    //            }
    //        }
    //    }


    //    if (ES2.Exists(path + index + "spritesForTextLogoBtn"))
    //    {
    //        spritesForTextLogoBtn = ES2.LoadList<Sprite>(path + index + "spritesForTextLogoBtn");
    //    }
    //        if (ES2.Exists(path + index + "PartNameWereCustomMaterialSave"))
    //    {
    //      //  DeleteButtonsInSavedContent();
    //        partNameWereCustomMaterialSave = ES2.LoadList<string>(path + index + "PartNameWereCustomMaterialSave");

    //        for (int i = 0; i < partNameWereCustomMaterialSave.Count; i++)
    //        {
    //            int counterP = i;
    //            int stepCounter = 0;
    //            if (ES2.Exists(path + index + partNameWereCustomMaterialSave[i] + "modelSavedMaterials"))
    //            {
    //                SavedMaterials.Clear();
    //                SavedMaterials = ES2.LoadList<Material>(path + index + partNameWereCustomMaterialSave[i] + "modelSavedMaterials");
    //                var tempRenderer = GameObject.Find(partNameWereCustomMaterialSave[i]).GetComponent<MeshRenderer>();
    //                arrayMaterials = tempRenderer.materials;
    //                tempMaterials = arrayMaterials;
    //                arrayMaterials = new Material[SavedMaterials.Count + 1];

                   
    //                for (int j = 0; j < SavedMaterials.Count + 1; j++)
    //                {
    //                    if (j == 0)
    //                    {
    //                        arrayMaterials[j] = tempMaterials[j];
    //                    }

    //                    if (j >= 1)
    //                    {
                            
    //                        SavedMaterials[j - 1].name = "Mat";
    //                        SavedMaterials[j - 1].SetFloat("_Mode", 2.0f);
    //                        SavedMaterials[j - 1].SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
    //                        SavedMaterials[j - 1].SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
    //                        SavedMaterials[j - 1].SetInt("_ZWrite", 0);
    //                        SavedMaterials[j - 1].DisableKeyword("_ALPHATEST_ON");
    //                        SavedMaterials[j - 1].EnableKeyword("_ALPHABLEND_ON");
    //                        SavedMaterials[j - 1].DisableKeyword("_ALPHAPREMULTIPLY_ON");
    //                        SavedMaterials[j - 1].renderQueue = 3000;

    //                        arrayMaterials[j] = SavedMaterials[j - 1];
    //                        var SavedMaterialsButton = Instantiate(Resources.Load("Prefabs/SavedBtn") as GameObject);
    //                        SavedMaterialsButton.transform.SetParent(savedContent, false);

    //                        int tempCounter = indexMatList[j - 1];
    //                        string name = partNameWereCustomMaterialSave[i];
    //                        SavedMaterialsButton.GetComponent<Button>().onClick.AddListener(() => PickTextAndLogoMaterial(tempCounter, name));
                           
    //                        SavedMaterialsButton.GetComponent<Image>().sprite = spritesForTextLogoBtn[stepCounter];

    //                        if (customTextList[j - 1] != hackText)
    //                        {
    //                            SavedMaterialsButton.GetComponentInChildren<Text>().text = customTextList[stepCounter];
    //                        }

    //                        stepCounter++;
    //                    }
    //                }

    //                tempRenderer.materials = arrayMaterials;
    //            }
    //        }
    //    }

    //    for (int i = 0; i < partsName.Count; i++)
    //    {
    //        if (partsMaterialsName[i] != "DefaultMaterial")
    //        {
    //            GameObject.Find(partsName[i]).GetComponent<MeshRenderer>().material = Resources.Load("LoadMaterials/" + partsMaterialsName[i]) as Material;
    //        }
    //    }

       

    //    if (ES2.Exists(path + index + "SavedScreenshot"))
    //    {
    //        savedScreenshots = ES2.LoadList<Sprite>(path + index + "SavedScreenshot");
    //    }
    //    camController.enabled = true;
    //    var parent = GameObject.FindWithTag("CurrentObjParent").transform;
    //    camController.target = parent;
    //    List<GameObject> listCurrentGO = new List<GameObject>(GameObject.FindGameObjectsWithTag("ChildObj1"));

    //    for (int i = 0; i < listCurrentGO.Count; i++)
    //    {
    //        if (listCurrentGO[i].transform.parent != null)
    //        {
    //            GameObject parent1;
    //            parent1 = listCurrentGO[i].transform.parent.gameObject;
    //            listCurrentGO[i].transform.SetParent(parent, false);

    //            if (parent1.tag != "CurrentObjParent")
    //            {
    //                Destroy(parent1);
    //            }
    //        }
    //    }

    //    //применяем кастомные цвета к материалу
    //    if (ES2.Exists(path + index + "partsWithChangedColor"))
    //    {
    //        partsWithChangedColor = ES2.LoadList<string>(path + index + "partsWithChangedColor");
    //        colorsList = ES2.LoadList<Color>(path + index + "colors");

    //        for (int i = 0; i < partsWithChangedColor.Count; i++)
    //        {
    //            GameObject.Find(partsWithChangedColor[i]).GetComponent<MeshRenderer>().material.color = colorsList[i];
    //            GameObject.Find(partsWithChangedColor[i]).GetComponent<ColorPickerTester>().savedColor = colorsList[i];
    //        }
    //    }



    //}

    //#endregion

    #region Materials

    public void DeleteActiveMaterial()
    {
        if (controlsForTextLogos.gameObject.activeSelf)
        {
            //var tempRenderer = GameObject.Find(partName).GetComponent<MeshRenderer>();
            //List<Material> list = new List<Material>(tempRenderer.materials);
            //list.Remove(list[matIndex]);
            //tempRenderer.materials = list.ToArray();
            //controlsForTextLogos.gameObject.SetActive(false);
            //DisableTransparencyForMaterial();
            ////DisableOutline();
            //Destroy(ButtonForDelete);
            //List<string> listOfTempName = new List<string>();

            //for (int i = 0; i < customMaterialPartName.Count; i++)
            //{
            //    if (customMaterialPartName[i] == partName)
            //    {
            //        listOfTempName.Add(customMaterialPartName[i]);
            //    }
            //}

            //for (int i = 0; i < customMaterialPartName.Count; i++)
            //{
            //    if (customMaterialPartName[i] == partName)
            //    {
            //        if (indexMatList[i] > matIndex)
            //        {
            //           indexMatList[i] = indexMatList[i] - 1;
            //           int counter = indexMatList[i];
            //           savedContent.GetChild(i).GetComponent<Button>().onClick.RemoveAllListeners();
            //           savedContent.GetChild(i).GetComponent<Button>().onClick.AddListener(() => PickTextAndLogoMaterial(counter, partName));
            //        }
            //    }
            //}

            //customMaterialPartName.RemoveAt(btnIndex);
            //indexMatList.RemoveAt(btnIndex);
            //customTextList.RemoveAt(btnIndex);
            //spritesForTextLogoBtn.RemoveAt(btnIndex);
        }

    }


    public void ChangeIndexAfterAddText()
    {
        for (int i = 0; i < savedContent.childCount-1; i++)
        {
           // if (customMaterialPartName[i] == partName)
           // {
             //   indexMatList[i] = indexMatList[i] + 1;
               // int counter = indexMatList[i];
                Debug.Log("counter " + counter);
                savedContent.GetChild(i).GetComponent<Button>().onClick.RemoveAllListeners();
                savedContent.GetChild(i).GetComponent<Button>().onClick.AddListener(() => PickTextAndLogoMaterial(counter, partName));
           // }
        }
    }
   

    public void AddTextAndLogoButtonRuntime(int index)
    {
        Debug.Log(index);
        var SavedMaterialsButton = Instantiate(Resources.Load("Prefabs/SavedBtn") as GameObject);
        SavedMaterialsButton.transform.SetParent(savedContent, false);
        string partName = GameObject.Find(this.partName).name;
        var contentIndex = savedContent.childCount;
        SavedMaterialsButton.GetComponent<Button>().onClick.AddListener(() => PickTextAndLogoMaterial(index, partName));

       // SavedMaterialsButton.GetComponent<Image>().sprite = spritesForTextLogoBtn[savedContent.childCount - 1];
        //if (customTextList[customTextList.Count - 1] != hackText)
        //{
        //    SavedMaterialsButton.GetComponentInChildren<Text>().text = customTextList[customTextList.Count - 1];
        //}
       

    }

    void PickTextAndLogoMaterial(int index, string goName)
    {
        ButtonForDelete = EventSystem.current.currentSelectedGameObject.gameObject;
        btnIndex = EventSystem.current.currentSelectedGameObject.transform.GetSiblingIndex();
      //  partName = customMaterialPartName[btnIndex];
        matIndex = index;
        Debug.Log(index);
       // EnableOutline();
        EnableControlsForTextLogos();
        DisableTransparencyForMaterial();
        EnableTransparencyForMaterial();
        var ui = FindObjectOfType<UIDropdown>();
       // ui.SelectOption(customMaterialPartName[btnIndex]);

    }


    public void EnableTransparencyForMaterial()
    {
        var myMaterials = GameObject.Find(partName).GetComponent<MeshRenderer>().materials;
        for (int i = 0; i < myMaterials.Length; i++)
        {
            if (i != matIndex)
            {
                myMaterials[i].color = new Color(myMaterials[i].color.r, myMaterials[i].color.g, myMaterials[i].color.b, 0.35f);
            }
        }
    }

    public void DisableTransparencyForMaterial()
    {
        var myMaterials = GameObject.Find(partName).GetComponent<MeshRenderer>().materials;
        for (int i = 0; i < myMaterials.Length; i++)
        {
            myMaterials[i].color = new Color(myMaterials[i].color.r, myMaterials[i].color.g, myMaterials[i].color.b, 1);
        }
    }

    public void UpDownCostomizePanel(RectTransform panel)
    {
        if (panel.anchoredPosition.y == -74f)
        {
            panel.anchoredPosition = new Vector2(panel.anchoredPosition.x, 143.6667f);
            costomizeSlideBtn.sprite = costomizeSlideDown;
        }
        else
        {
            panel.anchoredPosition = new Vector2(panel.anchoredPosition.x, -74f);
            costomizeSlideBtn.sprite = costomizeSlideUp;
        }
    }

    public void SwitchColorPanel(RectTransform panel)
    {
        if (panel.gameObject.activeSelf)
        {
            panel.gameObject.SetActive(false);
        }
        else
        {
            panel.gameObject.SetActive(true);
            
            EnablePicker();
        }
       
    }

}
#endregion
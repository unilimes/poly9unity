﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MaterialDropdown : MonoBehaviour {
    
    public Dropdown m_dropdown;

    public List<string> matID;

    public CheckItemsCount cik;
    public IEnumerator ChangeMaterial()
    {
        m_dropdown = gameObject.GetComponent<Dropdown>();
        HELPER.instance.currentMaterialNameHelper = m_dropdown.options[0].text;
        FilterMaterial();

        cik.CheckItems();
        m_dropdown.onValueChanged.AddListener(delegate
        {
            
            HELPER.instance.currentMaterialNameHelper = m_dropdown.options[m_dropdown.value].text;
            FilterMaterial();
        });

        yield return 0;
    }


    void FilterMaterial()
    {
        var sm = FindObjectOfType<SceneManager>();

        Debug.Log("filter");
        for (int i = 0; i < sm.m_finishUIParent.childCount; i++)
        {
            sm.m_finishUIParent.GetChild(i).gameObject.SetActive(true);
        }



        for (int i = 0; i < sm.m_finishUIParent.childCount; i++)
        {
            var trigger = false;
            for (int j = 0; j < sm.m_finishUIParent.GetChild(i).GetChild(0).GetComponent<FinishButton>().fBaseMatID.Count; j++)
            {
                for (int k = 0; k < matID.Count; k++)
                {
                    if (matID[k] == sm.m_finishUIParent.GetChild(i).GetChild(0).GetComponent<FinishButton>().fBaseMatID[j])
                    {
                        trigger = true;
                       
                    }

                    
                }
            }

            if (trigger)
            {
                
            }
            else
            {
              
                sm.m_finishUIParent.GetChild(i).gameObject.SetActive(false);
            }
        }
    }
    public void AddLoadName()
    {
        m_dropdown = gameObject.GetComponent<Dropdown>();
        m_dropdown.ClearOptions();

        for (int i = 0; i < matID.Count; i++)
        {
            for (int j = 0; j < HELPER.instance.materialID.Count; j++)
            {
                if (matID[i] == HELPER.instance.materialID[j])
                {
                   AddItemToDropdown(HELPER.instance.materialName[j]);
                }
            }

          
        }
        m_dropdown.RefreshShownValue();
        StartCoroutine(ChangeMaterial());
    }
    public void SelectOption(string text) {

        int option_id = 0;
        foreach (Dropdown.OptionData data in m_dropdown.options) {
            if (data.text == text) {
                m_dropdown.value = option_id;
                m_dropdown.RefreshShownValue();
            }
            option_id++;
        }
    }

    public void AddItemToDropdown(string item, bool select_on_add=false) {
        m_dropdown.options.Add(new Dropdown.OptionData(item));

        if (select_on_add) {
            SelectOption(item);
        }
    }

    public void AddItemToDropdownAndSelect(string item) {
        AddItemToDropdown(item, true);
    }

    public void ChangeItem(string value)
    {
        m_dropdown.options[m_dropdown.value].text = value;
        m_dropdown.RefreshShownValue();
    }
}

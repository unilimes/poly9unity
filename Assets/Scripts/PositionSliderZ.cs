﻿using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;

public class PositionSliderZ : MonoBehaviour,  IDragHandler, IInitializePotentialDragHandler {

    HELPER helper;
    public float multiplier;
    public Transform currentOBJ;
    Slider slider;
    float  currentPosX;
    float  currentPosY;
    float  currentPosZ;
    MainCameraController MCC;

    void Start ()
    {
        helper = FindObjectOfType<HELPER>();
        slider = GetComponent<Slider>();
        MCC = FindObjectOfType<MainCameraController>();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        eventData.selectedObject = gameObject;
    }

    public void OnInitializePotentialDrag(PointerEventData eventData)
    {
        
        currentOBJ = GameObject.Find(helper.partName).transform;
        MCC.blockControl = true;
        currentPosX = currentOBJ.localPosition.x;
        currentPosY = currentOBJ.localPosition.y;
        currentPosZ = currentOBJ.localPosition.z;
    }

    public void OnDrag(PointerEventData eventData)
    {
        //y pos
        if (currentPosX != 0 || currentPosY != 0 || currentPosZ != 0)
        {
            currentOBJ.localPosition = new Vector3(currentPosX, currentPosY, slider.value * multiplier + currentPosZ);
        }
        else
        {
            currentOBJ.localPosition = new Vector3(0, 0, slider.value * multiplier);
        }
       
        //Debug.Log(currentOBJ.localRotation.y);
    }

    public void End()
    {
        MCC.blockControl = false;
        slider.value = 0;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CategoryCanvasAnimation : MonoBehaviour {

    public GameObject scroll;
    public Image imSp;
    Sprite originSP;
    public Sprite downSP;
    //public GameObject text;

    private void Start()
    {
        originSP = imSp.sprite;
    }

    public void EnableDisableScroll()
    {
        if (imSp.sprite == originSP)
        {
            scroll.SetActive(false);
           // text.SetActive(false);
            imSp.sprite = downSP;
        }
        else
        {
            scroll.SetActive(true);
            //text.SetActive(true);
            imSp.sprite = originSP;
        }
    }
}

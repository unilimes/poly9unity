﻿using UnityEngine;

public class EnableSelectIcon : MonoBehaviour {

    public void EnableIcon()
    {
        HELPER.instance.DisableAllSelectIcon();
        transform.GetChild(1).gameObject.SetActive(true);
    }
}

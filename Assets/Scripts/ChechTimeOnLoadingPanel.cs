﻿using UnityEngine;
using UnityEngine.UI;

public class ChechTimeOnLoadingPanel : MonoBehaviour {

    public float targetTime = 30.0f;
    public Transform btn;
    [SerializeField]
    bool needTime;
    private void OnEnable()
    {
        targetTime = 30;
        btn.gameObject.SetActive(false);
        if (transform.GetChild(0).GetComponent<Text>().text != "Loading...")
        {
            needTime = true;
        }
        else
        {
            needTime = false;
        }


        //  Debug.Log("enable Loading");
    }

    private void Update()
    {

        if (!needTime)
        {
            targetTime -= Time.deltaTime;
            if (targetTime <= 0.0f)
            {
                btn.gameObject.SetActive(true);
            }
        }
    }
}

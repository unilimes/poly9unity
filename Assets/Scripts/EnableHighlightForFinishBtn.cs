﻿using UnityEngine;

public class EnableHighlightForFinishBtn : MonoBehaviour {

    public void EnableHighlight(Transform circle)
    {
        circle.gameObject.SetActive(true);
    }
}

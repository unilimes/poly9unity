﻿using UnityEngine;
using UnityEngine.UI;

public class CheckText1 : MonoBehaviour {

    public Text text;
    public GameObject btn;

    public void Check()
    {
        if (text.text.Length > 50)
        {
            btn.SetActive(true);
        }
        else
        {
            btn.SetActive(false);
        }
    }
}

﻿using UnityEngine;

public class SetupHeightOnScroll : MonoBehaviour {

	
	
	//void Update ()
 //   {
 //       if (Input.GetKeyDown(KeyCode.Space))
 //       {
 //           Setup();
 //       }
	//}

    public void Setup()
    {
        // 16 + 16 = 32 padding
        // 242 - height
        var count = transform.childCount;

        float row = (float)count / 2;
       

        if ((transform.childCount % 2) == 0)
        {
            Debug.Log("1");
            var height = row * 242 + row * 16f;
            Debug.Log("Height " + height);
            Debug.Log("Row " + row);
            transform.GetComponent<RectTransform>().sizeDelta = new Vector2(transform.GetComponent<RectTransform>().sizeDelta.x, height);
        }
        else
        {
            Debug.Log("2");
            var row1 = count / 2;
            row1++;
            var height1 = row1 * 242 + row * 16f;
            Debug.Log("Height " + height1);
            Debug.Log("Row " + row1);
            transform.GetComponent<RectTransform>().sizeDelta = new Vector2(transform.GetComponent<RectTransform>().sizeDelta.x, height1);
        }
    }
}

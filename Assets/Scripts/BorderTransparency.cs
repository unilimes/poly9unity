﻿using UnityEngine;

public class BorderTransparency : MonoBehaviour {

    public Texture2D texture2d;
    int borderWidth = 1;
 
 void Start()
    {
        var i = texture2d;
        var borderColor = new Color(0, 0, 0, 0);
        
        for (var x = 0; x < i.width; x++) {

            for (var y = 0; y < i.height; y++) {
                if (x < borderWidth || x > i.width - 1 - borderWidth) i.SetPixel(x, y, borderColor);

                else if (y < borderWidth || y > i.height - 1 - borderWidth) i.SetPixel(x, y, borderColor);
            }
        }

        i.Apply();


    }


}

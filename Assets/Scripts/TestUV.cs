﻿using UnityEngine;

public class TestUV : MonoBehaviour
{

    public Texture2D sourceTex;
    public Material[] allMaterials;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                Renderer rend = hit.transform.GetComponent<Renderer>();
                MeshCollider meshCollider = hit.collider as MeshCollider;

                if (rend == null || rend.sharedMaterial == null || rend.sharedMaterial.mainTexture == null || meshCollider == null)
                    return;
                Vector2 pixelUV = hit.textureCoord;
                Debug.Log("UV COORDS " + pixelUV);

                allMaterials = GetComponent<MeshRenderer>().materials;
                Debug.Log(allMaterials.Length);
                for (int i = 0; i < allMaterials.Length; i++)
                {

                    sourceTex =  allMaterials[i].GetTexture("_MainTex") as Texture2D;
                    Color p = sourceTex.GetPixelBilinear(pixelUV.x, pixelUV.y);
                    Debug.Log("Color " + p); 
                }

            }
        }
    }
}
﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using System.IO;

public class GetTexture : MonoBehaviour {

    public string url = "";
    public Texture2D tex;

    public List<Material> myMat;



    private void Start()
    {
        //var info = new DirectoryInfo(Application.persistentDataPath + "/Textures/");
        //var fileInfo = info.GetFiles();
        //foreach (var file in fileInfo)
        //{
        //    print(file);
        //} 
        
    }

    public void SaveTex()
    {
        StartCoroutine(LoadTexture());
    }

    IEnumerator LoadTexture()
    {
        
        tex = new Texture2D(1024, 1024, TextureFormat.ARGB32, false);
        using (WWW www = new WWW(url))
        {
            yield return www;
            www.LoadImageIntoTexture(tex);
            GetComponent<Renderer>().material.mainTexture = tex;
        }
        LoadTextureToFile();
        
        
    }

    void LoadTextureToFile()
    {
        

        tex.name = "lol1";
        var bytes = tex.EncodeToJPG();
        string path = Application.persistentDataPath + "/Textures/";
        if (!Directory.Exists(path))
            Directory.CreateDirectory(path);

        File.WriteAllBytes(path + tex.name + ".jpg", bytes);
        
    }

    public void LoadindTexturex(string texName)
    {
        
       // string newPath = Application.persistentDataPath + "/NewTextures/";
        string path = Application.persistentDataPath + "/Textures/";
        
        if (File.Exists(path + texName + ".jpg"))
        {
            var fileBytes = File.ReadAllBytes(path + texName + ".jpg");

            var texture = new Texture2D(1024, 1024, TextureFormat.ARGB32, false);
            texture.LoadImage(fileBytes);
            GetComponent<Renderer>().material.mainTexture = texture;
        }

       
        
    }

   
  

}

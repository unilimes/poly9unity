﻿using System.Collections.Generic;
using UnityEngine;
using VoxelBusters.RuntimeSerialization;
[RuntimeSerializable(typeof(MonoBehaviour), false)]

public class SaveMyCustomClass : MonoBehaviour {

    [RuntimeSerializeField]
    public List<FinishesForSave> finishesForSave;

}

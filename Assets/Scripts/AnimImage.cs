﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class AnimImage : MonoBehaviour {

    public Image _animImage;
    public bool needWait;

    public void StartAnim()
    {
        _animImage.fillAmount = 0;
        StartCoroutine(StartAnimImage(0, 1, 5f));
    }

    public IEnumerator StartAnimImage(float v_start, float v_end, float duration)
    {
        //Debug.Log("startcor");
        if (needWait)
        {
            Debug.Log("NEEEEEEEEEEEEEEEEEEEED WAAAAAAAAAAAAAAAAIT");
            yield return new WaitForSeconds(3f);
            HELPER.instance.isLoadiiiing = true;
            HELPER.instance.OpenMainMenuCanvas();
        }
        HELPER.instance.isLoadiiiing = true;
        HELPER.instance.OpenMainMenuCanvas();
        float elapsed = 0.0f;
        while (elapsed < duration)
        {
            _animImage.fillAmount = Mathf.Lerp(v_start, v_end, elapsed / duration);
            elapsed += Time.deltaTime;
            yield return null;
        }

        yield return new WaitUntil(() => HELPER.instance.needDisableLoadingCanvas);
        HELPER.instance.needDisableLoadingCanvas = false;
        HELPER.instance.loadingCanvas.gameObject.SetActive(false);
        // Debug.Log("startcor2");

    }

    public void SetFillAmount()
    {
        _animImage.fillAmount = 0;
    }
}

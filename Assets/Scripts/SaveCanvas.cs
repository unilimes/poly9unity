﻿
using UnityEngine;

public class SaveCanvas : MonoBehaviour {

    public Transform saveCanvas;

    public void Save()
    {
        //HELPER.instance.SaveModels();
        //HELPER.instance.isSave = true;
        saveCanvas.gameObject.SetActive(false);
    }

    public void Exit()
    {
        saveCanvas.gameObject.SetActive(false);
       // HELPER.instance.isSave = true;
    }
}

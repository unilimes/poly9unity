﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BinarySearchVertexBuffer  {
    // Checks if 2 floats are nearly equal
    static bool IsEqual(float a_val1, float a_val2)
    {
        double result = Math.Pow(a_val2 - a_val1, 2);
        if (result < 0.000001)
        {
            return true;
        }
        return false;

    }
    // Checks if 2 Vector3 are nearly equal 
    static bool IsVertexEqual(Vector3 a_vertex1,Vector3 a_vertex2)
	{
		if (IsEqual (a_vertex1.x, a_vertex2.x) && IsEqual (a_vertex1.y, a_vertex2.y) && IsEqual (a_vertex1.z, a_vertex2.z))
			return true;
		return false;
	}
	//Checks if 2 Vector2 are nearly equal
	static bool IsUVEqual(Vector2 a_uv1,Vector2 a_uv2)
	{
		if (IsEqual (a_uv1.x, a_uv2.x) && IsEqual (a_uv1.y, a_uv2.y))
			return true;
		return false;
	}


	// Binary Search of vertex Buffer
	public static int BinarySearch(List<Vector3> a_vertexList,List<Vector2> a_uvList, int l, int r, ref Vector3 a_vertex,ref Vector2 a_uv)
	{
		if (r >= l)
		{
			int mid = l + (r - l)/2;

			// If the element is present at the middle 
			// itself
			if(IsVertexEqual(a_vertexList[mid],a_vertex)&& IsUVEqual(a_uv,a_uvList[mid]))
				return mid;


			// If element is smaller than mid, then 
			// it can only be present in left subarray
			if(MergeSortVertexBuffer.IsLesser( a_vertex, a_uv, a_vertexList[mid], a_uvList[mid]))
				return BinarySearch( a_vertexList, a_uvList, l, mid-1,ref  a_vertex,ref a_uv);

			// Else the element can only be present
			// in right subarray
			return BinarySearch( a_vertexList, a_uvList, mid+1, r,ref  a_vertex,ref a_uv);
		}

		// We reach here when element is not 
		// present in array
		return -1;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MergeSortVertexBuffer
{
	// Checks which of the 2 Vertex Buffer is smaller
	public static bool IsLesser (Vector3 a_vertex1, Vector2 a_uv1, Vector3 a_vertex2, Vector2 a_uv2)
	{
		if (a_vertex1.x < a_vertex2.x) {
			if (a_vertex1.y < a_vertex2.y) {
				if (a_vertex1.z < a_vertex2.z) {
					if (a_uv1.x < a_uv2.x) {
						if (a_uv1.y < a_uv2.y) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	static void Merge (ref Vector3[] a_vertex, ref Vector2[] a_uv, int low, int high, int mid)
	{
		// We have low to mid and mid+1 to high already sorted.
		int i, j, k;
		Vector3[] temp = new Vector3[high - low + 1];
		Vector2[] tempUV = new Vector2[high - low + 1];
		i = low;
		k = 0;
		j = mid + 1;

		// Merge the two parts into temp[].
		while (i <= mid && j <= high) {
			if (IsLesser (a_vertex [i], a_uv [i], a_vertex [j], a_uv [j])) {
				temp [k] = a_vertex [i];
				tempUV [k] = a_uv [i];
				k++;
				i++;
			} else {
				temp [k] = a_vertex [j];
				tempUV [k] = a_uv [j];
				k++;
				j++;
			}
		}

		// Insert all the remaining values from i to mid into temp[].
		while (i <= mid) {
			temp [k] = a_vertex [i];
			tempUV [k] = a_uv [i];
			k++;
			i++;
		}

		// Insert all the remaining values from j to high into temp[].
		while (j <= high) {
			temp [k] = a_vertex [j];
			tempUV [k] = a_uv [j];
			k++;
			j++;
		}


		// Assign sorted data stored in temp[] to a[].
		for (i = low; i <= high; i++) {
			a_vertex [i] = temp [i - low];
			a_uv [i] = tempUV [i - low];
		}
	}

	// A function to split array into two parts.
	public static void MergeSort (ref Vector3[] a_vertex, ref Vector2[]  a_uv, int low, int high)
	{
		int mid;
		if (low < high) {
			mid = (low + high) / 2;
			// Split the data into two half.
			MergeSort (ref a_vertex, ref a_uv, low, mid);
			MergeSort (ref a_vertex, ref a_uv, mid + 1, high);

			// Merge them to get sorted output.
			Merge (ref a_vertex, ref a_uv, low, high, mid);
		}
	}


}

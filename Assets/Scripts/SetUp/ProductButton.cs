﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ProductButton : MonoBehaviour {

    public string productType;
    public string productID;
    [SerializeField]
    string url;

    private void Start()
    {
        if (PlayerPrefs.HasKey(productID + "style_Thumbnail_Img"))
        {
            url = PlayerPrefs.GetString(productID + "style_Thumbnail_Img");
        }
        if (PlayerPrefs.HasKey(productID + "style_Name"))
        {

            transform.GetChild(0).GetChild(1).GetComponent<Text>().text = PlayerPrefs.GetString(productID + "style_Name");
        }
		if (url != "") 
		{
			StartCoroutine(LoadHeader());
		}
       
    }

    IEnumerator LoadHeader()
    {
        yield return 0;
        Texture2D tex;
        tex = new Texture2D(1, 1, TextureFormat.DXT1, false);
        using (WWW www = new WWW(url))
        {
            yield return www;
            www.LoadImageIntoTexture(tex);

            var mySprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);

            transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Image>().sprite = mySprite;

        }
    }

    public void ClickOnProduct()
    {
        if (productType == "3DProduct")
        {
            Debug.Log("3DProduct");
            for (int i = 0; i < HELPER.instance.modelsUIParent.childCount; i++)
            {
                if (HELPER.instance.modelsUIParent.GetChild(i).GetChild(0).GetComponent<ModelButton>().product_id == productID)
                {
                    HELPER.instance.mainCanvas.gameObject.SetActive(true);
                    HELPER.instance.modelsUIParent.GetChild(i).GetChild(0).GetComponent<ModelButton>().Click();
                    HELPER.instance.detailBoard.gameObject.SetActive(false);
                    HELPER.instance.mainBoardCanvas.gameObject.SetActive(false);
                }
                
            }


        }
        else if (productType == "Product2D")
        {

            Debug.Log("Product2D");
            HELPER.instance.Product2DCanvas.transform.GetChild(3).GetComponent<Image>().sprite = transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Image>().sprite;
            HELPER.instance.Product2DCanvas.transform.GetChild(5).GetChild(0).GetChild(5).GetComponent<Text>().text = PlayerPrefs.GetString(productID + "style_Name");

            if (HELPER.instance.isPriceShow)
            {
                HELPER.instance.Product2DCanvas.transform.GetChild(5).GetChild(0).GetChild(3).gameObject.SetActive(true);
                HELPER.instance.Product2DCanvas.transform.GetChild(5).GetChild(0).GetChild(4).GetComponent<Text>().text = PlayerPrefs.GetString(productID + "style_price");

                if (PlayerPrefs.HasKey(productID + "styles"))
                {
                    HELPER.instance.Product2DCanvas.transform.GetChild(5).GetChild(0).GetChild(6).gameObject.SetActive(true);
                    HELPER.instance.Product2DCanvas.transform.GetChild(5).GetChild(0).GetChild(6).GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, 48.13335f);
                    var str = PlayerPrefs.GetString(productID + "styles");
                    HELPER.instance.Product2DCanvas.transform.GetChild(5).GetChild(0).GetChild(6).GetChild(1).GetComponent<Text>().text = str;

                }

                else
                {
                    HELPER.instance.Product2DCanvas.transform.GetChild(5).GetChild(0).GetChild(6).GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, 48.13335f);
                    HELPER.instance.Product2DCanvas.transform.GetChild(5).GetChild(0).GetChild(6).gameObject.SetActive(false);
                }



                }
            else
            {
                HELPER.instance.Product2DCanvas.transform.GetChild(5).GetChild(0).GetChild(3).gameObject.SetActive(false);

                if (PlayerPrefs.HasKey(productID + "styles"))
                {
                    HELPER.instance.Product2DCanvas.transform.GetChild(5).GetChild(0).GetChild(6).gameObject.SetActive(true);
                    var str = PlayerPrefs.GetString(productID + "styles");
                    HELPER.instance.Product2DCanvas.transform.GetChild(5).GetChild(0).GetChild(6).GetChild(1).GetComponent<Text>().text = str;
                    HELPER.instance.Product2DCanvas.transform.GetChild(5).GetChild(0).GetChild(6).GetComponent<RectTransform>().anchoredPosition = new Vector2(-53f, 48.13335f);
                }

                else
                {
                    HELPER.instance.Product2DCanvas.transform.GetChild(5).GetChild(0).GetChild(6).gameObject.SetActive(false);
                    HELPER.instance.Product2DCanvas.transform.GetChild(5).GetChild(0).GetChild(6).GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, 48.13335f);
                }

            }






            HELPER.instance.Product2DCanvas.SetActive(true);
            
        }
        else if (productType == "Product")
        {
            Debug.Log("Product");
            for (int i = 0; i < HELPER.instance.modelsUIParent.childCount; i++)
            {
                if (HELPER.instance.modelsUIParent.GetChild(i).GetChild(0).GetComponent<ModelButton>().product_id == productID)
                {
                    HELPER.instance.mainCanvas.gameObject.SetActive(true);
                    HELPER.instance.modelsUIParent.GetChild(i).GetChild(0).GetComponent<ModelButton>().Click();
                    HELPER.instance.detailBoard.gameObject.SetActive(false);
                    HELPER.instance.mainBoardCanvas.gameObject.SetActive(false);
                }

            }
        }

    }

}

﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NewModels {

    List<List<string>> region_Name;
    public string m_url;
    public string m_customization_RegionID;
    public System.Collections.Generic.List<string> m_region_Name;
    public string m_style_Thumbnail_Img;

}

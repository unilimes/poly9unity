﻿using System.Collections.Generic;
using UnityEngine;
using VoxelBusters.RuntimeSerialization;
[RuntimeSerializable]
[System.Serializable]
public class MainMenuButtons  {
    [RuntimeSerializeField]
    public List<int> polygon_GroupIDCount;
    [RuntimeSerializeField]
    public string product_Model_Source_File;
    [RuntimeSerializeField]
    public string product_id;
    [RuntimeSerializeField]
    public string product_StyleID;
    [RuntimeSerializeField]
    public string startFinishID;
    [RuntimeSerializeField]
    public string style_Thumbnail_Img;
    [RuntimeSerializeField]
    public string style_price;
    [RuntimeSerializeField]
    public string stylesCount;
    [RuntimeSerializeField]
    public string styleDescription;
    [RuntimeSerializeField]
    public string prName;
    [RuntimeSerializeField]
    public List<string> regionBaseMaterialID;
    [RuntimeSerializeField]
    public List<string> matIDInThisModel;
    [SerializeField,  RuntimeSerializeField]
    List<string> tempMatList;
    [SerializeField,  RuntimeSerializeField]
    public List<string> regionNameModelBtn;
    [RuntimeSerializeField]
    public List<string> finishesId;

    [Header("Styles")]
    [RuntimeSerializeField]
    public List<string> style_Thumbnail_ImgList;
    [RuntimeSerializeField]
    public List<string> style_priceList;
    [RuntimeSerializeField]
    public List<string> styleDescriptionList;
    [RuntimeSerializeField]
    public List<string> styleNameList;
    [SerializeField, RuntimeSerializeField]
    int counter;
    [RuntimeSerializeField]
    public bool modelIsSave;
}

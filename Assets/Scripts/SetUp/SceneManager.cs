﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class SceneManager : MonoBehaviour
{
	
	public List<Finishes> m_finishes;
	[SerializeField]

	Models[] m_models;
   
	public GameObject m_finishUI;
 
    public GameObject m_modelsUI;
    
    static SceneManager g_manager;
    
    public GameObject m_loadingObject;
    
    public Material m_defaultMaterial;
    
    GameObject m_currentModel;
   
    public Transform m_modelsUIParent;
    
    public Transform m_finishUIParent;
    
    public MainCameraController camController;
    
    public PerCharacterKerning[] per;
   
    public Font customFont;
   
    public Transform castommizatonPanel;
   
    public Transform mainScenePanel;
   
    public Example2_WWW ex;
    //Material mat;
    //  public Texture text2d;
  
    public List<GameObject> ObjWithCurrentCustomTag;
   
    public bool finishTrigger;


    [SerializeField]
    bool firstTexture;
    [SerializeField]
    bool secondTexture;
    [SerializeField]
    bool thirdTexture;
    [SerializeField]
    bool forthTexture;

    public void SaveFinishes()
    {
        for (int i = 0; i < m_finishes.Count; i++)
        {
            PlayerPrefs.SetString(i + "NewID", m_finishes[i].m_id);
            PlayerPrefs.SetString(i + "Newname", m_finishes[i].m_name);
            PlayerPrefs.SetString(i + "Newm_diffuseUrl", m_finishes[i].m_diffuseUrl);
            PlayerPrefs.SetString(i + "Newm_normalMapUrl", m_finishes[i].m_normalMapUrl);
            PlayerPrefs.SetString(i + "Newm_metalnessMapUrl", m_finishes[i].m_metalnessMapUrl);
            PlayerPrefs.SetString(i + "Newm_roughnessMapUrl", m_finishes[i].m_roughnessMapUrl);
            PlayerPrefs.SetString(i + "Newm_previewImageUrl", m_finishes[i].m_previewImageUrl);

            ES2.Save(m_finishes[i].fBaseMatID, i + "NewfBaseMatID");
        }

        PlayerPrefs.SetInt("NewNewFinishesCount", m_finishes.Count);
    }


    public void LoadFinishes()
    {
        var count = PlayerPrefs.GetInt("NewNewFinishesCount");

        Debug.Log("LOAAAD" + count);
        for (int i = 0; i < count; i++)
        {
            Finishes f = new Finishes();

            f.m_id = PlayerPrefs.GetString(i + "NewID");
            f.m_name = PlayerPrefs.GetString(i + "Newname");
            f.m_diffuseUrl = PlayerPrefs.GetString(i + "Newm_diffuseUrl");
            f.m_normalMapUrl = PlayerPrefs.GetString(i + "Newm_normalMapUrl");
            f.m_metalnessMapUrl = PlayerPrefs.GetString(i + "Newm_metalnessMapUrl");
            f.m_roughnessMapUrl = PlayerPrefs.GetString(i + "Newm_roughnessMapUrl");
            f.m_previewImageUrl = PlayerPrefs.GetString(i + "Newm_previewImageUrl");
            f.fBaseMatID = ES2.LoadList<string>(i + "NewfBaseMatID");
            f.m_gameObject = m_finishUIParent.GetChild(i).gameObject;
            m_finishes.Add(f);

        }
    }

    public void InstatiateFinishBtn ()
	{
       
		g_manager = this;
		
        for (int i = 0; i < m_finishes.Count; i++)
        {
            GameObject obj = Instantiate(m_finishUI);
            obj.GetComponentInChildren<Text>().text = m_finishes[i].m_name;
            obj.name = m_finishes[i].m_name;
            obj.transform.SetParent(m_finishUIParent, false);
            if (m_finishes[i].isColor)
            {
                obj.transform.GetChild(0).GetComponent<FinishButton>().isColor = m_finishes[i].isColor;
            }
           
            obj.transform.GetChild(0).GetComponent<FinishButton>().finishID = m_finishes[i].m_id;
            for (int j = 0; j < m_finishes[i].fBaseMatID.Count; j++)
            {
                obj.transform.GetChild(0).GetComponent<FinishButton>().fBaseMatID.Add(m_finishes[i].fBaseMatID[j]);
            }

            m_finishes[i].m_gameObject = obj;
            obj.GetComponentInChildren<Text>().text = m_finishes[i].m_name;
            StartCoroutine(Downloader.INSTANCE.DownloadFile(m_finishes[i].m_previewImageUrl, null, DownloadedPreviewTexture));
        }
    }

    public void InstatiateModelsBtn()
    {
        g_manager = this;
        GameObject obj = Instantiate(m_modelsUI);
        obj.transform.SetParent(m_modelsUIParent, false);
    }



    public void LoadModel()
    {
       
        mainScenePanel.gameObject.SetActive(true);
       // HELPER.instance.panelMainVideo.StartVideo();
        ex.LoadModel();
    }
    // On Obj Loaded OnObjectAdded gets called
    public void OnObjectAdded(GameObject a_object)
	{

        if (GameObject.FindWithTag("CurrentObjParent") != null)
        {
            Destroy(GameObject.FindWithTag("CurrentObjParent"));
        }
		
        mainScenePanel.gameObject.SetActive(false);
        m_currentModel = a_object;
        m_currentModel.tag = "CurrentObjParent";
        m_currentModel.transform.localScale = new Vector3(1f, 1f, 1f);
        for (int i = 0; i < m_currentModel.transform.childCount; i++)
        {
            m_currentModel.transform.GetChild(i).gameObject.AddComponent<MeshCollider>();
            m_currentModel.transform.GetChild(i).tag = "ChildObj1";
            m_currentModel.transform.GetChild(i).gameObject.AddComponent<ModelCustomTag>();
            m_currentModel.transform.GetChild(i).gameObject.AddComponent<cakeslice.Outline>();
            m_currentModel.transform.GetChild(i).gameObject.AddComponent<ColorPickerTester>();
            m_currentModel.transform.GetChild(i).gameObject.GetComponent<ColorPickerTester>().enabled = false;
        }

        var cam = GameObject.FindWithTag("MainCamera");
        cam.GetComponent<MainCameraController>().enabled = true;
        camController.target = m_currentModel.transform;
        HELPER.instance.OpenViewCanvas();
    }


	//On Downloading the Obj file,DownloadedObj is called to start parsing
	public void DownloadedObj (string s,string a_url)
	{
		ObjParser parser = new ObjParser();
		//Debug.Log ("Downloaded " + s);
		StartCoroutine(parser.CreateObjMesh (s));
		parser = null;

	}
	//On Downloading the preview texture file
	public void DownloadedPreviewTexture (Texture s,string a_url)
	{
		for (int i = 0; i < m_finishes.Count; i++)
		{
            if (m_finishes[i].m_previewImageUrl == null)
            {
                Debug.Log("preview image = null");
            }
            else if (m_finishes[i].m_previewImageUrl.Equals(a_url))
            {
                Sprite sprite = Sprite.Create((Texture2D)s, new Rect(0, 0, s.width, s.height), new Vector2(1.0f, 1.0f));

                m_finishes[i].m_gameObject.transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().sprite = sprite;
                return;
            }
        }
	}


    public void OpenStyle(Transform btnID)
    {
        HELPER.instance.viewPopupLoading.SetActive(true);
        HELPER.instance.currentFinishesID.Clear();
        HELPER.instance.currentProductNameHelper = btnID.GetChild(0).GetChild(1).GetComponent<Text>().text;
        for (int i = 0; i < HELPER.instance.regionNameHelper.Count; i++)
        {
            if (PlayerPrefs.HasKey(HELPER.instance.currentProductID + "StylesFinishes_ID" + i + btnID.GetSiblingIndex()))
            {
                var finishID = PlayerPrefs.GetString(HELPER.instance.currentProductID + "StylesFinishes_ID" + i + btnID.GetSiblingIndex());
                HELPER.instance.currentFinishesID.Add(finishID);
            }
        }

        StartCoroutine(StartLoad());
    }
   
    public void LoadTextures()
    {
        StartCoroutine(StartLoad());
    }

    IEnumerator StartLoad()
    {
        HELPER.instance.SetupModelTag();

        for (int i = 0; i < HELPER.instance.regionNameHelper.Count; i++)
        {
            HELPER.instance.currentRegionNameHelper = HELPER.instance.regionNameHelper[i];
            for (int j = 0; j < m_finishes.Count; j++)
            {
                if (HELPER.instance.currentFinishesID[i].Equals(m_finishes[j].m_id) && !HELPER.instance.isOfflineMode) //m_finishes[j].m_material == null
                {

                  //  if (m_finishes[j].m_material == null)
                   // {
                      //  Debug.Log("miss material");
                        firstTexture = false;
                        secondTexture = false;
                        thirdTexture = false;
                        forthTexture = false;

                        LoadFinish(m_finishes[j].m_name);
                        Debug.Log(m_finishes[j].m_name);
                        finishTrigger = false;
                        Debug.Log("Before");

                        yield return new WaitUntil(() => firstTexture && secondTexture && thirdTexture && forthTexture);

                        Debug.Log("After");

                        if (i == 0 && m_finishes[j].isColor)
                        {
                            HELPER.instance.needActiveColorPicker = true;
                        }
                        else if (i == 0 && !m_finishes[j].isColor)
                        {
                            HELPER.instance.needActiveColorPicker = false;
                        }


                        yield return new WaitUntil(() => finishTrigger == true);


                        if (i == HELPER.instance.regionNameHelper.Count - 1)
                        {
                            mainScenePanel.gameObject.SetActive(false);
                            HELPER.instance.viewPopupLoading.SetActive(false);
                            //Save Model
                            if (!HELPER.instance.isOfflineMode)
                            {
                                HELPER.instance.savedModel = GameObject.FindWithTag("CurrentObjParent");
                                HELPER.instance.SaveModel();
                            }
                            HELPER.instance.OpenViewCanvas();
                        }
                   // }
                    //else if (m_finishes[j].m_material != null)
                    //{
                    //    Debug.Log("foundMat " + m_finishes[j].m_id);
                    //    ApplyMaterial(m_finishes[j].m_name);

                    //    if (i == HELPER.instance.regionNameHelper.Count - 1)
                    //    {
                    //        mainScenePanel.gameObject.SetActive(false);
                    //        HELPER.instance.viewPopupLoading.SetActive(false);
                    //        //Save Model
                    //        if (!HELPER.instance.isOfflineMode)
                    //        {
                    //            HELPER.instance.savedModel = GameObject.FindWithTag("CurrentObjParent");
                    //            HELPER.instance.SaveModel();
                    //        }
                    //        HELPER.instance.OpenViewCanvas();
                    //    }
                    //}
                }

                else if (HELPER.instance.currentFinishesID[i].Equals(m_finishes[j].m_id) && HELPER.instance.isOfflineMode)
                {
                    Debug.Log("miss material");
                    firstTexture = false;
                    secondTexture = false;
                    thirdTexture = false;
                    forthTexture = false;

                    LoadFinish(m_finishes[j].m_name);
                    Debug.Log(m_finishes[j].m_name);
                    finishTrigger = false;
                    Debug.Log("Before");

                    yield return new WaitUntil(() => firstTexture && secondTexture && thirdTexture && forthTexture);

                    Debug.Log("After");

                    if (i == 0 && m_finishes[j].isColor)
                    {
                        HELPER.instance.needActiveColorPicker = true;
                    }
                    else if (i == 0 && !m_finishes[j].isColor)
                    {
                        HELPER.instance.needActiveColorPicker = false;
                    }


                    yield return new WaitUntil(() => finishTrigger == true);


                    if (i == HELPER.instance.regionNameHelper.Count - 1)
                    {
                        mainScenePanel.gameObject.SetActive(false);
                        HELPER.instance.viewPopupLoading.SetActive(false);
                        //Save Model
                        if (!HELPER.instance.isOfflineMode)
                        {
                            HELPER.instance.savedModel = GameObject.FindWithTag("CurrentObjParent");
                            HELPER.instance.SaveModel();
                        }
                        HELPER.instance.OpenViewCanvas();
                    }
                }
            }

        }
    }


    void ApplyMaterial(string name)
    {
        ObjWithCurrentCustomTag.Clear();

        var parentModel = GameObject.FindWithTag("CurrentObjParent");

        for (int i = 0; i < parentModel.transform.childCount; i++)
        {
            if (parentModel.transform.GetChild(i).GetComponent<ModelCustomTag>().customTag == HELPER.instance.currentRegionNameHelper)

            {
                ObjWithCurrentCustomTag.Add(parentModel.transform.GetChild(i).gameObject);
            }
        }

        for (int i = 0; i < ObjWithCurrentCustomTag.Count; i++)
        {

            DestroyImmediate(ObjWithCurrentCustomTag[i].GetComponent<MeshRenderer>().material.GetTexture("_BumpMap"), true);
            DestroyImmediate(ObjWithCurrentCustomTag[i].GetComponent<MeshRenderer>().material.GetTexture("_MetallicGlossMap"), true);
            DestroyImmediate(ObjWithCurrentCustomTag[i].GetComponent<MeshRenderer>().material.GetTexture("_SpecGlossMap"), true);
            DestroyImmediate(ObjWithCurrentCustomTag[i].GetComponent<MeshRenderer>().material.mainTexture, true);
            ObjWithCurrentCustomTag[i].GetComponent<MeshRenderer>().material.CopyPropertiesFromMaterial(m_defaultMaterial);
        }


        for (int i = 0; i < m_finishes.Count; i++)
        {

            if (name.Equals(m_finishes[i].m_name))
            {

                castommizatonPanel.gameObject.SetActive(true);

                for (int j = 0; j < ObjWithCurrentCustomTag.Count; j++)
                {
                    ObjWithCurrentCustomTag[j].GetComponent<MeshRenderer>().material = m_finishes[i].m_material;

                    if (HELPER.instance.currentOpenCunvas == HELPER.openCanvas.Customize)
                    {
                        if (j == 0 && m_finishes[i].isColor)
                        {
                            Debug.Log("ActiveColor");
                            HELPER.instance.EnablePicker();
                        }
                        else
                        {
                            Debug.Log("DiactiveteColor");
                            HELPER.instance.DisableColorPicker();

                            for (int k = 0; k < parentModel.transform.childCount; k++)
                            {
                                parentModel.transform.GetChild(k).GetComponent<ColorPickerTester>().savedColor = Color.white;
                                parentModel.transform.GetChild(k).GetComponent<MeshRenderer>().material.color = Color.white;
                            }

                        }
                    }

                }
            }

        }
    }

    public void LoadFinish(string a_name)
	{
        ObjWithCurrentCustomTag.Clear();

        var parentModel = GameObject.FindWithTag("CurrentObjParent");

        for (int i = 0; i < parentModel.transform.childCount; i++)
        {
            if (parentModel.transform.GetChild(i).GetComponent<ModelCustomTag>().customTag == HELPER.instance.currentRegionNameHelper)

            {
                ObjWithCurrentCustomTag.Add(parentModel.transform.GetChild(i).gameObject);
            }
        }

        for (int i = 0; i < ObjWithCurrentCustomTag.Count; i++)
        {
            
            DestroyImmediate(ObjWithCurrentCustomTag[i].GetComponent<MeshRenderer>().material.GetTexture("_BumpMap"), true);
            DestroyImmediate(ObjWithCurrentCustomTag[i].GetComponent<MeshRenderer>().material.GetTexture("_MetallicGlossMap"), true);
            DestroyImmediate(ObjWithCurrentCustomTag[i].GetComponent<MeshRenderer>().material.GetTexture("_SpecGlossMap"), true);
            DestroyImmediate(ObjWithCurrentCustomTag[i].GetComponent<MeshRenderer>().material.mainTexture, true);
            ObjWithCurrentCustomTag[i].GetComponent<MeshRenderer>().material.CopyPropertiesFromMaterial(m_defaultMaterial);
        }

            for (int i = 0; i < m_finishes.Count; i++) {

            if (a_name.Equals (m_finishes [i].m_name))
            {
              
                castommizatonPanel.gameObject.SetActive(true);
             
                for (int j = 0; j < ObjWithCurrentCustomTag.Count; j++)
                {
                    m_finishes[i].m_material = ObjWithCurrentCustomTag[j].GetComponent<MeshRenderer>().material;

                    if (HELPER.instance.currentOpenCunvas == HELPER.openCanvas.Customize)
                    {
                        if (j == 0 && m_finishes[i].isColor)
                        {
                            Debug.Log("ActiveColor");
                            HELPER.instance.EnablePicker();
                        }
                        else
                        {
                            Debug.Log("DiactiveteColor");
                            HELPER.instance.DisableColorPicker();

                            for (int k = 0; k < parentModel.transform.childCount; k++)
                            {
                                //Debug.Log("w " + k);
                                parentModel.transform.GetChild(k).GetComponent<ColorPickerTester>().savedColor = Color.white;
                                parentModel.transform.GetChild(k).GetComponent<MeshRenderer>().material.color = Color.white;
                            }

                        }
                    }
                    
                }

                if (!HELPER.instance.isOfflineMode)
                {
                    if (m_finishes[i].m_diffuseUrl != "" && m_finishes[i].m_diffuseUrl != null)
                    {
                        StartCoroutine(Downloader.INSTANCE.DownloadFile(m_finishes[i].m_diffuseUrl, null, DownloadedDiffuseTexture));
                    }

                    else
                    {
                        HELPER.instance.JustExit();
                    }
                    
                    StartCoroutine(Downloader.INSTANCE.DownloadFile(m_finishes[i].m_normalMapUrl, null, DownloadedNormalTexture));
                    StartCoroutine(Downloader.INSTANCE.DownloadFile(m_finishes[i].m_metalnessMapUrl, null, DownloadedSpecularTexture));
                    StartCoroutine(Downloader.INSTANCE.DownloadFile(m_finishes[i].m_roughnessMapUrl, null, DownloadedRoughnessTexture));


                    // StartCoroutine(NewLoaderDiffuse(m_finishes[i].m_diffuseUrl));
                    // StartCoroutine(NewLoaderNormal (m_finishes[i].m_normalMapUrl));
                    //StartCoroutine(NewLoaderSpecular(m_finishes[i].m_metalnessMapUrl));
                    // StartCoroutine(NewLoaderRoughness(m_finishes[i].m_roughnessMapUrl));



                    return;
                }
                else
                {


                    var tex1 = LoadPNG(Path.Combine(Application.persistentDataPath, i + "diffuse.jpg"));
                    var tex3 = LoadPNG(Path.Combine(Application.persistentDataPath, i + "metal.jpg"));
                    var tex2 = LoadPNG(Path.Combine(Application.persistentDataPath, i + "normal.jpg"));
                    var tex4 = LoadPNG(Path.Combine(Application.persistentDataPath, i + "roughness.jpg"));



                    //var tex1 = ES2.Load<Texture2D>(i + "diffuseTexture");
                    //var tex2 = ES2.Load<Texture2D>(i + "normalMap");
                    //var tex3 = ES2.Load<Texture2D>(i + "metalnessTexture");
                    //var tex4 = ES2.Load<Texture2D>(i + "roughnessTexture");

                    for (int j = 0; j < ObjWithCurrentCustomTag.Count; j++)
                    {
                        ObjWithCurrentCustomTag[j].GetComponent<MeshRenderer>().material.mainTexture = tex1;
                        ObjWithCurrentCustomTag[j].GetComponent<MeshRenderer>().material.SetTexture("_BumpMap", tex2);
                        ObjWithCurrentCustomTag[j].GetComponent<MeshRenderer>().material.SetTexture("_MetallicGlossMap", tex3);
                        ObjWithCurrentCustomTag[j].GetComponent<MeshRenderer>().material.SetTexture("_SpecGlossMap", tex4);
                    }

                    Resources.UnloadUnusedAssets();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    castommizatonPanel.gameObject.SetActive(false);
                }


               
            }
            
        }
	}


    public static Texture2D LoadPNG(string filePath)
    {

        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData); 
        }
        return tex;
    }



    //On Downloading the diffuse texture file
    public void DownloadedDiffuseTexture (Texture s,string a_url)
	{
        Debug.Log("DownloadDiffuse");
		for (int i = 0; i < m_finishes.Count; i++)
        {
            if (m_finishes[i].m_diffuseUrl == null)
            {
                Debug.Log("miss diffuse");
            }
            else if (m_finishes [i].m_diffuseUrl.Equals (a_url))
            {
                
                m_finishes [i].m_material.mainTexture = s;
                

                for (int j = 0; j < ObjWithCurrentCustomTag.Count; j++)

                {
                    ObjWithCurrentCustomTag[j].GetComponent<MeshRenderer>().material = m_finishes[i].m_material;
                }
                if (m_finishes[i].m_normalMapUrl != null && m_finishes[i].m_normalMapUrl != "")
                {
                  //  StartCoroutine(Downloader.INSTANCE.DownloadFile(m_finishes[i].m_normalMapUrl, null, DownloadedNormalTexture));
                }
                else
                {
                    HELPER.instance.JustExit();
                }
                firstTexture = true;
                return;
			}
			
		}
	}
	//On Downloading the normal texture file
	public void DownloadedNormalTexture (Texture s,string a_url)
	{
        Debug.Log("DownloadNormal");
        for (int i = 0; i < m_finishes.Count; i++){

            if (m_finishes[i].m_normalMapUrl == null)
            {
                Debug.Log("miss Normal");
            }

			else if (m_finishes [i].m_normalMapUrl.Equals (a_url)) {

                for (int j = 0; j < ObjWithCurrentCustomTag.Count; j++)
                {
                    m_finishes[i].m_material.SetTexture("_BumpMap", s);
                }

                if (m_finishes[i].m_metalnessMapUrl != null && m_finishes[i].m_metalnessMapUrl != "")
                {
                 //   StartCoroutine(Downloader.INSTANCE.DownloadFile(m_finishes[i].m_metalnessMapUrl, null, DownloadedSpecularTexture));
                }
                else
                {
                    HELPER.instance.JustExit();
                }


                secondTexture = true;
                return;
			}
		}
	}
	//On Downloading the specular texture file
	public void DownloadedSpecularTexture (Texture s,string a_url)
	{
        Debug.Log("DownloadSpec");
        for (int i = 0; i < m_finishes.Count; i++) {


            if (m_finishes[i].m_metalnessMapUrl == null)
            {
                Debug.Log("miss specular");
            }
			else if (m_finishes [i].m_metalnessMapUrl.Equals (a_url)) {

                for (int j = 0; j < ObjWithCurrentCustomTag.Count; j++)

                {
                    m_finishes[i].m_material.SetTexture("_MetallicGlossMap", s);
                }
                if (m_finishes[i].m_roughnessMapUrl != null && m_finishes[i].m_roughnessMapUrl != "")
                {
                 //   StartCoroutine(Downloader.INSTANCE.DownloadFile(m_finishes[i].m_roughnessMapUrl, null, DownloadedRoughnessTexture));
                }
                else
                {
                    HELPER.instance.JustExit();
                }
                thirdTexture = true;
                return;
			}
		}
	}
	//On Downloading the roughness texture file
	public void DownloadedRoughnessTexture (Texture s,string a_url)
	{
        Debug.Log("DownloadRoughness");
        for (int i = 0; i < m_finishes.Count; i++) {

            if (m_finishes[i].m_metalnessMapUrl == null)
            {
                Debug.Log("miss roughness");
            }

            else if (m_finishes [i].m_roughnessMapUrl.Equals (a_url)) {

                for (int j = 0; j < ObjWithCurrentCustomTag.Count; j++)

                {
                    m_finishes[i].m_material.SetTexture("_SpecGlossMap", s);
                }

                castommizatonPanel.gameObject.SetActive(false);
                Resources.UnloadUnusedAssets();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                finishTrigger = true;

                for (int j = 0; j < ObjWithCurrentCustomTag.Count; j++)
                {
                    if (ObjWithCurrentCustomTag[j].GetComponent<ColorPickerTester>().savedColor != Color.white)
                    {
                        ObjWithCurrentCustomTag[j].GetComponent<MeshRenderer>().material.color = ObjWithCurrentCustomTag[j].GetComponent<ColorPickerTester>().savedColor;
                    }
                   
                }

                forthTexture = true;
                return;
			}
		}
	}

    public static SceneManager INSTANCE { get; set; }
}

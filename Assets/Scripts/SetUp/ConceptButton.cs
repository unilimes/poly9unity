﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ConceptButton : MonoBehaviour {

    public string productType;
    public string hex;
    public string title;
    public string url;
    public string description;
    Color newCol;


    private void Start()
    {
        StartLoadImage();
    }

    public void GetColor()
    {
        hex = "#" + hex;
        if (ColorUtility.TryParseHtmlString(hex, out newCol))
        {
            transform.GetChild(0).GetChild(0).GetComponent<Image>().color = newCol;
            transform.GetChild(0).GetChild(3).gameObject.SetActive(false);
        }
        transform.GetChild(0).GetChild(1).GetComponent<Text>().text = title;
        transform.GetChild(0).GetChild(2).GetComponent<Text>().text = hex.ToUpper();
       
    }

    public void StartLoadImage()
    {
        if (url != "" && (hex == "jpg" || hex == "jpeg" || hex == "png"))
        {
            StartCoroutine(LoadHeader());
            transform.GetChild(0).GetChild(3).gameObject.SetActive(false);
        }
        transform.GetChild(0).GetChild(1).GetComponent<Text>().text = title;
        transform.GetChild(0).GetChild(2).GetComponent<Text>().text = hex;
       
    }

    IEnumerator LoadHeader()
    {
        yield return 0;
        Texture2D tex;
        tex = new Texture2D(1, 1, TextureFormat.DXT1, false);
        using (WWW www = new WWW(url))
        {
            yield return www;
            www.LoadImageIntoTexture(tex);

            var mySprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);

            transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = mySprite;

        }
    }

    public void ClickOnConcept()
    {
        if (productType == "Color")
        {
            Debug.Log("Color");
            HELPER.instance.colorCanvas.transform.GetChild(3).GetComponent<Text>().text = title;
            HELPER.instance.colorCanvas.transform.GetChild(4).GetComponent<Text>().text = hex;
            HELPER.instance.colorCanvas.transform.GetChild(8).GetComponent<Text>().text = description;

            HELPER.instance.colorCanvas.SetActive(true);

            if (ColorUtility.TryParseHtmlString(hex, out newCol))
            {
                HELPER.instance.colorCanvas.transform.GetChild(6).GetComponent<Image>().color = newCol;
            }
        }
        else if (productType == "Concept")
        {
            Debug.Log("Concept");
            HELPER.instance.conceptCanvas.transform.GetChild(3).GetComponent<Text>().text = title;
            HELPER.instance.conceptCanvas.transform.GetChild(4).GetComponent<Text>().text = hex;
            HELPER.instance.conceptCanvas.transform.GetChild(8).GetComponent<Text>().text = description;

            HELPER.instance.conceptCanvas.SetActive(true);
            

            FindObjectOfType<MobileUIController>().url = url; 
        }
    }

}

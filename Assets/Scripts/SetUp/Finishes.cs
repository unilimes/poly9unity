﻿using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Finishes {
  //[RuntimeSerializeField]
    public string m_id;
   // [RuntimeSerializeField]
    public string m_name;
    //[RuntimeSerializeField]
    public string m_diffuseUrl;
    //[RuntimeSerializeField]
    public string m_normalMapUrl;
   // [RuntimeSerializeField]
    public string m_metalnessMapUrl;
   // [RuntimeSerializeField]
    public string m_roughnessMapUrl;
   // [RuntimeSerializeField]
    public string m_previewImageUrl;
   // [RuntimeSerializeField]
    public List <string> fBaseMatID;
   // [HideInInspector]
   // [RuntimeSerializeField]
    public GameObject m_gameObject;
	//[HideInInspector]
   // [RuntimeSerializeField]
    public Material m_material;
    // [RuntimeSerializeField]
    public bool isColor;


}

﻿
using UnityEngine;

public class SetHeight : MonoBehaviour {

    public Transform firstC;
    public Transform secondC;
    public RectTransform content;

    public void CulculateHeight()
    {
        var first = 0f;
        var second = 0f;
        content.sizeDelta = new Vector2(content.sizeDelta.x, 0);
        for (int i = 0; i < firstC.childCount; i++)
        {
             first += firstC.GetChild(i).GetComponent<RectTransform>().sizeDelta.y + 16f;
             Debug.Log("f " + first);
        }

        for (int i = 0; i < secondC.childCount; i++)
        {
            second += secondC.GetChild(i).GetComponent<RectTransform>().sizeDelta.y + 16f;
            Debug.Log("s "+ second);
        }

        if (first > second)
        {
            content.sizeDelta = new Vector2(content.sizeDelta.x, first);
        }
        else
        {
            content.sizeDelta = new Vector2(content.sizeDelta.x, second);
        }
    } 

}

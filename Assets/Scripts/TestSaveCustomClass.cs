﻿using System.Collections.Generic;
using UnityEngine;
using VoxelBusters.RuntimeSerialization;
[RuntimeSerializable(typeof(MonoBehaviour), false)]

public class SaveCustomClass : MonoBehaviour {

    [RuntimeSerializeField]
    public List<Finishes> finishes;

    [RuntimeSerializeField]
    public List<MainMenuButtons> buttons;

}

﻿using UnityEngine;

[VoxelBusters.RuntimeSerialization.RuntimeSerializable(typeof(MonoBehaviour), false)]
public class ModelCustomTag : MonoBehaviour {

    [VoxelBusters.RuntimeSerialization.RuntimeSerializeField]
    public string customTag;
	
}

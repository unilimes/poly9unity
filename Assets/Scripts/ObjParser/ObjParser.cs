﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ObjParser
{

	List<Vector3> m_fullVertexList = new List<Vector3> ();
	List<Vector2> m_fullUVList = new List<Vector2> ();
	List<int> m_faces = new List<int> ();
	List<Vector3> m_vertexList = new List<Vector3> ();
	List<Vector2> m_uvList = new List<Vector2> ();
	float m_scale = 100000f;
	float m_size = 3f;
	string m_meshName = "";

	// Obj Mesh Loading  
	public IEnumerator  CreateObjMesh (string a_objData)
	{
		string[] lines = a_objData.Split (Environment.NewLine.ToCharArray ());
		GameObject obj = new GameObject ();
		obj.SetActive (false);
		int i = 0;
		//for (int i = 0; i < lines.Length; i++)
		while (i != lines.Length) {
			ParseLine (lines [i], obj);
			i++;
			if (i % 5000 == 0)
				yield return null;
		}
		if (!m_meshName.Equals ("")) {
			CreateMesh (obj);
		}
		Vector3 scale = new Vector3 (m_scale, m_scale, m_scale);
		obj.transform.localScale = scale;
		obj.SetActive (true);
		yield return null;
		m_fullVertexList.Clear ();
		m_faces.Clear ();
		m_fullUVList.Clear ();
		System.GC.Collect ();
		Debug.Log ("Loaded");
		SceneManager.INSTANCE.OnObjectAdded (obj);

		yield return null;
	}

	//Add to Vertex Buffer
	int AddVertexBuffer (int a_vertexId, int a_uvId)
	{
		Vector3 vertex = m_fullVertexList [a_vertexId];
		Vector2 uv = m_fullUVList [a_uvId];
		m_vertexList.Add (vertex);
		m_uvList.Add (uv);
		return m_vertexList.Count - 1;

	}
	//Parse Obj data to fill vertex List
	bool ParseVertex (string[] a_coords)
	{
		if (a_coords.Length < 4) {
			Debug.LogError ("Input array must be of minimum length " + 4);
			return false;
		}

		if (!a_coords [0].ToLower ().Equals ("v")) {
			Debug.LogError ("Data prefix must be '" + "v" + "'");
			return false;
		}

		bool success;

		float x, y, z;


		x = float.Parse (a_coords [1]);
		y = float.Parse (a_coords [2]);
		z = float.Parse (a_coords [3]);
		Vector3 coords = new Vector3 ();
		coords.x = x;
		coords.y = y;
		coords.z = z;
		m_fullVertexList.Add (coords);
		return true;
	}
	//Parse Obj data to fill index  List
	bool ParseFace (string[] a_faceData)
	{
		if (a_faceData.Length < 4) {
			Debug.LogError ("Input array must be of minimum length " + 4);
			return false;
		}

		if (!a_faceData [0].ToLower ().Equals ("f")) {
			Debug.LogError ("Data prefix must be '" + "f" + "'"); 
			return false;
		}

		int vcount = a_faceData.Length - 1;


		for (int i = 0; i < vcount; i++) {
			string[] parts = a_faceData [i + 1].Split ('/');

			int vindex = int.Parse (parts [0]);

			if (vindex > m_fullVertexList.Count)
				Debug.LogError ("index : " + vindex + " vertexcount " + m_fullVertexList.Count);
			int offset = 1;
			if (vindex < 0) {
				vindex = m_fullVertexList.Count + vindex;
				offset = 0;
			}
			if (vindex < 0)
				Debug.LogError ("index : " + vindex + " is less than 0");
				
			int uvIndex;
			if (parts.Length > 1) {
				uvIndex = int.Parse (parts [1]);
					
				int uvOffset = 1;
				if (uvIndex < 0) {
					uvIndex = m_fullUVList.Count + uvIndex;
					uvOffset = 0;
				}
				if (uvIndex > m_fullUVList.Count)
					Debug.LogError ("index : " + uvIndex + " uvcount " + m_fullUVList.Count);
				int index = AddVertexBuffer (vindex - offset, uvIndex - uvOffset);
				m_faces.Add (index);
					
			}

		}
		if (vcount == 4) {
			int first = m_faces [m_faces.Count - 4];
			int second = m_faces [m_faces.Count - 2];
			m_faces.Insert (m_faces.Count - 1, first);
			m_faces.Insert (m_faces.Count - 1, second);
		}
		return true;
	}
	//Parse Obj data to fill UV List
	public bool ParseUV (string[] data)
	{
		if (data.Length < 3) {
			Debug.LogError ("Input array must be of minimum length " + 3);
			return false;
		}

		if (!data [0].ToLower ().Equals ("vt")) {
			Debug.LogError ("Data prefix must be '" + "vt" + "'");
			return false;
		}


		float x, y;


		x = float.Parse (data [1]);
		y = float.Parse (data [2]);

		Vector2 uv = new Vector2 (x, y);
		m_fullUVList.Add (uv);
		return true;
	}
	//Weld vertices to reduce Vertex Buffer data
	public void WeldVertices (out Vector3[] out_vertex, out Vector2[] out_uv)
	{
		Vector3[] vertex = m_vertexList.ToArray ();
		Vector2[] uv = m_uvList.ToArray ();
		MergeSortVertexBuffer.MergeSort (ref vertex, ref uv, 0, m_vertexList.Count - 1);
		List<Vector3> vertices = new List<Vector3> ();
		List<Vector2> uvs = new List<Vector2> ();
		int[] map = new int[m_vertexList.Count];
		for (int i = 0; i < m_vertexList.Count; i++) {
			Vector3 vert = m_vertexList [i];
			Vector2 currentUV = m_uvList [i];
			int id = BinarySearchVertexBuffer.BinarySearch (vertices, uvs, 0, vertices.Count - 1, ref vert, ref currentUV);
			if (id == -1) {
				vertices.Add (vert);
				uvs.Add (currentUV);
				map [i] = vertices.Count - 1;
			} else {
				map [i] = id;
			}
		}

		for (int i = 0; i < m_faces.Count; i++) {
			
			m_faces [i] = map [m_faces [i]];
		}

		out_vertex = vertices.ToArray ();
		out_uv = uvs.ToArray ();
		vertices.Clear ();
		uvs.Clear ();
		vertex = null;
		uv = null;
	}
	//Scale GameObjects to have same size after loading
	void SetScale (float a_scale)
	{
		if (m_scale > a_scale)
			m_scale = a_scale;
	}

	void ScaleValue (Bounds bound)
	{
		Vector3 extents = bound.extents;
		float x = m_size / extents.x;
		float y = m_size / extents.y;
		float z = m_size / extents.z;
		if (x > y) {
			if (y > z) {
				SetScale (z);
			} else {
				SetScale (y);
			}
		} else {
			if (x > z) {
				SetScale (z);
			} else {
				SetScale (x);
			}
		}

	}
	//Create GameObjects from data
	void CreateMesh (GameObject a_parent)
	{
		GameObject obj = new GameObject ();
		obj.name = m_meshName;
		m_meshName = "";
		MeshFilter meshFilter = obj.AddComponent<MeshFilter> ();
		Mesh mesh = new Mesh ();
		mesh.name = obj.name;
		Vector2[] uv;
		Vector3[] vertex;
		WeldVertices (out vertex, out uv);

		mesh.vertices = vertex;
		mesh.uv = uv;
		mesh.SetTriangles (m_faces, 0);
		meshFilter.mesh = mesh;
		MeshRenderer mr = obj.AddComponent<MeshRenderer> ();
        //mr.material = SceneManager.INSTANCE.m_defaultMaterial;
        mr.material = new Material(Shader.Find("Standard (Roughness setup)"));

        obj.transform.parent = a_parent.transform;
		if (m_faces.Count % 3 != 0)
			Debug.LogError ("face data count is :" + m_faces.Count + " mesh : " + obj.name);
		m_faces.Clear ();
		mesh.RecalculateNormals ();
		mesh.RecalculateTangents ();
		mesh.RecalculateBounds ();
		Bounds bound = mesh.bounds;
		ScaleValue (bound);

		m_vertexList.Clear ();
		m_uvList.Clear ();
	
	}
	//Parse Obj data & determine the type of data
	void ParseLine (string a_line, GameObject a_parent)
	{
		a_line = a_line.Replace ("\r", "");
		string[] parts = a_line.Split (new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

		if (parts.Length > 0) {
			switch (parts [0]) {
			case "v":
				if (!m_meshName.Equals ("")) {
					CreateMesh (a_parent);
				}
				ParseVertex (parts);

				break;
			case "f":
				//if(!ParseFace (parts))
				//	Debug.LogError(a_line);
				ParseFace (parts);
				break;
			case "vt":
				ParseUV (parts);

				break;
			case "g":
				if (!parts [1].Equals ("default")) {
					if (parts.Length == 3)
						m_meshName = parts [2];
					else
						m_meshName = parts [1];
					

				} 
				break;
			

			}
		}
	}
}

﻿using System.IO;
using UnityEngine;

[RequireComponent(typeof(MobileUIView))]
public class MobileUIController : MonoBehaviour {
	[SerializeField]
	private EasyBgDownloaderCtl ebdCtl;
	[SerializeField]
	private MobileUIView mobileUIView;
	[SerializeField]
	private BrowseView browseView;
    
    private string currentDownloadURL = "";


    public string url;

	// Use this for initialization
	void Start () {
		ebdCtl.OnComplete = OnCompleteDownload;
	}
	
	// Update is called once per frame
	//void Update () {
 //      //update download progress
	//   if (!string.IsNullOrEmpty(currentDownloadURL) && ebdCtl.IsRunning(currentDownloadURL)) {
 //          mobileUIView.ChangeHeaderLabel("DOWNLOADING");
 //          mobileUIView.ChangeDownloadingFileNameWithPath(currentDownloadURL);
 //          mobileUIView.ChangeProgress(ebdCtl.GetProgress(currentDownloadURL));
 //      }
	//}


	/*
	 * Downloading Panel
	 */
	public void OnProgressChanged () {
		mobileUIView.ChangePercentageLabel (mobileUIView.progressManager.value);
	}
	// END : Downloading Panel

	/*
	 * Start&StopButton
	 */
	public void OnClickCtlButton () {
        string inputText = url;
        if (string.IsNullOrEmpty(inputText)) {
            return;
        }
        
		
		OnClickStartBtn (inputText);
		
	}

	private void OnClickStartBtn (string inputURL) {
        currentDownloadURL = inputURL;
		//  mobileUIView.EnableStopButton ();
        ebdCtl.StartDL(inputURL);
	}

	private void OnClickStopBtn (string inputURL) {
        currentDownloadURL = null;
        mobileUIView.ChangeHeaderLabel("CANCELD");
        mobileUIView.ChangeDownloadingFileName("No file downloading");
        mobileUIView.ChangeProgress(0.0f);
		mobileUIView.EnableStartButton ();
        ebdCtl.StopDL(inputURL);
	}
	// END : Start&StopButton

	/*
	 * Footer Tab
	 */
	public void OnClickTabDownload () {
		if (!mobileUIView.IsOpenDownloadPage()) {
			mobileUIView.GoDownlaodPage ();
		}
	}
	public void OnClickTabBrowse () {
		if (mobileUIView.IsOpenDownloadPage()) {
			mobileUIView.GoBrowsePage ();
		}
	}
	// END : Footer Tab

	/*
	 * EBD Event
	 */
	public void OnCompleteDownload (string requestURL, string destPath) {
        // browseView.RefreshFileList ();
        //  if (currentDownloadURL == requestURL) {
        //mobileUIView.EnableStartButton ();
        //      mobileUIView.ChangeDownloadingFileName("No file downloading");
        //      mobileUIView.ChangeHeaderLabel("COMPLETE");
        //      mobileUIView.ChangeProgress(0.0f);
        //  }
#if UNITY_ANDROID
        int dotIndex = requestURL.LastIndexOf('/');
        if (dotIndex >= 0)
        {
            requestURL = requestURL.Substring(dotIndex + 1);
        }
        Debug.Log("Complete " + destPath + requestURL);
        //RefreshAndroidFile(destPath + requestURL);
	}
#endif
    //void RefreshAndroidFile(string path) //Refresh file to show up in mtp mode
    //{
    //    if (!File.Exists(path))
    //        return;

    //    using (AndroidJavaClass jcUnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
    //    using (AndroidJavaObject joActivity = jcUnityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
    //    using (AndroidJavaObject joContext = joActivity.Call<AndroidJavaObject>("getApplicationContext"))
    //    using (AndroidJavaClass jcMediaScannerConnection = new AndroidJavaClass("android.media.MediaScannerConnection"))
    //        jcMediaScannerConnection.CallStatic("scanFile", joContext, new string[] { path }, null, null);
    //}

    public void OnErrorDownload (string requestURL, string errorMessage) {
        mobileUIView.ChangeHeaderLabel(errorMessage);
    }
}

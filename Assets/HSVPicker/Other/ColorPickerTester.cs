﻿using UnityEngine;

[VoxelBusters.RuntimeSerialization.RuntimeSerializable(typeof(MonoBehaviour), false)]
public class ColorPickerTester : MonoBehaviour
{
    [VoxelBusters.RuntimeSerialization.RuntimeSerializeField]
    new Renderer renderer;
    [VoxelBusters.RuntimeSerialization.RuntimeSerializeField]
    ColorPicker picker;
    [VoxelBusters.RuntimeSerialization.RuntimeSerializeField]
    public Color savedColor = Color.white;


    public void DisableColorPicker()
    {
        picker = FindObjectOfType<ColorPicker>();
        if (picker != null)
        {
            picker.onValueChanged.RemoveAllListeners();
            //if (GetComponent<MeshRenderer>().material.color != Color.white)
            //{
            //     savedColor = GetComponent<MeshRenderer>().material.color;
            //  }
            savedColor = Color.white;
        }
    }

    public void EnableColorPicker()
    {
        picker = FindObjectOfType<ColorPicker>();
        if (picker != null)
        {
            renderer = GetComponent<Renderer>();
            picker.onValueChanged.AddListener(color =>
            {
                renderer.material.color = color;
                //savedColor = color;
                savedColor = Color.white;
            });

            //if (GetComponent<MeshRenderer>().material.color != Color.white)
            //{
            //    savedColor = GetComponent<MeshRenderer>().material.color;
            //    picker.CurrentColor = savedColor;
            //}
            // else
            // {
            picker.CurrentColor = Color.white;
            //}
            renderer.material.color = picker.CurrentColor;
        }
        else
        {
            Debug.Log("pICKER = NULL");
        }
    }
    
}

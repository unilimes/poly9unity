﻿using UnityEngine;
using System.Collections;
using VoxelBusters.RuntimeSerialization;

public class JointLimitsRSExtension : IRuntimeSerializableExtension 
{
	#region Constants
	
	private 	const	string		kMaxKey			= "max";
	private 	const	string		kMaxBounceKey	= "maxBounce";
	private 	const	string		kMinKey			= "min";
	private 	const	string		kMinBounceKey	= "minBounce";

	#endregion

	#region Serialization Methods
	
	public override void WriteSerializationData (object _object, RuntimeSerializationInfo _info)
	{
		JointLimits	_jointLimits	= (JointLimits)_object;
		
		// Serialize properties
		_info.AddValue<float>(kMaxKey,	 		_jointLimits.max);
		_info.AddValue<float>(kMinKey, 			_jointLimits.min);


		_info.AddValue<float>(kMaxBounceKey,	_jointLimits.bounciness);
		_info.AddValue<float>(kMinBounceKey,	_jointLimits.bounciness);


	}
	
	public override object ReadSerializationData (object _object, RuntimeSerializationInfo _info)
	{
		JointLimits	_jointLimits	= (JointLimits)_object;

		// Deserialize properties
		_jointLimits.max			= _info.GetValue<float>(kMaxKey);
		_jointLimits.min			= _info.GetValue<float>(kMinKey);


		_jointLimits.bounciness		=  _info.GetValue<float>(kMaxBounceKey);

		
		return _jointLimits;
	}
	
	#endregion
}
﻿using UnityEngine;
using Moulin.DDP;
using UnityEngine.UI;
using System.Collections;

public class TestDdpAccount : MonoBehaviour
{
    public string serverUrl = "wss://clgupta.poly9.io/websocket"; //ws://clgupta.polynine.co/websocket - test
    public string username;
    public string password;
    string token;
    public bool logMessages;
    public InputField loginText;
    public InputField passText;
    private DdpConnection ddpConnection;
    private DdpAccount account;
    public AnimImage ai;

    public bool callbackPrStyleList;
    public bool callbackPrList;
    public bool callbackFinishList;
    public bool callbackMaterialList;
    float currCountdownValue;
    public GameObject alert;

    public void Awake()
    {
        ddpConnection = new DdpConnection(serverUrl);
        ddpConnection.logMessages = logMessages;

        account = new DdpAccount(ddpConnection);

        ddpConnection.OnConnected += (DdpConnection connection) =>
        {
            Debug.Log("Connected.");
        };

        ddpConnection.OnDisconnected += (DdpConnection connection) =>
        {
            Debug.Log("Disconnected.");

            StartCoroutine(CoroutineHelper.GetInstance().RunAfter(() =>
            {
                Debug.Log("Try to reconnect ...");
                connection.Connect();
            }, 2.0f));
        };

        ddpConnection.OnError += (DdpError error) =>
        {
            Debug.Log("Error: " + error.errorCode + " " + error.reason);
        };

        ddpConnection.OnAdded += (collection, id, fields) =>
        {
            Debug.Log("Added docId " + id +
                " in collection " + collection);
        };

        ddpConnection.OnRemoved += (collection, id) =>
        {
            Debug.Log("Removed docId " + id +
                " in collection " + collection);
        };

        ddpConnection.OnChanged += (collection, id, fields, cleared) =>
        {
            Debug.Log("Changed docId " + id +
                " in collection " + collection +
                " fields: " + fields +
                " cleared:" + cleared);
        };

        ddpConnection.OnAddedBefore += (collection, id, fields, before) =>
        {
            Debug.Log("Added docId " + id +
                " before docId " + before +
                " in collection " + collection +
                " fields: " + fields);
        };

        ddpConnection.OnMovedBefore += (collection, id, before) =>
        {
            Debug.Log("Moved docId " + id +
                " before docId " + before +
                " in collection " + collection);
        };

        ConnectToServer();

    }



   // public void Update()
  //  {

        //    //       if (Input.GetKeyDown(KeyCode.C)) {
        //    //		Debug.Log("Connecting ...");
        //    //		ddpConnection.Connect();
        //    //	}

        //    //	if (Input.GetKeyDown(KeyCode.V)) {
        //    //		ddpConnection.Close();
        //    //	}

        //    //	if (Input.GetKeyDown(KeyCode.U)) {
        //    //		StartCoroutine(account.CreateUserAndLogin(username, password));
        //    //	}

        //    //if (Input.GetKeyDown(KeyCode.L)) {
        //    //	StartCoroutine(account.Login(username, password));
        //    //}

        //    //	if (Input.GetKeyDown(KeyCode.R)) {
        //    //		StartCoroutine(account.ResumeSession(token));
        //    //	}

        //    //	if (Input.GetKeyDown(KeyCode.T)) {
        //    //		Debug.Log("Token " + account.token + " expires at " + account.tokenExpiration);
        //    //	}

        //if (Input.GetKeyDown(KeyCode.O))
       // {
            
       // }

        //if (Input.GetKeyDown(KeyCode.P))
        //{

        //    StartCoroutine(account.GetprductStyleList());

        //    //GetProductList();
        //}

        //if (Input.GetKeyDown(KeyCode.W))
        //{

        //    StartCoroutine(account.GetProductList());
        //}

        //if (Input.GetKeyDown(KeyCode.F))
        //{

        //    StartCoroutine(account.GetFinishesList());
        //}

        //if (Input.GetKeyDown(KeyCode.D))
        //{

        //    StartCoroutine(account.GetUserDetails());
        //}

        //if (Input.GetKeyDown(KeyCode.M))
        //{

        //    StartCoroutine(account.GetMaterialList());
        //}

   // }

    public void ShowPrice()
    {
        StartCoroutine(account.IsPriceShowFormanufacturer());
    }


    

    IEnumerator StartCountdown(float countdownValue = 20)
    {
        currCountdownValue = countdownValue;
        while (currCountdownValue > 0)
        {
            Debug.Log("Countdown: " + currCountdownValue);
            yield return new WaitForSeconds(1.0f);
            currCountdownValue--;
        }

        Debug.Log("End");
        alert.SetActive(true);
    }

    public void Meow()
    {
        StartCoroutine(StartCountdown());
    }
    public void LogOut()
    {
       
        callbackPrStyleList = false;
        callbackPrList = false;
        callbackFinishList = false;
        callbackMaterialList = false;
        HELPER.instance.userID = null;
        StartCoroutine(account.Logout());
        PlayerPrefs.DeleteAll();
        HELPER.instance.CloseAllCanvases();
        HELPER.instance.DeleteAllSa();
        HELPER.instance.gameObject.GetComponent<SceneManager>().m_finishes.Clear();
        
        for (int i = 0; i < HELPER.instance.modelsUIParent.childCount; i++)
        {
            Destroy(HELPER.instance.modelsUIParent.GetChild(i).gameObject);
        }

        for (int i = 0; i < HELPER.instance.finishesUIParent.childCount; i++)
        {
            Destroy(HELPER.instance.finishesUIParent.GetChild(i).gameObject);
        }

        for (int i = 0; i < HELPER.instance.mainBoardContent.childCount; i++)
        {
            Destroy(HELPER.instance.mainBoardContent.GetChild(i).gameObject);
        }
        StopAllCoroutines();
        HELPER.instance.loginCanvas.gameObject.SetActive(true);

        
    }

    public void GetBoardDetails()
    {
        StartCoroutine(account.GetBoardList());
    }

    public void GetprductStyleList()
    {
        StartCoroutine(account.GetprductStyleList());
    }

    public void GetUserDetails()
    {
        StartCoroutine(account.GetUserDetails());
    }

    public void GetMaterialList()
    {
        StartCoroutine(account.GetMaterialList());
    }

    public void GetProductList()
    {
        StartCoroutine(account.GetProductList());
    }

    public void GetFinishes()
    {
        StartCoroutine(account.GetFinishesList());
    }

    public void ConnectToServer()
    {
        Debug.Log("Connecting ...");
        ddpConnection.Connect();
    }

    public void LoginUser()
    {
        var loginGood = false;
        var passGood = false;
        if (loginText.text != "" && loginText.text.Contains("@") && loginText.text.Length > 5)
        {
            loginGood = true;
           
        }
        else
        {
            loginGood = false;
        }

        if (passText.text.Length <= 2)
        {

            passGood = false;
        }
        else
        {
            passGood = true;
        }

        if (!loginGood && !passGood)
        {
            HELPER.instance.IncorrectLoginAndPass();
        }

        if (loginGood && !passGood)
        {
            HELPER.instance.IncorrectPass();
        }
        if (!loginGood && passGood)
        {
            HELPER.instance.IncorrectLogin();
        }

        if (loginGood && passGood)
        {
            if (ddpConnection.ddpConnectionState != DdpConnection.ConnectionState.CONNECTED)
            {
                ConnectToServer();
            }

            username = loginText.text;
            password = passText.text;
            StartCoroutine(account.LoginWithEmail(username, password));
           // ShowPrice();

        }
    }

    public void CheckUserLoginStatus()
    {
        HELPER.instance.loginCanvas.gameObject.SetActive(false);
        HELPER.instance.loadingCanvas.gameObject.SetActive(true);

        if (callbackFinishList && callbackMaterialList && callbackPrList && callbackPrStyleList)
        {
            ai.StartAnim();
            Debug.Log("user login");
        }

    }


    public void OfflineMode()
    {
        HELPER.instance.loginCanvas.gameObject.SetActive(false);
        HELPER.instance.loadingCanvas.gameObject.SetActive(true);
        Debug.Log("offline mode");
    }

    public void NewToPoly()
    {
        Application.OpenURL("https://polynine.com");
    }

    public void TermsAndConditions()
    {
        Application.OpenURL("https://polynine.com/terms");
    }
}

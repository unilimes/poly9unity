using System;
using System.Collections;

namespace Moulin.DDP {

	public class MethodCall {

		public string id;
		public string methodName;
		public JSONObject[] items;

		public bool hasUpdated;
		public Action<MethodCall> OnUpdated;

		public JSONObject result;
		public bool hasResult;
		public Action<MethodCall> OnResult;

		public DdpError error;

        public enum methods
        {
            DEFAULT,
            GET_USER_DETAILS,
            FINISHES_LIST,
            PRODUCTS_LIST,
            PRODUCTS_STYLE_LIST,
            LOGIN,
            MATERIAL_LIST,
            PRODUCT_PARTS,
            IS_PRICE_SHOW,
            BOARD,
        };

        public methods currentMethods;

		public IEnumerator WaitForResult() {
			while (!hasResult) {
				yield return null;
			}
             
			yield break;
		}

	}

}

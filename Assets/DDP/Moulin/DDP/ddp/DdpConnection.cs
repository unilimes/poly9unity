using UnityEngine;
using System;
using System.Threading.Collections;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace Moulin.DDP
{

    /*
	 * DDP protocol:
	 *   https://github.com/meteor/meteor/blob/master/packages/ddp/DDP.md
	 */
    public class DdpConnection : MonoBehaviour, IDisposable
    {

        public List<string> dataString;

        // The possible values for the "msg" field.
        public class MessageType
        {
            // Client -> server.
            public const string CONNECT = "connect";
            public const string PONG = "pong";
            public const string SUB = "sub";
            public const string UNSUB = "unsub";
            public const string METHOD = "method";

            // Server -> client.
            public const string CONNECTED = "connected";
            public const string FAILED = "failed";
            public const string PING = "ping";
            public const string NOSUB = "nosub";
            public const string ADDED = "added";
            public const string CHANGED = "changed";
            public const string REMOVED = "removed";
            public const string READY = "ready";
            public const string ADDED_BEFORE = "addedBefore";
            public const string MOVED_BEFORE = "movedBefore";
            public const string RESULT = "result";
            public const string UPDATED = "updated";
            public const string ERROR = "error";
        }

        // Field names supported in the DDP protocol.
        public class Field
        {
            public const string SERVER_ID = "server_id";
            public const string MSG = "msg";
            public const string SESSION = "session";
            public const string VERSION = "version";
            public const string SUPPORT = "support";

            public const string NAME = "name";
            public const string PARAMS = "params";
            public const string SUBS = "subs";
            public const string COLLECTION = "collection";
            public const string FIELDS = "fields";
            public const string CLEARED = "cleared";
            public const string BEFORE = "before";

            public const string ID = "id";
            public const string METHOD = "method";
            public const string METHODS = "methods";
            public const string RANDOM_SEED = "randomSeed"; // unused
            public const string RESULT = "result";

            public const string ERROR = "error";
            public const string REASON = "reason";
            public const string DETAILS = "details";
            public const string MESSAGE = "message";   // undocumented
            public const string ERROR_TYPE = "errorType"; // undocumented
            public const string OFFENDING_MESSAGE = "offendingMessage";

            public const string MATERIAL_NAME = "material_Name";

           
            
        }

        public enum ConnectionState
        {
            NOT_CONNECTED,
            CONNECTING,
            CONNECTED,
            DISCONNECTED,
            CLOSING,
            CLOSED
        }

        // The DDP protocol version implemented by this library.
        public const string DDP_PROTOCOL_VERSION = "1";

        private CoroutineHelper coroutineHelper;
        private ConcurrentQueue<JSONObject> messageQueue = new ConcurrentQueue<JSONObject>();

        private WebSocketSharp.WebSocket ws;
        public ConnectionState ddpConnectionState;
        private string sessionId;

        private Dictionary<string, Subscription> subscriptions = new Dictionary<string, Subscription>();
        private Dictionary<string, MethodCall> methodCalls = new Dictionary<string, MethodCall>();
        

        private int subscriptionId;
        private int methodCallId;

        public delegate void OnConnectedDelegate(DdpConnection connection);
        public delegate void OnDisconnectedDelegate(DdpConnection connection);
        public delegate void OnConnectionClosedDelegate(DdpConnection connection);
        public delegate void OnAddedDelegate(string collection, string docId, JSONObject fields);
        public delegate void OnChangedDelegate(string collection, string docId, JSONObject fields, JSONObject cleared);
        public delegate void OnRemovedDelegate(string collection, string docId);
        public delegate void OnAddedBeforeDelegate(string collection, string docId, JSONObject fields, string before);
        public delegate void OnMovedBeforeDelegate(string collection, string docId, string before);
        public delegate void OnErrorDelegate(DdpError error);

        public event OnConnectedDelegate OnConnected;
        public event OnDisconnectedDelegate OnDisconnected;
        public event OnConnectionClosedDelegate OnConnectionClosed;
        public event OnAddedDelegate OnAdded;
        public event OnChangedDelegate OnChanged;
        public event OnRemovedDelegate OnRemoved;
        public event OnAddedBeforeDelegate OnAddedBefore;
        public event OnMovedBeforeDelegate OnMovedBefore;
        public event OnErrorDelegate OnError;

        public bool logMessages;

        public DdpConnection(string url)
        {
            coroutineHelper = CoroutineHelper.GetInstance();
            coroutineHelper.StartCoroutine(HandleMessages());

            ws = new WebSocketSharp.WebSocket(url);
            ws.OnOpen += OnWebSocketOpen;
            ws.OnError += OnWebSocketError;
            ws.OnClose += OnWebSocketClose;
            ws.OnMessage += OnWebSocketMessage;
        }

        private void OnWebSocketOpen(object sender, EventArgs e)
        {
            Send(GetConnectMessage());

            foreach (Subscription subscription in subscriptions.Values)
            {
                Send(GetSubscriptionMessage(subscription));
            }
            foreach (MethodCall methodCall in methodCalls.Values)
            {
                Send(GetMethodCallMessage(methodCall));
            }
        }

        private void OnWebSocketError(object sender, WebSocketSharp.ErrorEventArgs e)
        {
            coroutineHelper.RunInMainThread(() =>
            {
                if (OnError != null)
                {
                    OnError(new DdpError()
                    {
                        errorCode = "WebSocket error",
                        reason = e.Message
                    });
                }
            });
        }

        private void OnWebSocketClose(object sender, WebSocketSharp.CloseEventArgs e)
        {
            if (e.WasClean)
            {
                ddpConnectionState = ConnectionState.CLOSED;
                sessionId = null;
                subscriptions.Clear();
                methodCalls.Clear();
                coroutineHelper.RunInMainThread(() =>
                {
                    if (OnDisconnected != null)
                    {
                        OnDisconnected(this);
                    }
                });
            }
            else
            {
                ddpConnectionState = ConnectionState.DISCONNECTED;
                coroutineHelper.RunInMainThread(() =>
                {
                    if (OnDisconnected != null)
                    {
                        OnDisconnected(this);
                    }
                });
            }
        }

        private void OnWebSocketMessage(object sender, WebSocketSharp.MessageEventArgs e)
        {
            if (logMessages) Debug.Log("OnMessage: " + e.Data); 

           

            JSONObject message = new JSONObject(e.Data);
            messageQueue.Enqueue(message);
           
        }

        private IEnumerator HandleMessages()
        {
            while (true)
            {
                JSONObject message = null;
                while (messageQueue.TryDequeue(out message))
                {
                    HandleMessage(message);
                }
                yield return null;
            }
        }

        private void HandleMessage(JSONObject message)
        {
            if (!message.HasField(Field.MSG))
            {
                // Silently ignore those messages.
                return;
            }

            switch (message[Field.MSG].str)
            {
                case MessageType.CONNECTED:
                    {
                        sessionId = message[Field.SESSION].str;
                        ddpConnectionState = ConnectionState.CONNECTED;

                        if (OnConnected != null)
                        {
                            OnConnected(this);
                        }
                        break;
                    }

                case MessageType.FAILED:
                    {
                        if (OnError != null)
                        {
                            OnError(new DdpError()
                            {
                                errorCode = "Connection refused",
                                reason = "The server is using an unsupported DDP protocol version: " +
                                    message[Field.VERSION]
                            });
                        }
                        Close();
                        break;
                    }

                case MessageType.PING:
                    {
                        if (message.HasField(Field.ID))
                        {
                            Send(GetPongMessage(message[Field.ID].str));
                        }
                        else
                        {
                            Send(GetPongMessage());
                        }
                        break;
                    }

                case MessageType.NOSUB:
                    {
                        string subscriptionId = message[Field.ID].str;
                        subscriptions.Remove(subscriptionId);

                        if (message.HasField(Field.ERROR))
                        {
                            if (OnError != null)
                            {
                                OnError(GetError(message[Field.ERROR]));
                            }
                        }
                        break;
                    }
                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                case MessageType.ADDED:
                    {
                        if (OnAdded != null)
                        {
                            OnAdded(
                                message[Field.COLLECTION].str,
                                message[Field.ID].str,
                                message[Field.FIELDS]
                              );
                        }
                        break;
                    }

                case MessageType.CHANGED:
                    {
                        if (OnChanged != null)
                        {
                            OnChanged(
                                message[Field.COLLECTION].str,
                                message[Field.ID].str,
                                message[Field.FIELDS],
                                message[Field.CLEARED]);
                        }
                        break;
                    }

                case MessageType.REMOVED:
                    {
                        if (OnRemoved != null)
                        {
                            OnRemoved(
                                message[Field.COLLECTION].str,
                                message[Field.ID].str);
                        }
                        break;
                    }

                case MessageType.READY:
                    {
                        string[] subscriptionIds = ToStringArray(message[Field.SUBS]);

                        foreach (string subscriptionId in subscriptionIds)
                        {
                            Subscription subscription = subscriptions[subscriptionId];
                            if (subscription != null)
                            {
                                subscription.isReady = true;
                                if (subscription.OnReady != null)
                                {
                                    subscription.OnReady(subscription);
                                }
                            }
                        }
                        break;
                    }

                case MessageType.ADDED_BEFORE:
                    {
                        if (OnAddedBefore != null)
                        {
                            OnAddedBefore(
                                message[Field.COLLECTION].str,
                                message[Field.ID].str,
                                message[Field.FIELDS],
                                message[Field.BEFORE].str);
                        }
                        break;
                    }

                case MessageType.MOVED_BEFORE:
                    {
                        if (OnMovedBefore != null)
                        {
                            OnMovedBefore(
                                message[Field.COLLECTION].str,
                                message[Field.ID].str,
                                message[Field.BEFORE].str);
                        }
                        break;
                    }

                case MessageType.RESULT:
                    {
                        string methodCallId = message[Field.ID].str;
                        MethodCall methodCall = methodCalls[methodCallId];

                        
                            if (message.HasField(Field.ERROR))
                            {
                                methodCall.error = GetError(message.GetField("error"));


                                if (methodCall.error.reason == "User not found")
                                {
                                    Debug.Log("user not found ");
                                    HELPER.instance.IncorrectLogin();
                                    methodCall.currentMethods = MethodCall.methods.DEFAULT;
                                }
                                else if (methodCall.error.reason == "Incorrect password")
                                {
                                    Debug.Log("pass incorect ");
                                    HELPER.instance.IncorrectPass();
                                    methodCall.currentMethods = MethodCall.methods.DEFAULT;
                                }

                            }

                            else
                            {
                                methodCall.result = message[Field.RESULT];


                                if (methodCall.hasUpdated)
                                {
                                    methodCalls.Remove(methodCallId);
                                }
                                methodCall.hasResult = true;
                                if (methodCall.OnResult != null)
                                {
                                    methodCall.OnResult(methodCall);
                                }

                                Debug.Log("Res " + methodCall.result);
                                Debug.Log(methodCall.currentMethods);


                                #region Price
                                if (methodCall.currentMethods == MethodCall.methods.IS_PRICE_SHOW)
                                {
                                   
                                    bool res = Convert.ToBoolean(methodCall.result.str);
                                Debug.Log(res);
                                HELPER.instance.isPriceShow = res;
                                    var testDDPAccount = FindObjectOfType<TestDdpAccount>();
                                    testDDPAccount.GetprductStyleList();
                                    methodCall.currentMethods = MethodCall.methods.DEFAULT;
                                }
                                #endregion

                                #region Get User Details

                                if (methodCall.currentMethods == MethodCall.methods.GET_USER_DETAILS)
                                {
                                    var firstChar = methodCall.result[3].GetField("companyName").str.Substring(0, 1);

                                    PlayerPrefs.SetString("FirstCharacter", firstChar);

                                    HELPER.instance.userName.text = methodCall.result[3].GetField("firstName").str + " " + methodCall.result[3].GetField("lastName").str;
                                    HELPER.instance.userNameProfile.text = methodCall.result[3].GetField("firstName").str + " " + methodCall.result[3].GetField("lastName").str;
                                    HELPER.instance.phoneProfile.text = methodCall.result[3].GetField("phoneNumber").str;
                                    HELPER.instance.adressProfile.text = methodCall.result[3].GetField("address").str;

                                    for (int i = 0; i < methodCall.result[2].Count; i++)
                                    {
                                        HELPER.instance.emailID.text = methodCall.result[2][i].GetField("address").str;
                                    }


                                    var url = HELPER.instance.absoletePath + methodCall.result[3].GetField("logo").str;

                                    HELPER.instance.LoadLogo(url);

                                    for (int i = 0; i < HELPER.instance.CompanyNameList.Count; i++)
                                    {
                                        HELPER.instance.CompanyNameList[i].text = string.Empty;
                                        HELPER.instance.CompanyNameList[i].text = methodCall.result[3].GetField("companyName").str;
                                    }
                                    PlayerPrefs.SetString("CompanyName", methodCall.result[3].GetField("companyName").str);

                                    HELPER.instance.departamentNameProfile.text = methodCall.result[3].GetField("role").str;
                                    HELPER.instance.companyNameProfile.text = methodCall.result[3].GetField("companyName").str;

                                    HELPER.instance.firstCharacterCompanyName.text = firstChar.ToUpper();
                                    //Debug.Log("11111111111111111111111111111111111111111111111111111");
                                    var testDDPAccount = FindObjectOfType<TestDdpAccount>();
                                    //testDDPAccount.CheckUserLoginStatus();
                                    testDDPAccount.GetProductList();
                                    testDDPAccount.GetMaterialList();
                                    testDDPAccount.GetFinishes();
                                    testDDPAccount.ShowPrice();
                                    testDDPAccount.GetBoardDetails();
                                    //testDDPAccount.GetprductStyleList();

                                    methodCall.currentMethods = MethodCall.methods.DEFAULT;
                                }
                                #endregion

                                #region prductStyleList

                                else if (methodCall.currentMethods == MethodCall.methods.PRODUCTS_STYLE_LIST)
                                {
                                   

                                        List<string> style_Thumbnail_ImgList = new List<string>();
                                        List<string> style_NameList = new List<string>();
                                        List<string> style_PriceList = new List<string>();
                                        List<string> style_DescriptionList = new List<string>();

                                        for (int i = 0; i < methodCall.result.Count; i++)
                                        {

                                            PlayerPrefs.SetString(methodCall.result[i].GetField("productID").str, methodCall.result[i].GetField("productID").str);

                                            for (int j = 0; j < methodCall.result[i].GetField("styles").Count; j++)
                                            {
                                                if (j == methodCall.result[i].GetField("styles").Count - 1)
                                                {
                                                    PlayerPrefs.SetString(methodCall.result[i].GetField("productID").str + "style_Thumbnail_Img", HELPER.instance.absoletePath + methodCall.result[i].GetField("styles")[j].GetField("style_Thumbnail_Img").str);
                                                    PlayerPrefs.SetString(methodCall.result[i].GetField("productID").str + "style_Name", methodCall.result[i].GetField("styles")[j].GetField("style_Name").str);
                                                    PlayerPrefs.SetString(methodCall.result[i].GetField("productID").str + "styles", methodCall.result[i].GetField("styles").Count.ToString());
                                                    PlayerPrefs.SetString(methodCall.result[i].GetField("productID").str + "style_ID", methodCall.result[i].GetField("styles")[j].GetField("style_ID").str);
                                                    double timestamp = Convert.ToDouble(methodCall.result[i].GetField("styles")[j].GetField("createdAt")[0].ToString());
                                                    var dateTime = new DateTime(1970, 1, 1).AddMilliseconds(timestamp).ToLocalTime();
                                                    Debug.Log("tickkks " + dateTime.Ticks);
                                                    ES2.Save(dateTime.Ticks, methodCall.result[i].GetField("productID").str + "time");
                                                    if (methodCall.result[i].GetField("styles")[j].HasField("stylesRegion"))
                                                    {
                                                        for (int k = 0; k < methodCall.result[i].GetField("styles")[j].GetField("stylesRegion").Count; k++)
                                                        {
                                                            if (methodCall.result[i].GetField("styles")[j].GetField("stylesRegion")[k].HasField("finishes_ID"))
                                                            {
                                                                var finishID = methodCall.result[i].GetField("styles")[j].GetField("stylesRegion")[k].GetField("finishes_ID").str;
                                                                PlayerPrefs.SetString(methodCall.result[i].GetField("productID").str + "finishes_ID" + k, finishID);
                                                            }

                                                        }
                                                    }


                                                    if (methodCall.result[i].GetField("styles")[j].HasField("style_Description"))
                                                    {
                                                        PlayerPrefs.SetString(methodCall.result[i].GetField("productID").str + "style_Description", methodCall.result[i].GetField("styles")[j].GetField("style_Description").str);
                                                    }

                                                    if (HELPER.instance.isPriceShow)
                                                    {
                                                        PlayerPrefs.SetString(methodCall.result[i].GetField("productID").str + "style_price", methodCall.result[i].GetField("styles")[j].GetField("style_price").ToString());
                                                    }
                                                }
                                                else
                                                {
                                                    if (methodCall.result[i].GetField("styles")[j].HasField("style_Description"))
                                                    {
                                                        style_DescriptionList.Add(methodCall.result[i].GetField("styles")[j].GetField("style_Description").str);
                                                    }


                                                    style_PriceList.Add(methodCall.result[i].GetField("styles")[j].GetField("style_price").ToString());
                                                    style_Thumbnail_ImgList.Add(HELPER.instance.absoletePath + methodCall.result[i].GetField("styles")[j].GetField("style_Thumbnail_Img").str);
                                                    style_NameList.Add(methodCall.result[i].GetField("styles")[j].GetField("style_Name").str);



                                                    if (methodCall.result[i].GetField("styles")[j].HasField("stylesRegion"))
                                                    {
                                                        for (int k = 0; k < methodCall.result[i].GetField("styles")[j].GetField("stylesRegion").Count; k++)
                                                        {
                                                            if (methodCall.result[i].GetField("styles")[j].GetField("stylesRegion")[k].HasField("finishes_ID"))
                                                            {
                                                                var finishID = methodCall.result[i].GetField("styles")[j].GetField("stylesRegion")[k].GetField("finishes_ID").str;
                                                                PlayerPrefs.SetString(methodCall.result[i].GetField("productID").str + "StylesFinishes_ID" + k + j, finishID);
                                                            }


                                                        }
                                                    }
                                                }
                                            }


                                            ES2.Save(style_NameList, methodCall.result[i].GetField("productID").str + "style_NameList");
                                            ES2.Save(style_Thumbnail_ImgList, methodCall.result[i].GetField("productID").str + "style_Thumbnail_ImgList");

                                            if (HELPER.instance.isPriceShow)
                                            {
                                                ES2.Save(style_PriceList, methodCall.result[i].GetField("productID").str + "style_PriceList");
                                            }

                                            if (style_DescriptionList.Count > 0)
                                            {
                                                ES2.Save(style_DescriptionList, methodCall.result[i].GetField("productID").str + "style_DescriptionList");
                                            }
                                            style_DescriptionList.Clear();
                                            style_PriceList.Clear();
                                            style_Thumbnail_ImgList.Clear();
                                            style_NameList.Clear();
                                        }

                                        methodCall.currentMethods = MethodCall.methods.DEFAULT;
                                        var testDDPAccount = FindObjectOfType<TestDdpAccount>();
                                        testDDPAccount.callbackPrStyleList = true;
                                        testDDPAccount.CheckUserLoginStatus();
                                    
                                }
                                #endregion

                                #region prductList


                                else if (methodCall.currentMethods == MethodCall.methods.PRODUCTS_LIST)
                                {
                                    if (methodCall.result != null)
                                    {
                                        var sm = FindObjectOfType<SceneManager>();

                                        for (int i = 0; i < methodCall.result.Count; i++)
                                        {
                                            var id = methodCall.result[i].GetField("_id").str;
                                            if (methodCall.result[i].HasField("product_Customization_Regions"))
                                            {
                                                for (int j = 0; j < methodCall.result[i].GetField("product_Customization_Regions").Count; j++)
                                                {

                                                    var prName = methodCall.result[i].GetField("product_Customization_Regions")[j].GetField("region_Name").str;

                                                    HELPER.instance.region_ID.Add(id);
                                                    List<string> listMaterialID = new List<string>();


                                                    for (int k = 0; k < methodCall.result[i].GetField("product_Customization_Regions")[j].GetField("region_Base_MaterialID").Count; k++)
                                                    {
                                                        var baseRegion = methodCall.result[i].GetField("product_Customization_Regions")[j].GetField("region_Base_MaterialID")[k].str;
                                                        HELPER.instance.region_Base_MaterialID.Add(baseRegion);
                                                        listMaterialID.Add(baseRegion);
                                                    }

                                                    ES2.Save(listMaterialID, id + prName);
                                                }
                                            }


                                        }


                                        for (int i = 0; i < methodCall.result.Count; i++)
                                        {
                                            sm.InstatiateModelsBtn();
                                            var tempBtn = HELPER.instance.modelsUIParent.GetChild(HELPER.instance.modelsUIParent.childCount - 1);
                                            var tempModelButton = HELPER.instance.modelsUIParent.GetChild(HELPER.instance.modelsUIParent.childCount - 1).GetChild(0).GetComponent<ModelButton>();

                                            if (methodCall.result[i].HasField("product_Model_Source_File"))
                                            {
                                                tempModelButton.product_Model_Source_File = HELPER.instance.absoletePath + methodCall.result[i].GetField("product_Model_Source_File").str;
                                            }


                                            var id = methodCall.result[i].GetField("_id").str;
                                            tempModelButton.product_id = id;
                                            tempBtn.GetChild(0).GetChild(1).GetComponent<Text>().text = methodCall.result[i].GetField("product_Name").str;

                                            if (methodCall.result[i].HasField("product_Customization_Regions"))
                                            {
                                                for (int j = 0; j < methodCall.result[i].GetField("product_Customization_Regions").Count; j++)
                                                {
                                                    int meshCounter = 0;
                                                    //Debug.Log("CustomRegions " + j);

                                                    var regionName = methodCall.result[i].GetField("product_Customization_Regions")[j].GetField("region_Name").str;

                                                    tempModelButton.regionNameModelBtn.Add(regionName);
                                                    // HELPER.instance.regionName.Add();

                                                    for (int l = 0; l < methodCall.result[i].GetField("product_Customization_Regions")[j].GetField("polygon_GroupID").Count; l++)
                                                    {
                                                        //Debug.Log(methodCall.result[i].GetField("product_Customization_Regions")[j].GetField("polygon_GroupID")[l]);
                                                        meshCounter++;
                                                        var polygonName = methodCall.result[i].GetField("product_Customization_Regions")[j].GetField("polygon_GroupID")[l].str;

                                                        if (polygonName.Contains("_"))
                                                        {
                                                            polygonName = polygonName.Replace("_", " ");
                                                        }

                                                        //Debug.Log("Saaaave   " + id + regionName + polygonName);
                                                        PlayerPrefs.SetString(id + regionName + polygonName, "");

                                                    }

                                                    //tempModelButton.polygon_GroupIDCount.Add(meshCounter);
                                                }
                                            }


                                        }

                                        HELPER.instance.parentForMainScroll.GetComponent<SetupHeightOnScroll>().Setup();

                                        for (int i = 0; i < methodCall.result.Count; i++)
                                        {

                                            if (methodCall.result[i].HasField("product_Customization_Regions"))
                                            {
                                                for (int j = 0; j < methodCall.result[i].GetField("product_Customization_Regions").Count; j++)
                                                {
                                                    for (int k = 0; k < methodCall.result[i].GetField("product_Customization_Regions")[j].GetField("region_finishes_techniques").Count; k++)
                                                    {
                                                        {
                                                            HELPER.instance.region_finishes_techniques.Add(methodCall.result[i].GetField("product_Customization_Regions")[j].GetField("region_finishes_techniques")[k].str);
                                                        }
                                                    }
                                                }
                                            }

                                        }

                                        var testDDPAccount = FindObjectOfType<TestDdpAccount>();
                                        testDDPAccount.callbackPrList = true;

                                    }

                                    methodCall.currentMethods = MethodCall.methods.DEFAULT;
                                }

                                #endregion

                                #region Finishes_list 

                                else if (methodCall.currentMethods == MethodCall.methods.FINISHES_LIST)
                                {

                                    if (methodCall.result != null)
                                    {
                                        List<string> fbmID = new List<string>();

                                        var sceneManager = FindObjectOfType<SceneManager>();
                                        for (int i = 0; i < methodCall.result.Count; i++)
                                        {

                                            fbmID.Clear();
                                            Finishes f = new Finishes();

                                            for (int j = 0; j < methodCall.result[i].GetField("finishes_Base_MaterialsID").Count; j++)
                                            {
                                                fbmID.Add(methodCall.result[i].GetField("finishes_Base_MaterialsID")[j].str);
                                            }

                                            f.m_id = methodCall.result[i].GetField("_id").str;
                                            f.m_name = methodCall.result[i].GetField("finishes_Name").str;

                                            if (methodCall.result[i].HasField("finishes_Photo"))
                                            {
                                                f.m_previewImageUrl = HELPER.instance.absoletePath + methodCall.result[i].GetField("finishes_Photo").str;
                                            }
                                            if (methodCall.result[i].HasField("roughness"))
                                            {
                                                f.m_roughnessMapUrl = HELPER.instance.absoletePath + methodCall.result[i].GetField("roughness").str;
                                            }

                                            if (methodCall.result[i].HasField("diffuse"))
                                            {
                                                f.m_diffuseUrl = HELPER.instance.absoletePath + methodCall.result[i].GetField("diffuse").str;
                                            }
                                            if (methodCall.result[i].HasField("normals"))
                                            {
                                                f.m_normalMapUrl = HELPER.instance.absoletePath + methodCall.result[i].GetField("normals").str;
                                            }

                                            if (methodCall.result[i].HasField("metalness"))
                                            {
                                                f.m_metalnessMapUrl = HELPER.instance.absoletePath + methodCall.result[i].GetField("metalness").str;
                                            }

                                            if (methodCall.result[i].HasField("isColor"))
                                            {
                                                f.isColor = Convert.ToBoolean(methodCall.result[i].GetField("isColor"));
                                                // Debug.Log("Color " + methodCall.result[i].GetField("isColor"));
                                            }

                                            f.fBaseMatID = new List<string>();
                                            for (int k = 0; k < fbmID.Count; k++)
                                            {
                                                f.fBaseMatID.Add(fbmID[k]);
                                            }


                                            sceneManager.m_finishes.Add(f);

                                        }

                                        HELPER.instance.InstatiateFinishBTN();
                                        var testDDPAccount = FindObjectOfType<TestDdpAccount>();
                                        testDDPAccount.callbackFinishList = true;
                                        testDDPAccount.CheckUserLoginStatus();
                                    }
                                    methodCall.currentMethods = MethodCall.methods.DEFAULT;
                                }
                                #endregion

                                #region MATERIAL_LIST
                                else if (methodCall.currentMethods == MethodCall.methods.MATERIAL_LIST)
                                {
                                    if (methodCall.result != null)
                                    {

                                        for (int i = 0; i < methodCall.result.Count; i++)
                                        {
                                            var matName = methodCall.result[i].GetField("material_Name").str;
                                            var matID = methodCall.result[i].GetField("_id").str;
                                            HELPER.instance.materialName.Add(matName);
                                            HELPER.instance.materialID.Add(matID);

                                            var matBtn = Instantiate(HELPER.instance.materialBtn);
                                            matBtn.transform.SetParent(HELPER.instance.parentForMaterialBtns, false);
                                            matBtn.transform.GetChild(0).GetComponent<Text>().text = matName;

                                            var character = matName.Substring(0, 2);

                                            matBtn.transform.GetChild(1).GetComponent<Text>().text = character.ToUpper();

                                            matBtn.GetComponent<Button>().onClick.AddListener(() => HELPER.instance.ChooseMaterialInFilter(matBtn.GetComponent<Image>()));
                                        }

                                        var testDDPAccount = FindObjectOfType<TestDdpAccount>();
                                        testDDPAccount.callbackMaterialList = true;
                                        testDDPAccount.CheckUserLoginStatus();
                                    }
                                    methodCall.currentMethods = MethodCall.methods.DEFAULT;
                                }
                                #endregion

                                #region Login
                                else if (methodCall.currentMethods == MethodCall.methods.LOGIN)
                                {
                                    
                                        HELPER.instance.userID = methodCall.result.GetField("id").str;
                                        var testDdpAccount = FindObjectOfType<TestDdpAccount>();
                                        testDdpAccount.GetUserDetails();
                                        PlayerPrefs.SetString("userID", HELPER.instance.userID);
                                        PlayerPrefs.Save();
                                      
                                    
                                    methodCall.currentMethods = MethodCall.methods.DEFAULT;

                                }
                                #endregion

                                #region Board

                                else if (methodCall.currentMethods == MethodCall.methods.BOARD)
                                {

                                    if (methodCall.result != null)
                                    {
                                        for (int i = 0; i < methodCall.result.Count; i++)
                                        {
                                            GameObject boardWithHeader = null;
                                            GameObject boardWithoutHeader = null;
                                            if (methodCall.result[i].HasField("board_Images"))
                                            {
                                                var url = methodCall.result[i].GetField("board_Images").str;
                                                var boardName = methodCall.result[i].GetField("board_Name").str;

                                                boardWithHeader = Instantiate(HELPER.instance.boardWithHeader);
                                                boardWithHeader.transform.SetParent(HELPER.instance.mainBoardContent, false);
                                                if (methodCall.result[i].HasField("board_Description"))
                                                {
                                                    var boardDescription = methodCall.result[i].GetField("board_Description").str;
                                                    boardWithHeader.GetComponent<MainBoard>().mainDescription = boardDescription;
                                                }
                                                boardWithHeader.GetComponent<MainBoard>().mainTitle = boardName;
                                                boardWithHeader.GetComponent<MainBoard>().boardWithHeader = true;
                                                
                                                boardWithHeader.transform.GetChild(0).GetChild(0).GetComponent<Board>().headerURL = HELPER.instance.absoletePath + url;
                                                boardWithHeader.transform.GetChild(1).GetComponent<Text>().text = boardName;
                                               // Debug.Log("header");


                                                double timestamp = Convert.ToDouble(methodCall.result[i].GetField("createdAt")[0].ToString());
                                                var dateTime = new DateTime(1970, 1, 1).AddMilliseconds(timestamp).ToLocalTime();

                                                var day = dateTime.Day;
                                                var year = dateTime.Year;

                                                switch (dateTime.Month)
                                                {

                                                    case 1:
                                                        var dateText = ("Jan. " + day + "," + " " + year);
                                                        boardWithHeader.transform.GetChild(3).GetComponent<Text>().text = dateText;

                                                        break;
                                                    case 2:
                                                        var dateText1 = ("Feb. " + day + "," + " " + year);
                                                        boardWithHeader.transform.GetChild(3).GetComponent<Text>().text = dateText1;
                                                        break;
                                                    case 3:
                                                        var dateText2 = ("Mar. " + day + "," + " " + year);
                                                        boardWithHeader.transform.GetChild(3).GetComponent<Text>().text = dateText2;
                                                        break;
                                                    case 4:
                                                        var dateText3 = ("Apr. " + day + "," + " " + year);
                                                        boardWithHeader.transform.GetChild(3).GetComponent<Text>().text = dateText3;
                                                        break;
                                                    case 5:
                                                        var dateText4 = ("May " + day + "," + " " + year);
                                                        boardWithHeader.transform.GetChild(3).GetComponent<Text>().text = dateText4;
                                                        break;
                                                    case 6:
                                                        var dateText5 = ("Jun. " + day + "," + " " + year);
                                                        boardWithHeader.transform.GetChild(3).GetComponent<Text>().text = dateText5;
                                                        break;
                                                    case 7:
                                                        var dateText6 = ("Jul. " + day + "," + " " + year);
                                                        boardWithHeader.transform.GetChild(3).GetComponent<Text>().text = dateText6;
                                                        break;
                                                    case 8:
                                                        var dateText7 = ("Aug. " + day + "," + " " + year);
                                                        boardWithHeader.transform.GetChild(3).GetComponent<Text>().text = dateText7;
                                                        break;
                                                    case 9:
                                                        var dateText8 = ("Sept. " + day + "," + " " + year);
                                                        boardWithHeader.transform.GetChild(3).GetComponent<Text>().text = dateText8;
                                                        break;
                                                    case 10:
                                                        var dateText9 = ("Oct. " + day + "," + " " + year);
                                                        boardWithHeader.transform.GetChild(3).GetComponent<Text>().text = dateText9;
                                                        break;
                                                    case 11:
                                                        var dateText10 = ("Nov. " + day + "," + " " + year);
                                                        boardWithHeader.transform.GetChild(3).GetComponent<Text>().text = dateText10;
                                                        break;
                                                    case 12:
                                                        var dateText11 = ("Dec. " + day + "," + " " + year);
                                                        boardWithHeader.transform.GetChild(3).GetComponent<Text>().text = dateText11;
                                                        break;

                                                    default:
                                                        break;
                                                }

                                                var count = methodCall.result[i].GetField("board_Grid").Count;

                                                if (count > 1)
                                                {
                                                    boardWithHeader.transform.GetChild(2).GetComponent<Text>().text = count + " Items";
                                                }
                                                else
                                                {
                                                    boardWithHeader.transform.GetChild(2).GetComponent<Text>().text = count + " Item";
                                                }

                                                for (int j = 0; j < count; j++)
                                                {
                                                    Boards b = new Boards();
                                                    if (methodCall.result[i].GetField("board_Grid")[j].HasField("grid_type"))
                                                    {
                                                        b.gridType = methodCall.result[i].GetField("board_Grid")[j].GetField("grid_type").str;
                                                    }
                                                    if (methodCall.result[i].GetField("board_Grid")[j].HasField("productId"))
                                                    {
                                                        b.id = methodCall.result[i].GetField("board_Grid")[j].GetField("productId").str;
                                                    }
                                                    if (methodCall.result[i].GetField("board_Grid")[j].HasField("fileType"))
                                                    {
                                                        b.fileType = methodCall.result[i].GetField("board_Grid")[j].GetField("fileType").str;
                                                    }

                                                    if (methodCall.result[i].GetField("board_Grid")[j].HasField("file_path"))
                                                    {
                                                        b.url = HELPER.instance.absoletePath + methodCall.result[i].GetField("board_Grid")[j].GetField("file_path").str;
                                                    }
                                                    if (methodCall.result[i].GetField("board_Grid")[j].HasField("title"))
                                                    {
                                                        b.title = methodCall.result[i].GetField("board_Grid")[j].GetField("title").str;
                                                    }
                                                    if (methodCall.result[i].GetField("board_Grid")[j].HasField("colorCode"))
                                                    {
                                                        b.colorCode = methodCall.result[i].GetField("board_Grid")[j].GetField("colorCode").str;
                                                    }

                                                    if (methodCall.result[i].GetField("board_Grid")[j].HasField("description"))
                                                    {
                                                        b.description = methodCall.result[i].GetField("board_Grid")[j].GetField("description").str;
                                                    }

                                                boardWithHeader.GetComponent<MainBoard>().boards.Add(b);
                                                }

                                            }
// no header
                                            else if (methodCall.result[i].HasField("board_Grid"))
                                            {
                                                var boardName = methodCall.result[i].GetField("board_Name").str;

                                                boardWithoutHeader = Instantiate(HELPER.instance.boardWithoutHeader);
                                                boardWithoutHeader.transform.SetParent(HELPER.instance.mainBoardContent, false);
                                                boardWithoutHeader.transform.GetChild(5).GetComponent<Text>().text = boardName;

                                                var count = methodCall.result[i].GetField("board_Grid").Count;

                                                if (count > 1)
                                                {
                                                   boardWithoutHeader.transform.GetChild(6).GetComponent<Text>().text = count + " Items";
                                                }
                                                else
                                                {

                                                    boardWithoutHeader.transform.GetChild(6).GetComponent<Text>().text = count + " Item";

                                                }

                                                double timestamp = Convert.ToDouble(methodCall.result[i].GetField("createdAt")[0].ToString());
                                                var dateTime = new DateTime(1970, 1, 1).AddMilliseconds(timestamp).ToLocalTime();

                                                var day = dateTime.Day;
                                                var year = dateTime.Year;

                                                switch (dateTime.Month)
                                                {

                                                    case 1:
                                                        var dateText = ("Jan. " + day + "," + " " + year);
                                                        boardWithoutHeader.transform.GetChild(7).GetComponent<Text>().text = dateText;

                                                        break;
                                                    case 2:
                                                        var dateText1 = ("Feb. " + day + "," + " " + year);
                                                        boardWithoutHeader.transform.GetChild(7).GetComponent<Text>().text = dateText1;
                                                        break;
                                                    case 3:
                                                        var dateText2 = ("Mar. " + day + "," + " " + year);
                                                        boardWithoutHeader.transform.GetChild(7).GetComponent<Text>().text = dateText2;
                                                        break;
                                                    case 4:
                                                        var dateText3 = ("Apr. " + day + "," + " " + year);
                                                        boardWithoutHeader.transform.GetChild(7).GetComponent<Text>().text = dateText3;
                                                        break;
                                                    case 5:
                                                        var dateText4 = ("May " + day + "," + " " + year);
                                                        boardWithoutHeader.transform.GetChild(7).GetComponent<Text>().text = dateText4;
                                                        break;
                                                    case 6:
                                                        var dateText5 = ("Jun. " + day + "," + " " + year);
                                                        boardWithoutHeader.transform.GetChild(7).GetComponent<Text>().text = dateText5;
                                                        break;
                                                    case 7:
                                                        var dateText6 = ("Jul. " + day + "," + " " + year);
                                                        boardWithoutHeader.transform.GetChild(7).GetComponent<Text>().text = dateText6;
                                                        break;
                                                    case 8:
                                                        var dateText7 = ("Aug. " + day + "," + " " + year);
                                                        boardWithoutHeader.transform.GetChild(7).GetComponent<Text>().text = dateText7;
                                                        break;
                                                    case 9:
                                                        var dateText8 = ("Sept. " + day + "," + " " + year);
                                                        boardWithoutHeader.transform.GetChild(7).GetComponent<Text>().text = dateText8;
                                                        break;
                                                    case 10:
                                                        var dateText9 = ("Oct. " + day + "," + " " + year);
                                                        boardWithoutHeader.transform.GetChild(7).GetComponent<Text>().text = dateText9;
                                                        break;
                                                    case 11:
                                                        var dateText10 = ("Nov. " + day + "," + " " + year);
                                                        boardWithoutHeader.transform.GetChild(7).GetComponent<Text>().text = dateText10;
                                                        break;
                                                    case 12:
                                                        var dateText11 = ("Dec. " + day + "," + " " + year);
                                                        boardWithoutHeader.transform.GetChild(7).GetComponent<Text>().text = dateText11;
                                                        break;

                                                    default:
                                                        break;
                                                }



                                                for (int j = 0; j < methodCall.result[i].GetField("board_Grid").Count; j++)
                                                {
                                                    
                                                    if (j <= 4)
                                                    {
                                                        if (methodCall.result[i].GetField("board_Grid")[j].HasField("file_path"))
                                                        {

                                                            var url = methodCall.result[i].GetField("board_Grid")[j].GetField("file_path").str;

                                                            boardWithoutHeader.transform.GetChild(j).GetComponent<Board>().headerURL = HELPER.instance.absoletePath + url;
                                                        }
                                                        else if (methodCall.result[i].GetField("board_Grid")[j].HasField("image_path"))
                                                        {
                                                            var url2 = methodCall.result[i].GetField("board_Grid")[j].GetField("image_path").str;
                                                            boardWithoutHeader.transform.GetChild(j).GetComponent<Board>().headerURL = HELPER.instance.absoletePath + url2;
                                                        }
                                                        else if (methodCall.result[i].GetField("board_Grid")[j].HasField("colorCode"))
                                                        {
                                                            var hex = methodCall.result[i].GetField("board_Grid")[j].GetField("colorCode").str;
                                                            boardWithoutHeader.transform.GetChild(j).GetComponent<Board>().hex = hex;
                                                        }
                                                        
                                                        else if (methodCall.result[i].GetField("board_Grid")[j].HasField("productId"))
                                                        {
                                                            var id = methodCall.result[i].GetField("board_Grid")[j].GetField("productId").str;
                                                            boardWithoutHeader.transform.GetChild(j).GetComponent<Board>().id = id;
                                                        }
                                                    }
                                                }


                                                for (int j = 0; j < count; j++)
                                                {
                                                    Boards b = new Boards();
                                                    if (methodCall.result[i].GetField("board_Grid")[j].HasField("grid_type"))
                                                    {
                                                        b.gridType = methodCall.result[i].GetField("board_Grid")[j].GetField("grid_type").str;
                                                    }
                                                    if (methodCall.result[i].GetField("board_Grid")[j].HasField("productId"))
                                                    {
                                                        b.id = methodCall.result[i].GetField("board_Grid")[j].GetField("productId").str;
                                                    }
                                                    if (methodCall.result[i].GetField("board_Grid")[j].HasField("fileType"))
                                                    {
                                                        b.fileType = methodCall.result[i].GetField("board_Grid")[j].GetField("fileType").str;
                                                    }

                                                    if (methodCall.result[i].GetField("board_Grid")[j].HasField("file_path"))
                                                    {
                                                        b.url = HELPER.instance.absoletePath + methodCall.result[i].GetField("board_Grid")[j].GetField("file_path").str;
                                                    }
                                                    if (methodCall.result[i].GetField("board_Grid")[j].HasField("title"))
                                                    {
                                                        b.title = methodCall.result[i].GetField("board_Grid")[j].GetField("title").str;
                                                    }
                                                    if (methodCall.result[i].GetField("board_Grid")[j].HasField("colorCode"))
                                                    {
                                                        b.colorCode = methodCall.result[i].GetField("board_Grid")[j].GetField("colorCode").str;
                                                    }

                                                    if (methodCall.result[i].GetField("board_Grid")[j].HasField("description"))
                                                    {
                                                        b.description = methodCall.result[i].GetField("board_Grid")[j].GetField("description").str;
                                                    }
                                                boardWithoutHeader.GetComponent<MainBoard>().boards.Add(b);
                                                }

                                                if (methodCall.result[i].HasField("board_Description"))
                                                {
                                                    var boardDescription = methodCall.result[i].GetField("board_Description").str;
                                                    boardWithoutHeader.GetComponent<MainBoard>().mainDescription = boardDescription;
                                                }
                                                boardWithoutHeader.GetComponent<MainBoard>().mainTitle = boardName;


                                            }
                                        }

                                    }
                                    methodCall.currentMethods = MethodCall.methods.DEFAULT;
                                }
                                #endregion
                            }

                        
                        break;
                    }

                case MessageType.UPDATED:
                    {
                        string[] methodCallIds = ToStringArray(message[Field.METHODS]);
                        foreach (string methodCallId in methodCallIds)
                        {
                            MethodCall methodCall = methodCalls[methodCallId];


                            if (methodCall != null)
                            {
                                if (methodCall.hasResult)
                                {
                                    methodCalls.Remove(methodCallId);
                                }
                                methodCall.hasUpdated = true;
                                if (methodCall.OnUpdated != null)
                                {
                                    methodCall.OnUpdated(methodCall);
                                }


                            }
                        }
                        break;
                    }

                case MessageType.ERROR:
                    {
                        if (OnError != null)
                        {
                            OnError(GetError(message));
                            
                        }
                        break;
                    }
            }
        }


        

       

        //public void AccessData(JSONObject obj)
        //{
        //    switch (obj.type)
        //    {
        //        case JSONObject.Type.OBJECT:
        //            for (int i = 0; i < obj.list.Count; i++)
        //            {
        //                string key = (string)obj.keys[i];
        //                JSONObject j = (JSONObject)obj.list[i];
        //                Debug.Log(key);

        //                AccessData(j);
        //           }
        //            break;
        //        case JSONObject.Type.ARRAY:
        //            foreach (JSONObject j in obj.list)
        //            {

        //                AccessData(j);
        //            }
        //            break;
        //        case JSONObject.Type.STRING:

        //            Debug.Log(obj.str);
        //             break;
        //        case JSONObject.Type.NUMBER:

        //            Debug.Log(obj.n);
        //            break;
        //        case JSONObject.Type.BOOL:

        //            Debug.Log(obj.b);
        //            break;
        //        case JSONObject.Type.NULL:

        //            Debug.Log("NULL");
        //            break;

        //    }
        //}




        private string GetConnectMessage()
        {
            JSONObject message = new JSONObject(JSONObject.Type.OBJECT);
            message.AddField(Field.MSG, MessageType.CONNECT);
            if (sessionId != null)
            {
                message.AddField(Field.SESSION, sessionId);
            }
            message.AddField(Field.VERSION, DDP_PROTOCOL_VERSION);

            JSONObject supportedVersions = new JSONObject(JSONObject.Type.ARRAY);
            supportedVersions.Add(DDP_PROTOCOL_VERSION);
            message.AddField(Field.SUPPORT, supportedVersions);

            return message.Print();
        }

        private string GetPongMessage()
        {
            JSONObject message = new JSONObject(JSONObject.Type.OBJECT);
            message.AddField(Field.MSG, MessageType.PONG);

            return message.Print();
        }

        private string GetPongMessage(string id)
        {
            JSONObject message = new JSONObject(JSONObject.Type.OBJECT);
            message.AddField(Field.MSG, MessageType.PONG);
            message.AddField(Field.ID, id);

            return message.Print();
        }

        private string GetSubscriptionMessage(Subscription subscription)
        {
            JSONObject message = new JSONObject(JSONObject.Type.OBJECT);
            message.AddField(Field.MSG, MessageType.SUB);
            message.AddField(Field.ID, subscription.id);
            message.AddField(Field.NAME, subscription.name);
            if (subscription.items.Length > 0)
            {
                message.AddField(Field.PARAMS, new JSONObject(subscription.items));
            }

            return message.Print();
        }

        private string GetUnsubscriptionMessage(Subscription subscription)
        {
            JSONObject message = new JSONObject(JSONObject.Type.OBJECT);
            message.AddField(Field.MSG, MessageType.UNSUB);
            message.AddField(Field.ID, subscription.id);

            return message.Print();
        }

        private string GetMethodCallMessage(MethodCall methodCall)
        {
            JSONObject message = new JSONObject(JSONObject.Type.STRING);
            message.AddField(Field.MSG, MessageType.METHOD);
            message.AddField(Field.METHOD, methodCall.methodName);
            if (methodCall.items.Length > 0)
            {
                message.AddField(Field.PARAMS, new JSONObject(methodCall.items));
            }
            message.AddField(Field.ID, methodCall.id);
            //message.AddField(Field.RANDOM_SEED, xxx);

            return message.Print();
        }


        private string NewCallMessage(MethodCall methodCall)
        {
            JSONObject message = new JSONObject(JSONObject.Type.OBJECT);
            message.AddField(Field.MSG, MessageType.METHOD);
            message.AddField(Field.METHOD, methodCall.methodName);
            if (methodCall.items.Length > 0)
            {
                // JSONObject myString = new JSONObject(JSONObject.Type.OBJECT);
                //message.AddField(Field.PARAMS, delegate (JSONObject (fields));

                message.AddField(Field.PARAMS, delegate (JSONObject fields)
                {
                    fields.Add(HELPER.instance.userID);
                    fields.Add(HELPER.instance.userID);
                });
            }
            message.AddField(Field.ID, methodCall.id);
            return message.Print();
        }

        private string NewCallMessage2(MethodCall methodCall)
        {
            JSONObject message = new JSONObject(JSONObject.Type.OBJECT);
            message.AddField(Field.MSG, MessageType.METHOD);
            message.AddField(Field.METHOD, methodCall.methodName);

            message.AddField(Field.PARAMS, delegate (JSONObject fields)
                {
                    fields.Add(HELPER.instance.userID);
                   // fields.Add("H79SEW25CmRPHjGYd");
                });
            
            message.AddField(Field.ID, methodCall.id);
            return message.Print();
        }



        private string GetMethodCustomCallMessage(MethodCustomCall methodCustomCall)
        {
            JSONObject message = new JSONObject(JSONObject.Type.STRING);
            message.AddField(Field.MSG, MessageType.METHOD);
            message.AddField(methodCustomCall.methodName, "uixjmiDx8ZQioBCkq");

            // message.AddField(Field.METHOD, );

            message.AddField(Field.ID, methodCustomCall.id);
            //message.AddField(Field.RANDOM_SEED, xxx);

            return message.Print();
        }





        private string GetMethodCustomCallMessage(MethodCall methodCall)
        {
            JSONObject message = new JSONObject(JSONObject.Type.OBJECT);
            message.AddField(Field.MSG, MessageType.METHOD);
            message.AddField(Field.METHOD, methodCall.methodName);
            message.AddField(Field.ID, methodCall.id);


            return message.Print();
        }


        private DdpError GetError(JSONObject obj)
        {
            string errorCode = null;
            if (obj.HasField(Field.ERROR))
            {
                JSONObject errorCodeObj = obj[Field.ERROR];
                errorCode = errorCodeObj.IsNumber ? "" + errorCodeObj.i : errorCodeObj.str;
            }

            return new DdpError()
            {
                errorCode = errorCode,
                reason = obj.HasField(Field.REASON) ? obj[Field.REASON].str : null,
                message = obj.HasField(Field.MESSAGE) ? obj[Field.MESSAGE].str : null,
                errorType = obj.HasField(Field.ERROR_TYPE) ? obj[Field.ERROR_TYPE].str : null,
                offendingMessage = obj.HasField(Field.OFFENDING_MESSAGE) ? obj[Field.OFFENDING_MESSAGE].str : null
            };
        }

        private string[] ToStringArray(JSONObject jo)
        {
            string[] result = new string[jo.Count];
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = jo[i].str;
            }
            return result;
        }

        private void Send(string message)
        {
            if (logMessages) Debug.Log("Send: " + message);
            ws.Send(message);
        }

        public ConnectionState GetConnectionState()
        {
            //Debug.Log("Conn state " + ddpConnectionState);
            return ddpConnectionState;

        }

        public void Connect()
        {
            if ((ddpConnectionState == ConnectionState.NOT_CONNECTED) ||
                (ddpConnectionState == ConnectionState.DISCONNECTED) ||
                (ddpConnectionState == ConnectionState.CLOSED))
            {
                ddpConnectionState = ConnectionState.CONNECTING;


                ws.ConnectAsync();
            }
        }

        public void Close()
        {
            if (ddpConnectionState == ConnectionState.CONNECTED)
            {
                ddpConnectionState = ConnectionState.CLOSING;
                ws.Close();
            }
        }

        void IDisposable.Dispose()
        {
            Close();
        }

        public Subscription Subscribe(string name, params JSONObject[] items)
        {
            Subscription subscription = new Subscription()
            {
                id = "" + subscriptionId++,
                name = name,
                items = items
            };
            subscriptions[subscription.id] = subscription;

            Send(GetSubscriptionMessage(subscription));
            return subscription;
        }

        public void Unsubscribe(Subscription subscription)
        {
            Send(GetUnsubscriptionMessage(subscription));
        }

        public MethodCall Call(string methodName, params JSONObject[] items)
        {
            MethodCall methodCall = new MethodCall()
            {
                id = "" + methodCallId++,
                methodName = methodName,
                items = items
            };
            methodCalls[methodCall.id] = methodCall;
            Send(GetMethodCallMessage(methodCall));
            return methodCall;
        }

        public MethodCall NewCall(string methodName, params JSONObject[] items)
        {
            MethodCall methodCall = new MethodCall()
            {
                id = "" + methodCallId++,
                methodName = methodName,
                items = items
            };
            methodCalls[methodCall.id] = methodCall;
            Send(NewCallMessage(methodCall));
            return methodCall;
        }

        public MethodCall CallForUserID(string methodName, params JSONObject[] items)
        {
            MethodCall methodCall = new MethodCall()
            {
                id = "" + methodCallId++,
                methodName = methodName,
                items = items
            };
            methodCalls[methodCall.id] = methodCall;
            Send(NewCallMessage2(methodCall));
            return methodCall;
        }




    }

}

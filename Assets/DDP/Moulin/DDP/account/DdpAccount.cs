using System;
using System.Text;
using System.Security.Cryptography;
using System.Collections;

namespace Moulin.DDP {

	public class DdpAccount {

		private DdpConnection connection;

		public bool isLogged;
		public string userId;
		public string token;
		public DateTime tokenExpiration;

		public DdpError error;

		public DdpAccount(DdpConnection connection) {
			this.connection = connection;
		}

		private JSONObject GetPasswordObj(string password) {
			string digest = BitConverter.ToString(
				new SHA256Managed().ComputeHash(Encoding.UTF8.GetBytes(password)))
				.Replace("-", "").ToLower();

			JSONObject passwordObj = JSONObject.Create();
			passwordObj.AddField("digest", digest);
			passwordObj.AddField("algorithm", "sha-256");

			return passwordObj;
		}

		private void HandleLoginResult(MethodCall loginCall) {
			error = loginCall.error;

			if (error == null) {
				JSONObject result = loginCall.result;
				isLogged = true;
				this.userId = result["id"].str;
				this.token = result["token"].str;
				this.tokenExpiration = result["tokenExpires"].GetDateTime();
			}
		}

		private void HandleLogoutResult(MethodCall logoutCall) {
			error = logoutCall.error;

			if (error == null) {
				isLogged = false;
                userId = null;
                token = null;
                tokenExpiration = default(DateTime);
			}
		}

		public IEnumerator CreateUserAndLogin(string username, string password) {
			JSONObject loginPasswordObj = JSONObject.Create();
			loginPasswordObj.AddField("username", username);
			loginPasswordObj.AddField("password", GetPasswordObj(password));

			MethodCall loginCall = connection.Call("createUser", loginPasswordObj);
			yield return loginCall.WaitForResult();
			HandleLoginResult(loginCall);
		}

		public IEnumerator LoginWithEmail(string username, string password) {

            JSONObject userObj = JSONObject.Create();
			userObj.AddField("email", username);

			JSONObject loginPasswordObj = JSONObject.Create();
			loginPasswordObj.AddField("user", userObj);
            loginPasswordObj.AddField("password", GetPasswordObj(password));
            
            MethodCall loginCall = connection.Call("login", loginPasswordObj);
            loginCall.currentMethods = MethodCall.methods.LOGIN;
            yield return loginCall.WaitForResult();
			HandleLoginResult(loginCall);
           // GameObject.FindObjectOfType<TestDdpAccount>().CheckUserLoginStatus();

		}


        //public IEnumerator LoginWithUserName(string username, string password)
        //{
        //    JSONObject userObj = JSONObject.Create();
        //    userObj.AddField("username", username);

        //    JSONObject loginPasswordObj = JSONObject.Create();
        //    loginPasswordObj.AddField("user", userObj);
        //    loginPasswordObj.AddField("password", GetPasswordObj(password));

        //    MethodCall loginCall = connection.Call("login", loginPasswordObj);
        //    loginCall.currentMethods = MethodCall.methods.LOGIN;

        //    yield return loginCall.WaitForResult();
        //    HandleLoginResult(loginCall);
        //  //  GameObject.FindObjectOfType<TestDdpAccount>().CheckUserLoginStatus();

        //}

        public IEnumerator GetUserDetails()
        {

            JSONObject userObj = JSONObject.Create();
            //userObj.Add("clgupta@polynine.com");



            MethodCall testCall = connection.CallForUserID("currentUserDetails", userObj);
            testCall.currentMethods = MethodCall.methods.GET_USER_DETAILS;
            yield return testCall.WaitForResult();
            

        }

        public IEnumerator GetprductStyleList()
        {

            JSONObject userObj = JSONObject.Create();
            MethodCall Call = connection.NewCall("prductStyleList", userObj);
            Call.currentMethods = MethodCall.methods.PRODUCTS_STYLE_LIST;
            yield return Call.WaitForResult();
         
        }

        public IEnumerator GetBoardList()
        {

            JSONObject userObj = JSONObject.Create();
            MethodCall Call = connection.NewCall("boardsList", userObj);
            Call.currentMethods = MethodCall.methods.BOARD;
            yield return Call.WaitForResult();

        }

        public IEnumerator GetMaterialList()
        {
            JSONObject userObj = JSONObject.Create();
            MethodCall Call = connection.NewCall("materialsList", userObj);
            Call.currentMethods = MethodCall.methods.MATERIAL_LIST;
            yield return Call.WaitForResult();
        }

        

        public IEnumerator GetProductList()
        {
            JSONObject userObj = JSONObject.Create();
            MethodCall Call = connection.NewCall("productsList", userObj);
            Call.currentMethods = MethodCall.methods.PRODUCTS_LIST;
            yield return Call.WaitForResult();
        }


        public IEnumerator IsPriceShowFormanufacturer()
        {
            JSONObject userObj = JSONObject.Create();
            
            MethodCall Call = connection.NewCall("isPriceShowFormanufacturer", userObj);
            Call.currentMethods = MethodCall.methods.IS_PRICE_SHOW;
            yield return Call.WaitForResult();
        }



        public IEnumerator GetFinishesList()
        {
            JSONObject userObj = JSONObject.Create();
            MethodCall testCall = connection.NewCall("finishesList", userObj);
            testCall.currentMethods = MethodCall.methods.FINISHES_LIST;
            yield return testCall.WaitForResult();
        }

        public IEnumerator ResumeSession(string token) {
			JSONObject tokenObj = JSONObject.Create();
			tokenObj.AddField("resume", token);

			MethodCall loginCall = connection.Call("login", tokenObj);
			yield return loginCall.WaitForResult();
			HandleLoginResult(loginCall);
		}

		public IEnumerator Logout() {
			MethodCall logoutCall = connection.Call("logout");
			yield return logoutCall.WaitForResult();
			HandleLogoutResult(logoutCall);
		}

	}

}

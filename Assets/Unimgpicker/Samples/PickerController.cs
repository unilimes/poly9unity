﻿using UnityEngine;
using System.Collections;

namespace Kakera
{
    public class PickerController : MonoBehaviour
    {
        [SerializeField]
        Unimgpicker imagePicker;
        MeshRenderer tempRenderer;
        public Texture2D texture2d;
        [SerializeField]
        int borderWidth = 5;
        [SerializeField]
        Texture2D tex;
        [SerializeField]
        Material[] arrayMaterials;
        [SerializeField]
        Material[] tempMaterials;
        Material newMat;
        public Material materialForCopy;
        public Sprite createdSprite;

        void Awake()
        {
            imagePicker.Completed += (string path) =>
            {
                tempRenderer = GameObject.Find(HELPER.instance.partName).GetComponent<MeshRenderer>();
                StartCoroutine(LoadImage(path));
               
            };
        }


        public void PickTexture()
        {
           
            tempRenderer = GameObject.Find(HELPER.instance.partName).GetComponent<MeshRenderer>();
            newMat = new Material(Shader.Find("Standard"));

            var i = texture2d;
            var borderColor = new Color(0, 0, 0, 0);

            for (var x = 0; x < i.width; x++)
            {

                for (var y = 0; y < i.height; y++)
                {
                    if (x < borderWidth || x > i.width - 1 - borderWidth) i.SetPixel(x, y, borderColor);

                    else if (y < borderWidth || y > i.height - 1 - borderWidth) i.SetPixel(x, y, borderColor);
                }
            }

            i.Apply();
            texture2d = i;
            texture2d.wrapMode = TextureWrapMode.Clamp;

            newMat.SetTexture("_MainTex", texture2d);
            newMat.SetFloat("_Mode", 1.0f);
            newMat.SetFloat("_Cutoff", 1.0f);
            newMat.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            newMat.EnableKeyword("_ALPHABLEND_ON");

            createdSprite = Sprite.Create((Texture2D)newMat.mainTexture, new Rect(0.0f, 0.0f, newMat.mainTexture.width, newMat.mainTexture.height), new Vector2(0.5f, 0.5f), 100.0f);

            newMat.renderQueue = 3000;
            newMat.name = "LogoMat";


          //  helper.savedLogoMaterials.Add(newMat);
            //ES2.Save(helper.savedLogoMaterials, "savedMaterials.txt?tag=LogoMaterials");
           // helper.AddLogoRuntime(helper.savedLogoMaterials.Count-1);

            
            

            arrayMaterials = tempRenderer.sharedMaterials;
            tempMaterials = arrayMaterials;
           
            arrayMaterials = new Material[arrayMaterials.Length + 1];
           
            for (int j = 0; j < arrayMaterials.Length - 1; j++)
            {
               if (j == 0)
                {
                    arrayMaterials[j] = tempMaterials[j];
                }
                else if (j >= 1)
                {
                    arrayMaterials[j + 1] = tempMaterials[j];
                }

            }

            arrayMaterials[1] = newMat;
            arrayMaterials[1].CopyPropertiesFromMaterial(materialForCopy);
            arrayMaterials[1].SetTexture("_MainTex", texture2d);

            Vector2 scale = arrayMaterials[1].GetTextureScale("_MainTex");
            Vector2 offset = arrayMaterials[1].GetTextureOffset("_MainTex");
            arrayMaterials[1].SetTextureScale("_MainTex", new Vector2(scale.x = 1.5f, scale.y = 1.5f));
            arrayMaterials[1].SetTextureOffset("_MainTex", new Vector2(offset.x = -0.25f, offset.y = -0.25f));

            tempRenderer.materials = arrayMaterials;
            var sp = Sprite.Create((Texture2D)newMat.mainTexture, new Rect(0.0f, 0.0f, newMat.mainTexture.width, newMat.mainTexture.height), new Vector2(0.5f, 0.5f), 100.0f);
            //HELPER.instance.spritesForTextLogoBtn.Add(sp);


           // HELPER.instance.customTextList.Add(HELPER.instance.hackText);
           // HELPER.instance.customMaterialPartName.Add(HELPER.instance.partName);
            HELPER.instance.AddTextAndLogoButtonRuntime(1);
            
            HELPER.instance.ChangeIndexAfterAddText();
           // HELPER.instance.indexMatList.Add(1);
         
            // HELPER.instance.SavedMaterials.Insert(0,newMat);

        }

        public void OnPressShowPicker()
        {
#if !UNITY_EDITOR
             imagePicker.Show("Select Image", "unimgpicker", 1024);
#elif UNITY_EDITOR
             texture2d = tex;
#endif


        }

        private IEnumerator LoadImage(string path)
        {
            var url = "file://" + path;
            var www = new WWW(url);
            yield return www;

            texture2d = www.texture;
            if (texture2d == null)
            {
                Debug.LogError("Failed to load texture url:" + url);
            }

            var i = texture2d;
            var borderColor = new Color(0, 0, 0, 0);

            for (var x = 0; x < i.width; x++)
            {

                for (var y = 0; y < i.height; y++)
                {
                    if (x < borderWidth || x > i.width - 1 - borderWidth) i.SetPixel(x, y, borderColor);

                    else if (y < borderWidth || y > i.height - 1 - borderWidth) i.SetPixel(x, y, borderColor);
                }
            }

            i.Apply();
            var tempText = new Texture2D(texture2d.width, texture2d.height, TextureFormat.ARGB32, false);
            www.LoadImageIntoTexture(tempText);
            tempText.Apply();
            texture2d = tempText;
            texture2d.Apply();

            PickTexture();

        }
        //private void Update()
        //{
        //    if (Input.GetKeyDown(KeyCode.A))
        //    {
        //        OnPressShowPicker();
        //        PickTexture();
        //    }

        //}
    }

   
}